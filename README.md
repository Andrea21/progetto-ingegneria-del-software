INSTRUCTIONS TO START THE GAME:

[1] Insert in the file IPServer.txt the IP address of the server you want to use 
[2] Run the class StarterServer 
[3] Run the class ViewCLI to open the console 
[4] Choose the configuration you want to use 
[5] Choose the connection type you want to use 
[6] Insert your name 
[7] Wait for the connection of other players 
[8] Start playing