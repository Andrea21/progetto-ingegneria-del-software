package controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.*;
import view.Observer;
import bonusandcards.*;

/**
 * Class of controller. It manages inputs from view and resources of model.
 * 
 * @author Federico
 *
 */
public class Controller implements Observer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Enumerator for type of round.<br>
	 * NORMAL: when players do main and quick actions;<br>
	 * MARKET1: when players choose what they want to sell.<br>
	 * MARKET2: when players buy objects on sale by other players.<br>
	 * FINISHED: when game is over.
	 * 
	 * @author Federico
	 *
	 */
	public enum TypeRound {
		NORMAL, MARKET1, MARKET2, FINISHED
	};

	private Model model;
	private ArrayList<Player> orderGameTurn;
	private TypeRound round;
	private boolean quickAction;
	private int mainAction;

	/**
	 * Class constructor. It sets up the order of players.
	 * 
	 * @param modelParam
	 *            Model of game.
	 */
	public Controller(Model modelParam) {

		model = modelParam;

		quickAction = true;
		mainAction = 1;
		round = TypeRound.NORMAL;

		orderGameTurn = new ArrayList<>();
		clonePlayers(orderGameTurn);
		model.setCurrentPlayer(orderGameTurn.get(0));
		orderGameTurn.remove(0);
	}

	/**
	 * Clone list of player from model to list. List must be initialized.
	 * 
	 * @param list
	 *            List of player where clone list of player from model.
	 */
	private void clonePlayers(ArrayList<Player> list) {
		list.clear();
		for (Player player : model.getPlayers()) {
			list.add(player);
		}
	}

	/**
	 * Communicate to model that is last round and set orderGameTurn to save the
	 * last players. Add 3 victory point to current player that have builded his
	 * last emporium.
	 */
	private void setLastRound() {
		model.isLastRound(true);

		for (Player player : model.getPlayers()) {
			if (player == model.getCurrentPlayer()) {
				break;
			}
			orderGameTurn.add(player);
		}
	}

	/**
	 * Method who changes the player and the type of round.
	 */
	private void changePlayer() {

		if (model.getCurrentPlayer().getNumberAvailableEmporium() == 0) {
			setLastRound();
		}

		if (orderGameTurn.isEmpty()) {
			if (model.isLastRound()) {
				setTypeRound(TypeRound.FINISHED);
				calculatePoints();
				model.notifyObservers(model);
				return;
			} else {
				if (round == TypeRound.NORMAL) {
					setTypeRound(TypeRound.MARKET1);
					clonePlayers(orderGameTurn);
				} else if (round == TypeRound.MARKET1) {
					setTypeRound(TypeRound.MARKET2);
					clonePlayers(orderGameTurn);
					Collections.shuffle(orderGameTurn);
				} else if (round == TypeRound.MARKET2) {
					setTypeRound(TypeRound.NORMAL);
					model.clearObjectForSale();
					clonePlayers(orderGameTurn);
				}
			}
		}

		model.setCurrentPlayer(orderGameTurn.get(0));

		if (model.getCurrentPlayer().isActive()) {
			mainAction = 1;
			quickAction = true;
			orderGameTurn.remove(0);

			if (round == TypeRound.NORMAL) {
				model.getCurrentPlayer().drawCardPolitic();
			}
		} else {
			orderGameTurn.remove(0);
			changePlayer();
		}
	}

	/**
	 * Calculate points of players at the end of game.
	 */
	private void calculatePoints() {

		int first;
		int second;
		int numberOfFirst;
		ArrayList<Player> playOff;
		ArrayList<Player> tempPlayers;
		HashMap<Integer, Player> ranking;
		int rank;
		Player tempPlayer;

		tempPlayers = new ArrayList<>();
		clonePlayers(tempPlayers);

		// Remove players that are inactive
		for (int i = 0; i < tempPlayers.size(); i++) {
			if (!tempPlayers.get(i).isActive()) {
				tempPlayers.remove(i);
				i--;
			}
		}

		// Take bonus from bonus tile for each player
		for (Player player : tempPlayers) {
			for (TileBonus tileBonus : player.getTilesBonus()) {
				tileBonus.getBonus().takeBonus(player);
			}
		}

		numberOfFirst = 0;
		first = -1;
		second = -1;
		// Find first in nobility track
		for (Player player : tempPlayers) {
			if (player.getPositionNobility() > first) {
				first = player.getPositionNobility();
			}
		}

		// Find another player with same nobility position and/or the second
		// player on nobility track.
		// For each player on first position, add 5 victory point.
		for (Player player : tempPlayers) {
			if (player.getPositionNobility() == first) {
				player.setPositionVictory(player.getPositionVictory() + 5);
				numberOfFirst++;
			} else if (player.getPositionNobility() > second) {
				second = player.getPositionNobility();
			}
		}

		// If there is only one player on first position, add 2 victory point
		// for each player at second position.
		if (numberOfFirst <= 1) {
			for (Player player : tempPlayers) {
				if (player.getPositionNobility() == second) {
					player.setPositionVictory(player.getPositionVictory() + 2);
				}
			}
		}

		// Find who has the most permit tile.
		first = -1;
		for (Player player : tempPlayers) {
			if (player.getTileBusinessPermit().size() > first) {
				first = player.getTileBusinessPermit().size();
			}
		}

		// Add 3 victory point at who has the most permit tile.
		for (Player player : tempPlayers) {
			if (player.getTileBusinessPermit().size() == first) {
				player.setPositionVictory(player.getPositionVictory() + 3);
			}
		}

		// Find the first player/s at victory track.
		playOff = new ArrayList<>();
		first = -1;
		for (Player player : tempPlayers) {
			if (player.getPositionVictory() > first) {
				first = player.getPositionVictory();
				playOff.clear();
				playOff.add(player);
			} else if (player.getPositionVictory() == first) {
				playOff.add(player);
			}
		}

		ranking = new HashMap<>();
		rank = 1;
		// If there's only one first, he is the winner and add him to ranking.
		if (playOff.size() == 1) {

			tempPlayers.remove(playOff.get(0));
			ranking.put(rank, playOff.get(0));

		}
		// If there are more first player, find the winner calculates number of
		// politic cards and assistants
		else {

			tempPlayer = null;

			for (Player player : playOff) {
				if (tempPlayer == null) {
					tempPlayer = player;
				} else if ((player.getcardsPolitic().size()
						+ player.getAssistants()) > (tempPlayer.getcardsPolitic().size()
								+ tempPlayer.getAssistants())) {
					tempPlayer = player;
				}
			}

			ranking.put(rank, tempPlayer);
			tempPlayers.remove(tempPlayer);

		}

		// Add the remain players to ranking
		while (!tempPlayers.isEmpty()) {
			rank++;

			tempPlayer = null;

			for (Player player : tempPlayers) {
				if (tempPlayer == null) {
					tempPlayer = player;
				} else if (player.getPositionVictory() > tempPlayer.getPositionVictory()) {
					tempPlayer = player;
				}
			}

			tempPlayers.remove(tempPlayer);

			ranking.put(rank, tempPlayer);
		}
		model.setRanking(ranking);
		model.notifyObservers("-!-Game over! The winner is " + ranking.get(1).getName());
	}

	/**
	 * Set type round on controller and model
	 * 
	 * @param typeRound
	 *            Type round
	 */
	public void setTypeRound(TypeRound typeRound) {
		round = typeRound;
		model.setTypeRound(typeRound);
	}

	/**
	 * Remove a player by name. Only set is active to false.
	 * 
	 * @param namePlayer
	 *            Name of player.
	 */
	public void removePlayer(String namePlayer) {
		model.removePlayer(namePlayer);
		model.notifyObservers("-!-Player " + namePlayer + " removed");
		if (!removalManagement() && model.getCurrentPlayer() == model.getPlayerByName(namePlayer)) {
			changePlayer();
			model.notifyObservers(model);
		}
	}

	/**
	 * Return true if it's remained one player. If true, type round is set to
	 * "FINISHED", calls calculatePoints() and notifies to model.
	 * 
	 * @return True if it's remained one player. Else false.
	 */
	private boolean removalManagement() {
		int numberActive;

		numberActive = 0;
		for (Player player : model.getPlayers()) {
			if (player.isActive()) {
				numberActive++;
			}
		}

		if (numberActive <= 1) {
			setTypeRound(TypeRound.FINISHED);
			calculatePoints();
			model.notifyObservers(model);
			return true;
		}
		return false;
	}

	/**
	 * Override of update method of observer class. The string is an input from
	 * user and I call the method round.
	 */
	@Override
	public void update(String message) {
		round(message);
	}

	/**
	 * Override of update method of observer class. I overwrite the actual model
	 * with new model.
	 */
	@Override
	public void update(Model model) {
		this.model = model;
	}

	/**
	 * Called by view by event. It calls the specific methods depending on the
	 * type of round. In case of error, it calls error method of Model to
	 * communicate error to view.
	 * 
	 * @param input
	 *            Input string of current player.
	 */
	public void round(String input) {
		String output = "";
		if (round == TypeRound.NORMAL)
			output = roundNormal(input);
		else if (round == TypeRound.MARKET1)
			output = roundMarket1(input);
		else if (round == TypeRound.MARKET2)
			output = roundMarket2(input);
		else if (round == TypeRound.FINISHED) {
			output = "Game over";
		}
		if (!"OK".equals(output)) {
			model.error(output);
		} else {
			model.notifyView();
		}
	}

	// Controllata!
	/**
	 * Method for add the object fore the sale that the current player wants to
	 * sell.
	 * 
	 * @param input
	 *            A specific string that indicates the input of current player.
	 *            <p>
	 *            "Number of assistants"
	 *            :"Price"-"Card1":"Price":"Card2":"Price"...-
	 *            "Business permit tile":"Price"
	 *            <p>
	 *            Tile business permit: string representation of tile. Look
	 *            TileBusinessPermit.toString().<br>
	 *            Card: "FUCHSIA", "BLACK", "PURPLE", "BLUE", "ORANGE", "WHITE",
	 *            "SPECIAL".<br>
	 * @return Returns "OK" if there isn't problem in input of player. Else a
	 *         wrong message.
	 */
	public String roundMarket1(String input) {

		String tempString;
		String stringAssistant;
		String stringCard;
		String stringTile;
		String supportString;

		int numberAssistants;
		int priceAssistants;

		tempString = input;
		HashMap<CardPolitic, List<Integer>> cards;
		CardPolitic tempCard;
		HashMap<TileBusinessPermit, List<Integer>> tiles;
		TileBusinessPermit tempTile;

		int tempPrice;

		try {
			stringAssistant = tempString.substring(0, tempString.indexOf('-'));
			tempString = tempString.substring(tempString.indexOf('-') + 1);
			stringCard = tempString.substring(0, tempString.indexOf('-'));
			tempString = tempString.substring(tempString.indexOf('-') + 1);
			stringTile = tempString;
		} catch (IndexOutOfBoundsException exception) {
			return "Invalid input format";
		}

		try {
			if (!"".equals(stringAssistant)) {
				numberAssistants = Integer.parseInt(stringAssistant.substring(0, stringAssistant.indexOf(':')));
				priceAssistants = Integer.parseInt(stringAssistant.substring(stringAssistant.lastIndexOf(':') + 1));
			} else {
				numberAssistants = 0;
				priceAssistants = 0;
			}
		} catch (NumberFormatException | IndexOutOfBoundsException exception) {
			return "Invalid input format";
		}

		if (numberAssistants < 0 || priceAssistants < 0
				|| numberAssistants > model.getCurrentPlayer().getAssistants()) {
			return "Invalid input format";
		}

		cards = new HashMap<>();

		while (!"".equals(stringCard)) {
			try {
				supportString = stringCard.substring(0, stringCard.indexOf(':'));

				if ("SPECIAL".equalsIgnoreCase(supportString)) {
					tempCard = new CardPolitic(null, true);
				} else {
					tempCard = new CardPolitic(ColorCouncillor.parseIn(supportString), false);
				}

				stringCard = stringCard.substring(stringCard.indexOf(':') + 1);

				if (stringCard.indexOf(':') == -1) {
					supportString = stringCard;
					stringCard = "";
				} else {
					supportString = stringCard.substring(0, stringCard.indexOf(':'));
					stringCard = stringCard.substring(stringCard.indexOf(':') + 1);
				}

				tempPrice = Integer.parseInt(supportString);

			} catch (NumberFormatException | IndexOutOfBoundsException exception) {
				return "Invalid input format";
			}

			if (tempPrice < 0) {
				return "Invalid price of a politic card";
			}

			if (cards.get(tempCard) == null) {
				cards.put(tempCard, new ArrayList<>());
			}

			cards.get(tempCard).add(tempPrice);
		}

		if (!cards.isEmpty()
				&& !model.getCurrentPlayer().haveCardsPolitic(new ArrayList<CardPolitic>(cards.keySet()))) {
			return "You haven't a politic card that you want to sell";
		}

		tiles = new HashMap<>();

		while (!"".equals(stringTile)) {
			try {
				supportString = stringTile.substring(0, stringTile.indexOf(':'));

				tempTile = model.getCurrentPlayer().getTileBusinessPermit(supportString);

				if (tempTile == null) {
					return "Invalid input format";
				}

				stringTile = stringTile.substring(stringTile.indexOf(':') + 1);

				if (stringTile.indexOf(':') == -1) {
					supportString = stringTile;
					stringTile = "";
				} else {
					supportString = stringTile.substring(0, stringTile.indexOf(':'));
					stringTile = stringTile.substring(stringTile.indexOf(':') + 1);
				}

				tempPrice = Integer.parseInt(supportString);
			} catch (IndexOutOfBoundsException | NumberFormatException exception) {
				return "Invalid input format";
			}

			if (tempPrice < 0) {
				return "Invalid price of a business permit tile";
			}

			if (model.getCurrentPlayer().getTileBusinessPermit().indexOf(tempTile) == -1) {
				return "You haven't a business permit tile that you want to sell";
			}

			if (tiles.get(tempTile) == null) {
				tiles.put(tempTile, new ArrayList<>());
			}

			tiles.get(tempTile).add(tempPrice);
		}

		model.addObjectsForSale(numberAssistants, priceAssistants, cards, tiles);
		changePlayer();

		return "OK";
	}

	// Controllata!
	/**
	 * Method to buy the object that current player wants to buy.
	 * 
	 * @param input
	 *            A specific string that indicates the input of current player.
	 *            <p>
	 *            "Name player":"A":"Number assistants"-"Name player"
	 *            :"C":"Card"-"Name player":"T":"Tile business permit".
	 *            </p>
	 *            Tile business permit: string representation of tile. Look
	 *            TileBusinessPermit.toString().<br>
	 *            Card: "FUCHSIA", "BLACK", "PURPLE", "BLUE", "ORANGE", "WHITE",
	 *            "SPECIAL".<br>
	 *            (The order of cards and tiles isn't important).
	 * @return Returns "OK" if there isn't problem in input of player. Else a
	 *         wrong message.
	 */
	public String roundMarket2(String input) {

		String tempString;
		String supportString;
		Map<Player, List<ObjectForSale>> sellers;
		Player tempPlayer;
		ColorCouncillor tempColor;
		CardPolitic tempCard;
		TileBusinessPermit tempTile;
		Integer tempNumber;
		Integer totalCoins;

		tempString = input;
		totalCoins = 0;
		tempPlayer = null;
		sellers = new HashMap<>();

		while (!("").equals(tempString)) {
			if (tempString.indexOf('-') == -1) {
				supportString = tempString;
				tempString = "";
			} else {
				supportString = tempString.substring(0, tempString.indexOf('-'));
				tempString = tempString.substring(tempString.indexOf('-') + 1);
			}

			try {
				for (Player player : model.getPlayers()) {
					if (player.getName().equalsIgnoreCase(supportString.substring(0, supportString.indexOf(':')))) {
						tempPlayer = player;
					}
				}
			} catch (IndexOutOfBoundsException exception) {
				return "Invalid input format";
			}

			if (tempPlayer == null) {
				return "Name of seller not valid";
			}

			try {
				supportString = supportString.substring(supportString.indexOf(':') + 1);

				if (supportString.substring(0, supportString.indexOf(':')).equalsIgnoreCase("A")) {

					supportString = supportString.substring(supportString.indexOf(':') + 1);
					tempNumber = Integer.parseInt(supportString);

					if (!sellers.containsKey(tempPlayer)) {
						sellers.put(tempPlayer, new ArrayList<ObjectForSale>());
					}
					sellers.get(tempPlayer).add(new ObjectForSale(tempNumber, 0));

				} else if (supportString.substring(0, supportString.indexOf(':')).equalsIgnoreCase("C")) {

					supportString = supportString.substring(supportString.indexOf(':') + 1);

					if (!sellers.containsKey(tempPlayer)) {
						sellers.put(tempPlayer, new ArrayList<ObjectForSale>());
					}

					if ("SPECIAL".equalsIgnoreCase(supportString)) {
						tempCard = new CardPolitic(null, true);
					} else {
						tempColor = ColorCouncillor.parseIn(supportString);

						if (tempColor == null) {
							return "Invalid input format";
						}

						tempCard = new CardPolitic(tempColor, false);
					}

					sellers.get(tempPlayer).add(new ObjectForSale(tempCard, 0));

				} else if (supportString.substring(0, supportString.indexOf(':')).equalsIgnoreCase("T")) {
					supportString = supportString.substring(supportString.indexOf(':') + 1);

					if (!sellers.containsKey(tempPlayer)) {
						sellers.put(tempPlayer, new ArrayList<ObjectForSale>());
					}

					tempTile = null;

					for (TileBusinessPermit tile : tempPlayer.getTileBusinessPermit()) {
						if (tile.toString().equalsIgnoreCase(supportString)) {
							tempTile = tile;

						}
					}
					if (tempTile != null) {
						sellers.get(tempPlayer).add(new ObjectForSale(tempTile, 0));
					} else {
						return "Invalid input format";
					}
				}

			} catch (IndexOutOfBoundsException exception) {
				return "Invalid input format";
			} catch (NumberFormatException exception) {
				return "Invalid input format";
			}
		}

		// Controllo che i giocatori selezionati abbiano effettivamente
		// l'oggetto tra quelli da vendere
		for (Player player : sellers.keySet()) {
			if (!player.haveForSale(sellers.get(player))) {
				return "A request to purchase isn't valid";
			}
		}

		// Controllo che il giocatore abbia abbastanza soldi per comprare tutto
		for (Player player : sellers.keySet()) {
			totalCoins += player.getPriceOfObjects(sellers.get(player));
		}

		if (totalCoins > model.getCurrentPlayer().getPositionCoinsTrack()) {
			return "You haven't enough coins to buy the elements that you choose.";
		}

		// A questo punto ho fatto tutti i controlli, quindi compro senza
		// problemi
		model.buyObjects(sellers);

		changePlayer();

		return "OK";
	}

	/**
	 * The normal round of game where current player can do a main action, a
	 * quick action and indicates another input for particular bonus.
	 * 
	 * @param input
	 *            A specific string that indicates the input of current player.
	 *            <p>
	 *            "Action"-"Parameter"
	 *            </p>
	 *            Action: <br>
	 *            0: None action. <br>
	 *            1: Elect councillor. <br>
	 *            2: Acquire business permit tile.<br>
	 *            3: Build an emporium with permit tile.<br>
	 *            4: Build an emporium with help of king.<br>
	 *            5: Engage an assistant.<br>
	 *            6: Change business permit tile.<br>
	 *            7: Send an assistant to elect a councillor.<br>
	 *            8: Perform an additional main action.<br>
	 *            9: Bonus: choose a city and take its token.<br>
	 *            10: Bonus: take a business permit tile.<br>
	 *            11: Bonus: take bonus of a your business permit tile.<br>
	 *            12: Bonus: choose 2 cities and take their token.<br>
	 *            Look specific method of each action to know the format of
	 *            parameters.
	 * @return Returns "OK" if there isn't problem in input of player. Else a
	 *         wrong message.
	 */
	public String roundNormal(String input) {

		String tempString;
		String output = "";
		String specialBonus;

		int action;

		tempString = input;
		specialBonus = model.getCurrentPlayer().getSpecialNobilityBonus();

		if (tempString == null) {
			throw new NullPointerException("Input of Round method cannot be null.");
		}

		tempString = tempString.toUpperCase();

		try {
			action = Integer.parseInt(tempString.substring(0, 1));
			tempString = tempString.substring(tempString.indexOf('-') + 1);
		} catch (IndexOutOfBoundsException | NumberFormatException exception) {
			return "Invalid input format";
		}

		if (action < 0 || action > 11) {
			return "Invalid input format";
		} else if (action == 0 && mainAction >= 1) {
			return "You must do a main action.";
		} else if (action >= 1 && action <= 4 && mainAction == 0) {
			return "You cannot do another main action.";
		} else if (action >= 5 && action <= 8 && !quickAction) {
			return "You cannot do another quick action.";
		} else if (action == 9 && !"BonusTokenReward1".equalsIgnoreCase(specialBonus)) {
			return "Action not valid.";
		} else if (action == 10 && !"BonusTakePermitTile".equalsIgnoreCase(specialBonus)) {
			return "Action not valid.";
		} else if (action == 11 && !"BonusTakePreviousBonus".equalsIgnoreCase(specialBonus)) {
			return "Action not valid.";
		} else if (action == 12 && !"BonusTokenReward2".equalsIgnoreCase(specialBonus)) {
			return "Action not valid.";
		}

		if (action == 1) {
			output = electCouncillor(tempString);
		} else if (action == 2) {
			output = aquireTileBusinessPermit(tempString);
		} else if (action == 3) {
			output = buildEmporiumPermitTile(tempString);
		} else if (action == 4) {
			output = buildEmporiumKing(tempString);
		} else if (action == 5) {
			output = engageAssistant();
		} else if (action == 6) {
			output = changeBusinessPermitTile(tempString);
		} else if (action == 7) {
			output = assistantForCouncillor(tempString);
		} else if (action == 8) {
			output = twoMainActions();
		} else if (action == 9) {
			output = takeBonusTokenReward(tempString, 1);
		} else if (action == 10) {
			output = takeBonusTakePermitTile(tempString);
		} else if (action == 11) {
			output = takeBonusTakePreviousBonus(tempString);
		} else if (action == 12) {
			output = takeBonusTokenReward(tempString, 2);
		} else if (action == 0) {
			output = "OK";
		}

		if (model.getCurrentPlayer().getDoubleAction()) {
			model.getCurrentPlayer().setDoubleAction(false);
			mainAction++;
		}

		if ("OK".equals(output)) {
			if (action >= 1 && action <= 4) {
				mainAction -= 1;
			} else if (action >= 5 && action <= 8) {
				quickAction = false;
			} else if (action > 8 && action <= 11) {
				model.getCurrentPlayer().setSpecialNobilityBonus("");
				specialBonus = "";
			} else if (action == 0) {
				model.getCurrentPlayer().setSpecialNobilityBonus("");
				specialBonus = "";
				quickAction = false;
			}

			if (mainAction == 0 && !quickAction && "".equals(specialBonus)) {
				changePlayer();
			}

		}
		return output;

	}

	// Portata in model!
	/**
	 * Method to elect a councillor.
	 * 
	 * @param parameter
	 *            Parameters of action 1.
	 *            <p>
	 *            "Balcony":"Color councillor"
	 *            </p>
	 *            Balcony: "SEASIDE", "HILLSIDE", "MOUNTAIN", "KINGBOARD".<br>
	 *            Color councillor: "FUCHSIA", "BLACK", "PURPLE", "BLUE",
	 *            "ORANGE", "WHITE".
	 * @return Returns "OK" if there isn't problem in input of player. Else a
	 *         wrong message.
	 */
	private String electCouncillor(String parameter) {
		String tempString;
		String balcony;
		ColorCouncillor color;

		tempString = parameter;

		try {
			balcony = tempString.substring(0, tempString.indexOf(':'));
			tempString = tempString.substring(tempString.indexOf(':') + 1);
		} catch (IndexOutOfBoundsException exception) {
			return "Invalid input format";
		}

		try {
			color = ColorCouncillor.parseIn(tempString.substring(0));
		} catch (IndexOutOfBoundsException exception) {
			return "Invalid input format";
		}

		if (model.getBoard().getCouncillors().indexOf(color) == -1) {
			return "There isn't a councillor available";
		}

		try {
			model.electCouncillor(color, balcony);
		} catch (WrongInputException | NotFoundException exception) {
			return exception.toString();
		}

		return "OK";
	}

	// Portata in model!
	/**
	 * Method to acquire an business permit tile.
	 * 
	 * @param parameter
	 *            Parameters of action 2.
	 *            <p>
	 *            "Region":"Tile business permit"
	 *            :"Card1":"Card2":"Card3":"Card4"
	 *            </p>
	 *            Region: "SEASIDE", "HILLSIDE", "MOUNTAIN".<br>
	 *            Card: "FUCHSIA", "BLACK", "PURPLE", "BLUE", "ORANGE", "WHITE",
	 *            "SPECIAL".<br>
	 *            Tile business permit: string representation of tile. Look
	 *            TileBusinessPermit.toString().
	 * @return Returns "OK" if there isn't problem in input of player. Else a
	 *         wrong message.
	 */
	private String aquireTileBusinessPermit(String parameter) {

		String tempString;
		String supportString;
		String chosenTile;
		TypeRegion typeRegion;
		ArrayList<CardPolitic> cards;
		int numberCoins = 0;
		tempString = parameter;

		// Check input region
		try {
			typeRegion = TypeRegion.parseIn(tempString.substring(0, tempString.indexOf(':')));
			tempString = tempString.substring(tempString.indexOf(':') + 1);
		} catch (IndexOutOfBoundsException exception) {
			return "Invalid input format1";
		}

		if (typeRegion == null) {
			return "Invalid input format2";
		}

		// Check string of permit tile
		try {
			supportString = tempString.substring(0, tempString.indexOf(':'));
			tempString = tempString.substring(tempString.indexOf(':') + 1);
		} catch (IndexOutOfBoundsException exception) {
			return "Invalid input format3";
		}

		if (model.getBoard().getRegion(typeRegion).getTileBusinessPermitA().toString()
				.equalsIgnoreCase(supportString)) {
			chosenTile = "A";
		} else if (model.getBoard().getRegion(typeRegion).getTileBusinessPermitB().toString()
				.equalsIgnoreCase(supportString)) {
			chosenTile = "B";
		} else {
			return "Invalid business permit tile";
		}

		// Check politic cards
		cards = new ArrayList<>();

		while (cards.size() < 4 && !("").equals(tempString)) {
			try {
				if (tempString.indexOf(':') != -1) {
					supportString = tempString.substring(0, tempString.indexOf(':'));
					tempString = tempString.substring(tempString.indexOf(':') + 1);
				} else {
					supportString = tempString;
					tempString = "";
				}

				if (ColorCouncillor.parseIn(supportString) == null && !("SPECIAL").equalsIgnoreCase(supportString)) {
					return "Invalid input format4";
				}

				if ("SPECIAL".equalsIgnoreCase(supportString)) {
					cards.add(new CardPolitic(null, true));
				} else {
					cards.add(new CardPolitic(ColorCouncillor.parseIn(supportString), false));
				}

			} catch (IndexOutOfBoundsException exception) {
				return "Invalid input format";
			}
		}

		if (cards.isEmpty()) {
			return "You must use at least one politic card";
		}

		// Check politic cards are valid for councillor
		if (!model.getBoard().getRegion(typeRegion).checkCardsMeetCouncil(cards)) {
			return "The politic cards that are selected, aren't valid";
		}

		if (!model.getCurrentPlayer().haveCardsPolitic(cards)) {
			return "You haven't all politic cards that you choose";
		}

		// Calculate coins and check
		if (cards.size() == 1) {
			numberCoins = 10;
		} else if (cards.size() == 2) {
			numberCoins = 7;
		} else if (cards.size() == 3) {
			numberCoins = 4;
		} else if (cards.size() == 4) {
			numberCoins = 0;
		}

		for (CardPolitic card : cards) {
			if (card.isSpecial()) {
				numberCoins++;
			}
		}

		if (model.getCurrentPlayer().getPositionCoinsTrack() < numberCoins) {
			return "Player haven't enough coins";
		}

		try {
			model.aquireTileBusinessPermit(chosenTile, typeRegion, cards, numberCoins);
		} catch (WrongInputException | NegativeException | NullPointerException exception) {
			return exception.getMessage();
		}

		return "OK";
	}

	// Portata in model!
	/**
	 * Method to build an emporium with business permit tile.
	 * 
	 * @param parameter
	 *            Parameters of action 3.
	 *            <p>
	 *            "Tile business permit":"City"
	 *            </p>
	 *            Tile business permit: string representation of tile. Look
	 *            TileBusinessPermit.toString().<br>
	 *            City: name of city.
	 * @return Returns "OK" if there isn't problem in input of player. Else a
	 *         wrong message.
	 */
	private String buildEmporiumPermitTile(String parameter) {

		String tempString;
		String chosenTile;
		String nameCity;
		TileBusinessPermit tileBusinessPermit;
		City city;

		tempString = parameter;

		// Check input
		try {
			chosenTile = tempString.substring(0, tempString.indexOf(':'));
			tempString = tempString.substring(tempString.indexOf(':') + 1);
		} catch (IndexOutOfBoundsException exception) {
			return "Invalid input format";
		}

		tileBusinessPermit = model.getCurrentPlayer().getTileBusinessPermit(chosenTile);

		if (tileBusinessPermit == null) {
			return "Invalid input format";
		}

		nameCity = tempString;

		// Don't use getCity of class board because I must check that region of
		// business permit tile is the same of city.
		city = model.getBoard().getRegion(tileBusinessPermit.getRegion()).getCity(nameCity);

		// Check that tile allows to build in this city
		if (tileBusinessPermit.getCities().indexOf(String.valueOf(nameCity.charAt(0))) == -1) {
			return "You can't use this business permit tile for build in the selected city.";
		}

		// Check there is the city
		if (city == null) {
			return "There isn't the selected city in region of business permit tile.";
		}

		// Check available emporium of actualPlayer
		if (model.getCurrentPlayer().getNumberAvailableEmporium() == 0) {
			return "You haven't available emporium to build";
		}

		if (city.getNumberOfBuildedEmporium() > model.getCurrentPlayer().getAssistants()) {
			return "You haven't enough assistants to build in this city";
		}

		model.buildEmporiumPermitTile(tileBusinessPermit, city);

		return "OK";
	}

	// Portata in model!
	/**
	 * Method to build an emporium with help of king.
	 * 
	 * @param parameter
	 *            Parameters of action 4.
	 *            <p>
	 *            "Card1":"Card2":"Card3":"Card4":"City":"City":"City"...
	 *            </p>
	 *            Card: "FUCHSIA", "BLACK", "PURPLE", "BLUE", "ORANGE", "WHITE",
	 *            "SPECIAL".<br>
	 *            City: name of city.
	 * @return Returns "OK" if there isn't problem in input of player. Else a
	 *         wrong message.
	 */
	private String buildEmporiumKing(String parameter) {

		String tempString;
		ArrayList<CardPolitic> cards;
		City city;
		int numberCoins;
		String supportString;

		tempString = parameter;
		cards = new ArrayList<>();

		while (cards.size() < 4 && !("").equals(tempString)) {
			try {
				if (tempString.indexOf(':') != -1) {
					supportString = tempString.substring(0, tempString.indexOf(':'));
				} else {
					supportString = tempString;

				}

				if (ColorCouncillor.parseIn(supportString) == null && !("SPECIAL").equalsIgnoreCase(supportString)) {
					break;
				}

				if ("SPECIAL".equalsIgnoreCase(supportString)) {
					cards.add(new CardPolitic(null, true));
				} else {
					cards.add(new CardPolitic(ColorCouncillor.parseIn(supportString), false));
				}

				if (!("").equals(tempString)) {
					tempString = tempString.substring(tempString.indexOf(':') + 1);
				}
			} catch (IndexOutOfBoundsException exception) {
				break;
			}
		}

		city = model.getBoard().getKingBoard().getKingCity();
		numberCoins = 0;

		while (!("").equals(tempString)) {
			if (tempString.indexOf(':') != -1) {
				supportString = tempString.substring(0, tempString.indexOf(':'));
				tempString = tempString.substring(tempString.indexOf(':') + 1);
			} else {
				supportString = tempString;
				tempString = "";
			}

			city = city.getNearbyCity(supportString);

			if (city == null) {
				return "City " + supportString + " not valid";
			}
			numberCoins += 2;
		}

		if (cards.isEmpty()) {
			return "You must use at least one politic card";
		}

		// Check politic cards are valid for councillor
		if (!model.getBoard().getKingBoard().checkCardsMeetCouncil(cards)) {
			return "The politic cards that are selected, aren't valid";
		}

		if (!model.getCurrentPlayer().haveCardsPolitic(cards)) {
			return "You haven't all politic cards that you choose";
		}

		// Calculate coins and check
		if (cards.size() == 1) {
			numberCoins += 10;
		} else if (cards.size() == 2) {
			numberCoins += 7;
		} else if (cards.size() == 3) {
			numberCoins += 4;
		} else if (cards.size() == 4) {
			numberCoins += 0;
		}

		for (CardPolitic card : cards) {
			if (card.isSpecial()) {
				numberCoins++;
			}
		}

		if (model.getCurrentPlayer().getPositionCoinsTrack() - numberCoins < 0) {
			return "You haven't enough coins to build the emporium";
		}

		if (city.hasEmporium(model.getCurrentPlayer())) {
			return "You already have an emporium on this city";
		}

		model.buildEmporiumKing(city, cards, numberCoins);

		return "OK";
	}

	// Portata in model!
	/**
	 * Method to engage an assistant.
	 * 
	 * @return Returns "OK" if there isn't problem in input of player. Else a
	 *         wrong message.
	 */
	private String engageAssistant() {

		if (model.getCurrentPlayer().getPositionCoinsTrack() < 3) {
			return "You haven't enough coins";
		}

		if (model.getBoard().getPoolAssistants() < 1) {
			return "There isn't enough assistants in the pool";
		}

		model.engageAssistant();

		return "OK";
	}

	// Portata nel model!
	/**
	 * Method to change business permit tile of a region
	 * 
	 * @param parameter
	 *            Parameters of action 6.
	 *            <p>
	 *            "Region"
	 *            </p>
	 *            Region: "SEASIDE", "HILLSIDE", "MOUNTAIN".
	 * @return Returns "OK" if there isn't problem in input of player. Else a
	 *         wrong message.
	 */
	private String changeBusinessPermitTile(String parameter) {

		TypeRegion typeRegion;

		typeRegion = TypeRegion.parseIn(parameter);

		if (typeRegion == null) {
			return "Invalid input format";
		}

		if (model.getCurrentPlayer().getAssistants() < 1) {
			return "You haven't enough assistants";
		}

		try {
			model.changeBusinessPermitTile(typeRegion);
		} catch (NullPointerException exception) {
			return exception.getMessage();
		}

		return "OK";
	}

	// Portata nel model!
	/**
	 * Method to send an assistant to elect a councillor.
	 * 
	 * @param parameter
	 *            Parameters of action 7.
	 *            <p>
	 *            "Balcony":"Color councillor"
	 *            </p>
	 *            Balcony: "SEASIDE", "HILLSIDE", "MOUNTAIN", "KINGBOARD".<br>
	 *            Color councillor: "FUCHSIA", "BLACK", "PURPLE", "BLUE",
	 *            "ORANGE", "WHITE".
	 * @return Returns "OK" if there isn't problem in input of player. Else a
	 *         wrong message.
	 */
	private String assistantForCouncillor(String parameter) {

		String tempString;
		String balcony;
		ColorCouncillor color;

		tempString = parameter;

		try {
			balcony = tempString.substring(0, tempString.indexOf(':'));
			tempString = tempString.substring(tempString.indexOf(':') + 1);
		} catch (IndexOutOfBoundsException exception) {
			return "Invalid input format";
		}

		color = ColorCouncillor.parseIn(tempString);

		if (color == null) {
			return "Invalid input format";
		}

		if (model.getCurrentPlayer().getAssistants() < 1) {
			return "You haven't enough assistants";
		}

		if (model.getBoard().getCouncillors().indexOf(color) == -1) {
			return "There isn't a " + color.toString() + " councillor available";
		}

		try {
			model.assistantForCouncillor(balcony, color);
		} catch (WrongInputException exception) {
			return exception.getMessage();
		}

		return "OK";
	}

	// Controllata!
	/**
	 * Method to perform an additional main action.
	 * 
	 * @return It's ever "OK".
	 */
	private String twoMainActions() {
		mainAction++;
		return "OK";
	}

	// Portata nel model!
	/**
	 * Method to get bonus of token of a city.
	 * 
	 * @param parameter
	 *            Parameters of action 9 and 12.
	 *            <p>
	 *            "City":"City"...
	 *            </p>
	 *            City: name of city.
	 * @param numberOfToken
	 *            Number of token that current player can take. Number of token
	 *            must be the same of number of city in parameter "parameter".
	 * @return Returns "OK" if there isn't problem in input of player. Else a
	 *         wrong message.
	 */
	private String takeBonusTokenReward(String parameter, int numberOfToken) {

		String tempString;
		City city;
		ArrayList<Bonus> tempBonusList;

		tempString = parameter;
		tempBonusList = new ArrayList<Bonus>();

		if ("".equals(tempString)) {
			return "OK";
		}

		for (int i = 0; i < numberOfToken; i++) {

			if (tempString.indexOf(':') != -1) {
				city = model.getBoard().getCity(tempString.substring(0, tempString.indexOf(':')));
				tempString = tempString.substring(tempString.indexOf(':') + 1);
				if (tempString.equalsIgnoreCase(city.getName())) {
					return "The cities cannot be the same city";
				}
			} else {
				city = model.getBoard().getCity(tempString);
			}

			if (city == null) {
				return "City not exist";
			}

			if (city.hasEmporium(model.getCurrentPlayer()) == false) {
				return "You haven't an emporium in this city";
			}

			for (Bonus bonus : city.getTokenReward().getBonus()) {
				if (bonus instanceof BonusNobility) {
					return "Invalid city because its token has a nobility bonus";
				}
			}

			tempBonusList.addAll(city.getTokenReward().getBonus());
		}

		model.takeBonusTokenReward(tempBonusList);

		return "OK";
	}

	// Portata nel model!
	/**
	 * Method to take a business permit tile.
	 * 
	 * @param parameter
	 *            Parameters of action 10.
	 *            <p>
	 *            "Tile business permit"
	 *            </p>
	 *            Tile business permit: string representation of tile. Look
	 *            TileBusinessPermit.toString().
	 * @return Returns "OK" if there isn't problem in input of player. Else a
	 *         wrong message.
	 */
	private String takeBonusTakePermitTile(String parameter) {

		TileBusinessPermit tile;

		tile = null;

		if ("".equals(parameter)) {
			return "OK";
		}

		for (Region region : model.getBoard().getRegion()) {

			if (region.getTileBusinessPermitA().toString().equalsIgnoreCase(parameter)) {
				try {
					tile = region.acquireTileBusinessPermit("A");
				} catch (WrongInputException exception) {
					return "Unexpected error";
				}

				model.getCurrentPlayer().getTileBusinessPermit().add(tile);
				break;
			} else if (region.getTileBusinessPermitB().toString().equalsIgnoreCase(parameter)) {
				try {
					tile = region.acquireTileBusinessPermit("B");
				} catch (WrongInputException exception) {
					return "Unexpected error";
				}

				model.takeBonusTakePermitTile(tile);
				break;
			}
		}

		if (tile == null) {
			return "Invalid input format";
		}

		return "OK";
	}

	// Controllata!
	/**
	 * Method to take bonus of an your business permit tile.
	 * 
	 * @param parameter
	 *            Parameters of action 11.
	 *            <p>
	 *            "Tile business permit"
	 *            </p>
	 *            Tile business permit: string representation of tile. Look
	 *            TileBusinessPermit.toString().
	 * @return Returns "OK" if there isn't problem in input of player. Else a
	 *         wrong message.
	 */
	private String takeBonusTakePreviousBonus(String parameter) {

		boolean finded;

		finded = false;

		if ("".equals(parameter)) {
			return "OK";
		}

		for (TileBusinessPermit tile : model.getCurrentPlayer().getTileBusinessPermit()) {
			if (tile.toString().equalsIgnoreCase(parameter)) {
				finded = true;
				model.takeBonusTakePreviousBonus(tile);
				break;
			}
		}

		if (!finded) {
			for (TileBusinessPermit tile : model.getCurrentPlayer().getRefuseTileBusinessPermit()) {
				if (tile.toString().equalsIgnoreCase(parameter)) {
					finded = true;
					model.takeBonusTakePreviousBonus(tile);
					break;
				}
			}
		}

		if (!finded) {
			return "Invalid input format";
		}

		return "OK";
	}
}