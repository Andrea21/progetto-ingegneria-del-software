package bonusandcards;

import model.*;
import java.io.Serializable;

/**
 * Class that represents a politic card. Card has a color of type
 * ColorCouncillor.
 * 
 * @author Federico
 *
 */
public class CardPolitic implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ColorCouncillor color;

	private boolean special;

	/**
	 * Class constructor. If card is special, is recommended to pass null to color.
	 * 
	 * @param color
	 *            Identifies the color of the card.
	 * @param special
	 *            Is true if the card is of the special kind.
	 */
	public CardPolitic(ColorCouncillor color, boolean special) {
		super();
		this.color = color;
		this.special = special;
	}

	/**
	 * Get the boolean special attribute.
	 * 
	 * @return The special attribute.
	 */
	public boolean isSpecial() {
		return special;
	}

	/**
	 * Get the color of the card.
	 * 
	 * @return The color of the card.
	 */
	public ColorCouncillor getColor() {
		return color;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CardPolitic)) {
			return false;
		}
		CardPolitic card;

		card = (CardPolitic) obj;

		if (card.isSpecial() && this.isSpecial())
			return true;
		if (card.getColor() == this.getColor())
			return true;
		return false;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public String toString() {
		if (!special) {
			return "CardPolitic " + color.toString();
		} else {
			return "CardPolitic SPECIAL";
		}
	}
}