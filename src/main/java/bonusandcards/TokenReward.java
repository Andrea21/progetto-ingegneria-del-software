package bonusandcards;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import model.Player;

/**
 * Class that represents a reward token of cities.
 * @author Federico
 *
 */
public class TokenReward implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Bonus> bonus;

	/**
	 * Class constructor
	 * 
	 * @param bonus
	 *            It's an array of bonuses (a token can have more than one bonus)
	 */
	public TokenReward(List<Bonus> bonus) {
		super();
		this.bonus = (ArrayList<Bonus>) bonus;
	}
	
	/**
	 * Get the bonuses of the token.
	 * 
	 * @return The bonuses of the token.
	 */
	public List<Bonus> getBonus() {
		return bonus;
	}
	
	/**
	 * Method that lets the player take all the bonuses in the tile
	 * 
	 * @param player
	 * 			  The player that gets the bonuses		
	 */
	public void doBonus(Player player) {
		for (Bonus b : bonus) {
			b.takeBonus(player);
		}
	}
	
	@Override
	public String toString() {
		String text;

		text = "";

		if (bonus != null) {
			for (Bonus b : bonus) {
				text = text + b.toString();
			}
		}
		return text;
	}
}
