package bonusandcards;
 
import model.Player;

/**
 * Class that represents a bonus to take bonus of a token reward of cities.
 * @author Federico
 *
 */
public class BonusTokenReward extends Bonus {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int numberOfTokenToTake;

	/**
	 * Class constructor
	 *            
	 * @param number
	 * 			  Is the number of token you can select 	           
	 */
	public BonusTokenReward( int number) {
		super();
		this.numberOfTokenToTake = number;
		
	}

	/**
	 * Sets the String attribute of the player 
	 * 
	 * @param player
	 *            The player who receives the bonus
	 */
	@Override
	public void takeBonus(Player player) {
		player.setSpecialNobilityBonus("BonusTokenReward" + numberOfTokenToTake);
	}
	
	/**
	 * Get the number of token to take.
	 * 
	 * @return The number of token to take.
	 */
	public int getnumberOfTokenToTake() {
		return numberOfTokenToTake;
	}

	@Override
	public String toString() {
		return " Bonus token reward";
	}
}
