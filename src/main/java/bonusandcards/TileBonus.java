package bonusandcards;
import java.io.Serializable;

/**
 * Class to represents a tile bonus that can be region bonus tile, color bonus tile or king bonus tile.
 * @author Federico
 *
 */
public abstract class TileBonus implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Bonus bonus;

	/**
	 * Class constructor.
	 * 
	 * @param bonus
	 *            The selected bonus in the tile
	 */
	public TileBonus(Bonus bonus) {
		super();
		this.bonus = bonus;
	}
	
	/**
	 * Get the bonus of the tile.
	 * 
	 * @return The bonus of the tile.
	 */
	public Bonus getBonus() {
		return bonus;
	}
}


