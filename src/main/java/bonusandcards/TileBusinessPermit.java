package bonusandcards;

import model.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that represents a business permit tile. It has bonus and list of initials of cities.
 * @author Federico
 *
 */
public class TileBusinessPermit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private TypeRegion region;

	private ArrayList<String> cities;

	private ArrayList<Bonus> bonus;

	/**
	 * Class constructor.
	 * 
	 * @param region
	 *            Region of business permit tile.
	 * @param cities
	 *            Initials of the name of the cities.
	 * @param bonus
	 *            List of bonus that a player take when he acquires the tile.
	 */
	public TileBusinessPermit(TypeRegion region, ArrayList<String> cities, ArrayList<Bonus> bonus) {
		this.region = region;
		this.cities = cities;
		this.bonus = bonus;
	}

	/**
	 * Class constructor.
	 * 
	 * @param region
	 *            Region of business permit tile.
	 * @param cities
	 *            Initials of the name of the cities.
	 */
	public TileBusinessPermit(TypeRegion region, ArrayList<String> cities) {
		this.region = region;
		this.cities = cities;
		bonus = new ArrayList<>();
	}

	/**
	 * Class constructor with an empty list of cities and list of bonus.
	 * @param region
	 */
	public TileBusinessPermit(TypeRegion region) {
		this.region = region;
		this.cities = new ArrayList<>();
		bonus = new ArrayList<>();
	}

	/**
	 * Takes all bonus of tile and gives them to player.
	 * @param player	Player that takes bonus.
	 */
	public void doBonus(Player player) {
		for (Bonus b : bonus) {
			b.takeBonus(player);
		}
	}

	/**
	 * Returns type of region of permit tile.
	 * @return	Type of region.
	 */
	public TypeRegion getRegion() {
		return region;
	}

	/**
	 * Returns list of initials of cities of permit tile.
	 * @return	List of initials.
	 */
	public List<String> getCities() {
		return cities;
	}

	/**
	 * Returns list of bonus of permit tile.
	 * @return	List of bonus.
	 */
	public List<Bonus> getBonus() {
		return bonus;
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof TileBusinessPermit)){
			return false;
		}
		
		TileBusinessPermit tile;
		
		tile = (TileBusinessPermit)obj;

		if (tile.getRegion() != this.region)
			return false;

		if (tile.getBonus().size() != this.bonus.size())
			return false;

		for (Bonus b : tile.getBonus()) {
			if (this.bonus.indexOf(b) == -1)
				return false;
		}

		if (tile.getCities().size() != this.cities.size())
			return false;

		for (String c : tile.getCities()) {
			if (this.cities.indexOf(c) == -1)
				return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public String toString() {
		String text;

		text = region.toString() + "|";

		if (cities != null) {
			for (String city : cities) {
				text = text + city + "/";
			}
		}

		if (text.length() > 0 && text.charAt(text.length() - 1) == '/') {
			text = text.substring(0, text.length() - 1);
		}

		if (bonus != null) {
			for (Bonus b : bonus) {
				text = text + b.toString();
			}
		}
		return text;
	}
}
