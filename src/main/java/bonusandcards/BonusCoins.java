package bonusandcards;

import model.*;

/**
 * Class that represents a bonus of coins.
 * @author Federico
 *
 */
public class BonusCoins extends Bonus {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int numberOfCoins;

	/**
	 * Class constructor.
	 * 
	 * @param numberOfCoins
	 *            The number of coins that the bonus provides.
	 */
	public BonusCoins(int numberOfCoins) {
		super();
		this.numberOfCoins = numberOfCoins;
	}

	/**
	 * Adds the number of coins of the bonus to the player coinpath.
	 * 
	 * @param player
	 *            The player who receives the bonus.
	 */
	@Override
	public void takeBonus(Player player) {
		player.addCoins(numberOfCoins);
	}

	/**
	 * Get the number of coins.
	 * 
	 * @return Number of coins.
	 */
	public int getNumberOfCoins() {
		return numberOfCoins;
	}

	@Override
	public String toString() {
		return " Bonus coins [" + numberOfCoins + "]";
	}

}