package bonusandcards;

import java.io.Serializable;

/**
 * Class that represents a reward tile of king.
 * @author Federico
 *
 */
public class TileKingsReward extends TileBonus implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int number;

	/**
	 * Class constructor
	 * 
	 * @param bonus
	 *            The selected bonus in the tile
	 * @param number
	 * 			  The number of the tile that provides the bonus	
	 */
	public TileKingsReward(Bonus bonus, int number) {
		super(bonus);
		this.number = number;
	}
	
	/**
	 * Get the number of the tile.
	 * 
	 * @return The number of the tile.
	 */
	public int getNumber()
	{
		return number;
	}
}
