package bonusandcards;

import model.*;

/**
 * Class that represents a bonus to advance on victory track.
 * @author Federico
 *
 */
public class BonusVictoryPoint extends Bonus {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int numberOfVictoryPoint;

	/**
	 * Class constructor
	 * 
	 * @param numberOfVictoryPoint
	 *            The number of victory points that the bonus provides
	 */
	public BonusVictoryPoint(int numberOfVictoryPoint) {
		super();
		this.numberOfVictoryPoint = numberOfVictoryPoint;
	}

	/**
	 * Adds the number of victory points of the bonus to the player victory point path
	 * 
	 * @param player
	 *            The player who receives the bonus
	 */
	@Override
	public void takeBonus(Player player){
		player.setPositionVictory(player.getPositionVictory() + numberOfVictoryPoint);
	}
	
	/**
	 * Get the number of victory points.
	 * 
	 * @return The number of victory points.
	 */
	public int getNumberOfVictoryPoint() {
		return numberOfVictoryPoint;
	}
	
	@Override
	public String toString() {
		return " Bonus victory point [" + numberOfVictoryPoint+"]";
	}
	
	
	
}