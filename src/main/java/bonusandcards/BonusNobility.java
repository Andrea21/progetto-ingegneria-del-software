package bonusandcards;

import model.*;

/**
 * Class that represents a bonus to advance on nobility track.
 * @author Federico
 *
 */
public class BonusNobility extends Bonus {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int numberOfNobilityPoint;

	/**
	 * Class constructor
	 * 
	 * @param icon
	 *            The selected icon of the bonus
	 * @param n
	 *            The number of nobility points the bonus provides
	 */
	public BonusNobility(int n) {
		super();
		numberOfNobilityPoint = n;
	}

	/**
	 * Adds the number of position that the bonus provides to the player nobility path
	 * 
	 * @param player
	 *            The player who receives the bonus
	 */
	@Override
	public void takeBonus(Player player) {
		player.setPositionNobility(player.getPositionNobility() + numberOfNobilityPoint);
	}

	/**
	 * Get the number of nobility points.
	 * 
	 * @return Number of nobility points
	 */
	public int getNumberOfNobilityPoint() {
		return numberOfNobilityPoint;
	}

	@Override
	public String toString() {
		return " Bonus nobility point [" + numberOfNobilityPoint + "]";
	}
}