package bonusandcards;

import model.ColorCity;

/**
 * Class that represents a color bonus tile. It has bonus and a color of cities.
 * @author Federico
 *
 */
public class TileBonusColor extends TileBonus {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ColorCity color;

	/**
	 * Class constructor.
	 * 
	 * @param bonus
	 *            The selected bonus in the tile
	 * @param color
	 * 			  The color of the city that provides the bonus	
	 */
	public TileBonusColor(Bonus bonus, ColorCity color) {
		super(bonus);
		this.color = color;
	}
	
	/**
	 * Get the color of the city in the tile.
	 * 
	 * @return The color of the city in the tile.
	 */
	public ColorCity getColor() {
		return color;
	}
	
	@Override
	public String toString() {
		return "Tile bonus color " + color.toString();
	}
	
}
