package bonusandcards;

import java.io.Serializable;

import model.*;

/**
 * Class that represents a type of bonus. These bonus are used in permit tile,
 * token reward, nobility track, etc...
 * 
 * @author Federico
 *
 */
public abstract class Bonus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Class constructor creates the icon of the bonus
	 * 
	 * @param icon
	 *            Identifies the bonus
	 */
	public Bonus() {
	}

	/**
	 * Methods that lets the player take some type of bonus
	 * 
	 * @param player
	 *            Player that takes bonus.
	 * 
	 */
	public abstract void takeBonus(Player player);

	/**
	 * Method that selects a type of bonus from a string
	 * 
	 * @param typeBonus
	 *            Describes the type of bonus
	 * @param number
	 *            Some types of bonus require a number of elements
	 * 
	 * @return The selected type of bonus
	 */
	public static Bonus bonusByString(String typeBonus, int number) {
		Bonus bonus = null;

		if ("Coins".equalsIgnoreCase(typeBonus)) {
			bonus = new BonusCoins(number);
		} else if ("Victory".equalsIgnoreCase(typeBonus)) {
			bonus = new BonusVictoryPoint(number);
		} else if ("Assistants".equalsIgnoreCase(typeBonus)) {
			bonus = new BonusTakeAssistant(number);
		} else if ("PoliticCards".equalsIgnoreCase(typeBonus)) {
			bonus = new BonusTakeCardPolitic(number);
		} else if ("Nobility".equalsIgnoreCase(typeBonus)) {
			bonus = new BonusNobility(number);
		} else if ("DoubleAction".equalsIgnoreCase(typeBonus)) {
			bonus = new BonusDoubleAction();
		} else if ("BonusTokenReward".equalsIgnoreCase(typeBonus)) {
			bonus = new BonusTokenReward(number);
		} else if ("BonusTakePreviousBonus".equalsIgnoreCase(typeBonus)) {
			bonus = new BonusTakePreviousBonus();
		} else if ("BonusTakePermitTile".equalsIgnoreCase(typeBonus)) {
			bonus = new BonusTakePermitTile();
		}

		return bonus;
	}
}
