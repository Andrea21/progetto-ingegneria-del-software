package bonusandcards;
 
import model.Player;

/**
 * Class that represents a bonus to take business permit tiles.
 * @author Federico
 *
 */
public class BonusTakePermitTile extends Bonus {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	/**
	 * Class constructor.
	 */
	public BonusTakePermitTile() {
		super();		
	}

	/**
	 * Sets the String attribute of the player 
	 * 
	 * @param player
	 *            The player who receives the bonus
	 */
	@Override
	public void takeBonus(Player player){
		player.setSpecialNobilityBonus("BonusTakePermitTile");
	}

	
	@Override
	public String toString() {
		return "Bonus take permit tile";
	}
	
	

}
