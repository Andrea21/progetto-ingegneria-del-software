package bonusandcards;

import model.*;

/**
 * Class that represents a bonus to take politic cards.
 * @author Federico
 *
 */
public class BonusTakeCardPolitic extends Bonus {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int numberOfCards;

	/**
	 * Class constructor
	 * 
	 * @param numberOfCards
	 *            The number of politic cards that the bonus provides
	 */
	public BonusTakeCardPolitic(int numberOfCards) {
		super();
		this.numberOfCards = numberOfCards;
	}

	/**
	 * Makes the player draw the politic card of the bonus
	 * 
	 * @param player
	 *            The player who receives the bonus
	 * @exception InfinityLoopException
	 *            If the player tries to draw from an empty deck
	 */
	@Override
	public void takeBonus(Player player){
		for(int i=0; i<numberOfCards; i++){
		player.drawCardPolitic();
		}
	}
	
	/**
	 * Get the number of politic cards.
	 * 
	 * @return The number of politic cards.
	 */
	public int getNumberOfCards() {
		return numberOfCards;
	}
	
	@Override
	public String toString() {
		return " Bonus politic cards [" + numberOfCards + "]";
	}
}





