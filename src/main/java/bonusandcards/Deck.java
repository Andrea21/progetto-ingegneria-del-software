package bonusandcards;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.Stack;

/**
 * Class that represents a collection of element that behave as a deck of cards.
 * Class has two collections of element: one is the deck and the other is the
 * refuse deck. You draw a card and lifted from deck, then you can add it to
 * refuse deck. If you set the property InfinityLoop to True, when deck is
 * finished, deck is filled by refuse deck and mixed.
 * 
 * @author Federico
 *
 * @param <E>
 *            Type of element of deck.
 */
public class Deck<E> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Stack<E> mainStack;

	private Stack<E> refuseStack;

	private boolean infinityLoop;

	/**
	 * Class constructors. Deck and refuse deck are empty.
	 * 
	 * @param infinityLoop
	 *            Is true if the deck needs to be reformed once totally used.
	 */
	public Deck(boolean infinityLoop) {
		this.mainStack = new Stack<E>();
		this.refuseStack = new Stack<E>();
		this.infinityLoop = infinityLoop;
	}

	/**
	 * Class constructors. It sets deck and refuse deck with parameter.
	 * 
	 * @param mainStack
	 *            Main deck.
	 * @param refuseStack
	 *            Refuse deck.
	 * @param infinityLoop
	 *            Is true if the deck needs to be reformed once totally used.
	 */
	public Deck(Stack<E> mainStack, Stack<E> refuseStack, boolean infinityLoop) {
		if (refuseStack == null) {
			throw new NullPointerException("Refuse stack cannot be null");
		}
		if (mainStack == null) {
			throw new NullPointerException("Main stack cannot be null");
		}
		this.mainStack = mainStack;
		this.refuseStack = refuseStack;
		this.infinityLoop = infinityLoop;

	}

	/**
	 * Class constructors. It sets deck with parameter and refuse deck is empty.
	 * 
	 * @param mainStack
	 *            Main deck.
	 * @param infinityLoop
	 *            Is true if the deck needs to be reformed once totally used.
	 */
	public Deck(Stack<E> mainStack, boolean infinityLoop) {
		if (mainStack == null) {
			throw new NullPointerException("Main stack cannot be null");
		}
		this.mainStack = mainStack;
		this.refuseStack = new Stack<E>();
		this.infinityLoop = infinityLoop;
	}

	/**
	 * Method that lets you draw a card and lifted from deck.
	 * 
	 * @exception InfinityLoopException
	 *                Checks if the player is trying to draw from an empty deck.
	 * 
	 * @return A card.
	 */
	public E draw() throws InfinityLoopException {
		if (mainStack.isEmpty()) {
			if (infinityLoop && !refuseStack.empty()) {
				takeFromRefuse();
				mixMainStack();
			} else {
				throw new InfinityLoopException("The deck is empty!");

			}

		}
		return mainStack.pop();
	}

	/**
	 * Method that checks if the deck is empty
	 */
	public boolean isEmpty() {
		return mainStack.empty();
	}

	/**
	 * Method that adds a card to the refuse deck.
	 * @param e	Element to add to the refuse deck.
	 */
	public void addRefuse(E e) {
		refuseStack.push(e);
	}

	/**
	 * Method that shuffles the main stack.
	 */
	public void mixMainStack() {
		Collections.shuffle(mainStack); // Check out java.util.collections
	}

	/**
	 * Adds an element on top of main deck.
	 * @param item	Element to add to main deck.
	 */
	public void add(E item) {
		mainStack.push(item);
	}

	/**
	 * Add an element on bottom of main deck.
	 * @param item	Element to add to main deck.
	 */
	public void addToBottomDeck(E item) {
		Stack<E> tempDeck = new Stack<E>();
		tempDeck.push(item);
		for (E e : mainStack) {
			tempDeck.push(e);
		}
		mainStack = tempDeck;
	}

	/**
	 * Method that reforms the main stack with the cards in the refuse stack.
	 */
	public void takeFromRefuse() {
		for (E e : refuseStack) {
			mainStack.push(e);
		}
	}

	/**
	 * Returns an iterator for available item of deck.
	 * 
	 * @return Iterator for available item.
	 */
	public Iterator<E> getItem() {
		return mainStack.iterator();
	}

	/**
	 * Returns an iterator for refuse item.
	 * 
	 * @return Iterator for refuse item.
	 */
	public Iterator<E> getRefuseItem() {
		return refuseStack.iterator();
	}
}
