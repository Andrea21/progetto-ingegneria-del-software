package bonusandcards;

import model.*;

/**
 * Class that represents a bonus to take assistants.
 * @author Federico
 *
 */
public class BonusTakeAssistant extends Bonus {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int numberOfAssistant;

	/**
	 * Class constructor
	 * 
	 * @param numberOfAssistant
	 *            The number of assistants that the bonus provides
	 */
	public BonusTakeAssistant(int numberOfAssistant) {
		super();
		this.numberOfAssistant = numberOfAssistant;
	}

	/**
	 * Adds the number of assistants of the bonus to the player assistants reserves
	 * 
	 * @param player
	 *            The player who receives the bonus
	 */
	@Override
	public void takeBonus(Player player){
		player.addAssistants(numberOfAssistant);
	}
	
	/**
	 * Get the number of assistants.
	 * 
	 * @return The number of assistants.
	 */
	public int getNumberOfAssistants() {
		return numberOfAssistant;
	}
	
	@Override
	public String toString()
	{
		return " Bonus assistants [" + numberOfAssistant + "]";
	}
}





