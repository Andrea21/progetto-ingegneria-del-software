package bonusandcards;

import model.*;

/**
 * Class that represents a bonus that permit another main action.
 * @author Federico
 *
 */
public class BonusDoubleAction extends Bonus {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Class constructor.
	 */
	public BonusDoubleAction() {
		super();
	}

	/**
	 * Sets the boolean attribute that lets the player do a double action to
	 * true.
	 * 
	 * @param player
	 *            The player who receives the bonus.
	 */
	@Override
	public void takeBonus(Player player) {
		player.setDoubleAction(true);
	}

	@Override
	public String toString() {
		return " Bonus double action";
	}
}
