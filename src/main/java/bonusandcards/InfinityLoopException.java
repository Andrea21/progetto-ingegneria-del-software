package bonusandcards;

public class InfinityLoopException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InfinityLoopException()
	{ 
		super(); 
	} 
	public InfinityLoopException(String s)
	{ 
		super(s);
	} 
}