package bonusandcards;
 
import model.Player;

/**
 * Class that represents a bonus to take a previous bonus of a permit tile of the player.
 * @author Federico
 *
 */
public class BonusTakePreviousBonus extends Bonus {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	/**
	 * Class constructor.
	 */
	public BonusTakePreviousBonus() {
		super();	
	}

	/**
	 * Sets the String attribute of the player 
	 * 
	 * @param player
	 *            The player who receives the bonus
	 */
	@Override
	public void takeBonus(Player player) {
		player.setSpecialNobilityBonus("BonusTakePreviousBonus");
	}

	
	@Override
	public String toString() {
		return " Bonus take previous bonus";
	}
}
