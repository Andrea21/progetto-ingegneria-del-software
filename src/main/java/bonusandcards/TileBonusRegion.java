package bonusandcards;

import model.TypeRegion;

/**
 * Class that represents a region bonus tile. It has bonus and an attribute that is type of region
 * @author Federico
 *
 */
public class TileBonusRegion extends TileBonus {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TypeRegion region;

	/**
	 * Class constructor.
	 * 
	 * @param bonus
	 *            The selected bonus in the tile.
	 * @param region
	 * 			  The type of the region that provides the bonus.
	 */
	public TileBonusRegion(Bonus bonus, TypeRegion region) {
		super(bonus);
		this.region = region;
	}

	/**
	 * Get the type of the region in the tile.
	 * 
	 * @return The type of the region in the tile.
	 */
	public TypeRegion getRegion() {
		return region;
	}
	
	@Override
	public String toString() {
		return "Tile bonus region " + region.toString();
	}
}
