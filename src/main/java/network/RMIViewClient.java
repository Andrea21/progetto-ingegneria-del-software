package network;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import model.Model;
import view.Log;
import view.Observer;

/**
 * Class to connect with RMI lobby and then with a RMI server view (RMIViewServer class).
 * @author Federico
 *
 */
public class RMIViewClient extends UnicastRemoteObject implements Observer, IViewRMIClient {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Observer> observers;
	private transient IViewRMIServer viewRMIServer;
	private transient ILobbyRMIServer lobbyRMIServer;
	boolean initialization;
	private String host;
	private String name;
	
	/**
	 * Constructor of class. Initialized client to be called by RMI server view and connect with RMI lobby.
	 * @param ipHost	IP address of server. 
	 * @throws Exception	If there are some problems with initialization or connection.
	 */
	public RMIViewClient(String ipHost) throws Exception {
		
		host = ipHost;
		observers = new ArrayList<>();
		name="";
		try {
			lobbyRMIServer = (ILobbyRMIServer) Naming.lookup("//" + host + "/Lobby");
		} catch (RemoteException exception) {
			Log.logger.info("Host not found");
			throw exception;
		} catch (MalformedURLException | NotBoundException exception) {
			Log.logger.info("Host not valid.");
			throw exception;
		}
		
		initialization = true;
	}

	/**
	 * Register a new observer.
	 * @param observer	Observer to register.
	 */
	public void register(Observer observer) {
		observers.add(observer);
	}

	/**
	 * Unregister an observer.
	 * @param observer	Observer to unregister.
	 */
	public void unregister(Observer observer) {
		observers.remove(observer);
	}
	
	/**
	 * Not used because client doesn't send a model to server.
	 */
	@Override
	public void update(Model model) {
		throw new UnsupportedOperationException("Client can't send a model to server");
	}

	/**
	 * Method called by a view. It calls method updateRMI of RMI server view.
	 */
	@Override
	public void update(String message) {
		try {
			viewRMIServer.updateRMI(message);
		} catch (RemoteException exception) {
			Log.logger.severe("Error on RMI invocation");
			return;
		}
	}

	/**
	 * Method to initialized player on lobby. After this, RMI client view communicates with RMI server view, and player is inserted in a game.
	 * @param name	Name of player.
	 * @param configuration	Configuration of game
	 * @return	Return "Waiting" if there aren't problems, else a message of problem.
	 */
	public String initialize(String name,String configuration){
		String answer;
		
		if (!"".equals(this.name)){
			try {
				Naming.unbind("ClientRMI"+this.name);
			} catch (Exception exception) {
				Log.logger.severe("Error on initialized RMI client: " + exception.getMessage());
				return "Error on initialized RMI client: " + exception.getMessage();
			}
		}
		
		this.name = name;
		
		try {
			Naming.rebind("ClientRMI"+name, this);
		} catch (Exception exception) {
			Log.logger.severe("Error on initialized RMI client: " + exception.getMessage());
			return "Error on initialized RMI client: " + exception.getMessage();
		}
		
		try {
		 	answer = lobbyRMIServer.newPlayer(name, configuration);
		} catch (RemoteException exception) {
			Log.logger.severe("Error on RMI invocation: " + exception.getMessage());
			return exception.getMessage();
		}
		
		try {
			viewRMIServer = (IViewRMIServer) Naming.lookup("//" + host + "/Server"+name);
		} catch (MalformedURLException|RemoteException|NotBoundException exception) {
			Log.logger.severe("Error on initialization of player:" +exception.getMessage());
			answer = exception.getMessage();
			return answer;
		}
		
		if (!"OK".equals(answer)){
			initialization=false;
			return answer;
		}
		
		Log.logger.info("Player initialized");
		return "Waiting";
		
	}

	/**
	 *	Method called by RMI server view to send an object model. Then, method notifies object model to all observers.
	 */
	@Override
	public void notifyModel(Model model) throws RemoteException {
		for (Observer observer: observers){
			observer.update(model);
		}
	}

	/**
	 * Method called by RMI server view to send a message. Then, method notifies message to all observer.
	 */
	@Override
	public void notifyMessage(String message) throws RemoteException {
		for (Observer observer: observers){
			observer.update(message);
		}
	}
	
	/**
	 * Method called by RMI server view to check connection.
	 */
	@Override
	public String checkConnection() throws RemoteException {
		return "OK";
	}
}
