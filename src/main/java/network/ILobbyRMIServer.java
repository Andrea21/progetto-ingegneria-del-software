package network;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface for an RMI lobby
 * @author Federico
 *
 */
public interface ILobbyRMIServer extends Remote {
	/**
	 * RMI method to add a new RMI client and initialized it.
	 * @param name	Name of player.
	 * @param configuration Configuration of game.
	 * @return	Return a message for client on result of initialization. "OK" if initialization succeeded.
	 * @throws RemoteException	If there is a problem with RMI connection.
	 */
	String newPlayer(String name, String configuration) throws RemoteException;
}
