package network;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Date;

import model.Model;
import view.Log;
import view.Observable;
import view.Observer;

/**
 * Class to create a server socket view that manages a socket connection with
 * socket client view (ViewSocketClient class).
 * 
 * @author Federico
 *
 */
public class ViewSocketServer extends Observable implements Observer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private transient Socket connection;
	private transient InputStream streamIn;
	private transient OutputStream streamOut;
	private String error;
	private transient Listener listener;
	private Date lastActivity;

	/**
	 * Constructor of class. Extract streams from socket connection and start a
	 * thread to listen the network.
	 * 
	 * @param clientConnection
	 */
	public ViewSocketServer(Socket clientConnection) {
		connection = clientConnection;
		try {
			streamIn = connection.getInputStream();
			streamOut = connection.getOutputStream();
		} catch (IOException exception) {
			errorManagement(exception);
		}

		listener = new Listener(streamIn);
		listener.start();

		error = "";

		lastActivity = new Date();
		Log.logger.info("New view socket with client " + connection.getRemoteSocketAddress().toString());
	}

	/**
	 * Returns ip address of client.
	 * @return	Ip address.
	 */
	public String getIpClient(){
		return connection.getInetAddress().toString();
	}
	
	/**
	 * Method called by observer and it sends an object model to socket client
	 * view.
	 */
	@Override
	public void update(Model model) {
		ObjectOutputStream objectOutputStream;
		try {
			objectOutputStream = new ObjectOutputStream(streamOut);
			objectOutputStream.writeObject(model);
		} catch (IOException | NullPointerException | SecurityException exception) {
			errorManagement(exception);
		}

		Log.logger.fine("To client object model");
	}

	/**
	 * Method called by observer and it sends a message to socket client view.
	 */
	@Override
	public void update(String message) {
		ObjectOutputStream objectOutputStream;
		try {
			objectOutputStream = new ObjectOutputStream(streamOut);
			objectOutputStream.writeObject(message);
		} catch (IOException | NullPointerException | SecurityException exception) {
			errorManagement(exception);
		}

		lastActivity = new Date();

		Log.logger.fine("To client " + connection.getRemoteSocketAddress().toString() + ": " + message);
	}

	/**
	 * Get time of last activity of connection.
	 * 
	 * @return Time of last activity.
	 */
	public Date getLastActivity() {
		return lastActivity;
	}

	/**
	 * Report a method to outside.
	 * 
	 * @return "" if there aren't errors, else text of error.
	 */
	public String getError() {
		if ("".equals(error) && !connection.isConnected()) {
			return "Connection lost";
		}
		return error;
	}

	/**
	 * Manage an error on communication.
	 * 
	 * @param exception
	 *            Exception receipt.
	 */
	private void errorManagement(Exception exception) {
		error = exception.getMessage();
		Log.logger.severe("Error with client " + connection.getRemoteSocketAddress().toString() + ":" + error);
		try {
			connection.close();
		} catch (IOException exception2) {
			connection = null;
		}
		streamIn = null;
		streamOut = null;
	}

	/**
	 * Close connection and server. It sends a message of closing ("CLOSE") to
	 * socket client view and sends signal to listener thread.
	 */
	public void close() {
		String message = "CLOSE";
		ObjectOutputStream objectOutputStream;

		try {
			objectOutputStream = new ObjectOutputStream(streamOut);
			objectOutputStream.writeObject(message);
		} catch (IOException exception) {
			errorManagement(exception);
			return;
		}

		listener.close();
		Log.logger.info("Client " + connection.getRemoteSocketAddress().toString() + " cloesed");

		try {
			connection.close();
		} catch (IOException exception) {
			connection = null;
		}
		streamIn = null;
		streamOut = null;
	}

	/**
	 * Close connection and server. It sends a message of closing ("CLOSE") with
	 * a specific message to socket client view and sends signal to listener
	 * thread.
	 * 
	 * @param message
	 *            Message concatenated with default message of closing.
	 */
	public void close(String message) {
		String messageClose = "CLOSE" + message;
		ObjectOutputStream objectOutputStream;

		try {
			objectOutputStream = new ObjectOutputStream(streamOut);
			objectOutputStream.writeObject(messageClose);
		} catch (IOException exception) {
			errorManagement(exception);
			return;
		}

		listener.close();

		Log.logger.info("Client " + connection.getRemoteSocketAddress().toString() + " cloesed");

		try {
			connection.close();
		} catch (IOException exception) {
			connection = null;
		}
		streamIn = null;
		streamOut = null;
	}

	/**
	 * Class to listen network on a separate thread.
	 * 
	 * @author Federico
	 *
	 */
	public class Listener extends Thread {
		InputStream streamIn;
		boolean close;

		/**
		 * Constructor of class.
		 * 
		 * @param inputStream
		 *            Input stream of socket connection.
		 */
		public Listener(InputStream inputStream) {
			streamIn = inputStream;
			close = false;
		}

		/**
		 * Set to True the variable "close" that is read by thread to know when
		 * it must end.
		 */
		public void close() {
			close = true;
		}

		/**
		 * Method that listens network and notifies to observers a string or a
		 * model.
		 */
		@Override
		public void run() {
			ObjectInputStream objectInputStream;
			Object object;
			while (!close) {
				try {
					objectInputStream = new ObjectInputStream(streamIn);
					object = objectInputStream.readObject();
					if (object instanceof String) {
						notifyObservers((String) object);
						Log.logger.info("From client " + connection.getRemoteSocketAddress().toString() + ": "
								+ (String) object);
					} else if (object instanceof Model) {
						notifyObservers((Model) object);
						Log.logger.info(
								"From client " + connection.getRemoteSocketAddress().toString() + " object model");
					}
				} catch (Exception exception) {
					errorManagement(exception);
					return;
				}
				lastActivity = new Date();
			}
		}
	}

}
