package network;

import java.rmi.Remote;
import java.rmi.RemoteException;

import model.Model;

/**
 * Interface for RMI client view.
 * @author Federico
 *
 */
public interface IViewRMIClient  extends Remote{
	/**
	 * Method called by RMI server view to send an object model.
	 * @param model	Object model.
	 * @throws RemoteException	If there are some problems with RMI connection.
	 */
	void notifyModel(Model model) throws RemoteException;
	/**
	 * Method called by RMI server view to send a message.
	 * @param message	Text of message.
	 * @throws RemoteException	If there are some problems with RMI connection.
	 */
	void notifyMessage(String message) throws RemoteException;
	/**
	 * Method called by RMI server view to check that connection are available.
	 * @return	Return "OK".
	 * @throws RemoteException	If there are some problems with RMI connection.
	 */
	String checkConnection() throws RemoteException;
}
