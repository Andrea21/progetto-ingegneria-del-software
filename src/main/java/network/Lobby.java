package network;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.RemoteServer;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import model.WrongInputException;
import utility.FileUtility;
import view.Log;

/**
 * Class to initialized and manage all new connection with socket or RMI. It
 * also creates new games and manages it.
 * 
 * @author Federico
 *
 */
public class Lobby extends UnicastRemoteObject implements ILobbyRMIServer {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Game> games;
	private transient GameManagement gameManagement;
	private long index;
	private int minuteLastActivity;

	/**
	 * Constructor of lobby class. It's created and started a gameManagement
	 * thread.
	 * @param minuteToRemovePlayer Minutes to wait before removing a player from game.
	 * @throws RemoteException	If there are problems when client RMI calls this method.
	 */
	public Lobby(int minuteToRemovePlayer) throws RemoteException {
		games = new ArrayList<>();
		gameManagement = new GameManagement();
		gameManagement.start();
		index = 0;
		minuteLastActivity = minuteToRemovePlayer;
		Log.logger.info("Lobby start");
	}

	/**
	 * Method checks if there is player with the same name of parameter.
	 * 
	 * @param name
	 *            Name to find
	 * @return True if there is a player with the same name. Else false.
	 */
	public synchronized boolean checkName(String name) {
		Iterator<String> iterator;

		for (Game game : games) {
			iterator = game.getNamePlayerSocket();
			while (iterator.hasNext()) {
				if (iterator.next().equals(name)) {
					return true;
				}
			}
			iterator = game.getNamePlayerRMI();
			while (iterator.hasNext()) {
				if (iterator.next().equals(name)) {
					return true;
				}
			}
		}
		return false;
	}

	public synchronized boolean checkIp(String ip){
		Iterator<ViewSocketServer> iterator;

		for (Game game : games) {
			iterator = game.getViewPlayerSocket();
			if (iterator==null){
				return false;
			}
			while (iterator.hasNext()) {
				if (iterator.next().getIpClient().equals(ip)){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Method starts a new PlayerInitialize thread, to initialize the new
	 * client.
	 * 
	 * @param connection
	 *            Socket of new client.
	 * @throws IOException
	 *             If there are errors in the extraction of the streams from
	 *             socket connection.
	 */
	public void newPlayer(Socket connection) throws IOException {
		PlayerInitialize playerInitialize;

		playerInitialize = new PlayerInitialize(connection);

		playerInitialize.start();
	}

	@Override
	public String newPlayer(String name, String configuration) throws RemoteException {

		RMIViewServer rmiViewServer;
		boolean founded;
		String ipClient;

		try {
			ipClient = RemoteServer.getClientHost();
		} catch (ServerNotActiveException exception) {
			Log.logger.severe("Error on extraction ip of client RMI: " + exception.getMessage());
			return "Error on extraction ip of client RMI: " + exception.getMessage();
		}

		if (checkName(name)) {
			return "Name is already in use. Choose another name";
		}

		try {
			rmiViewServer = new RMIViewServer(name, ipClient);
		} catch (Exception exception) {
			Log.logger.severe("Error on creation of RMI server view: " + exception.getMessage());
			return "Error on creation of RMI server view: " + exception.getMessage();
		}

		try {
			Naming.rebind("Server" + name, rmiViewServer);
		} catch (Exception exception) {
			Log.logger.severe("Error in initialized RMI player " + name + ": " + exception.getMessage());
			return "Error in initialized RMI player " + name + ": " + exception.getMessage();
		}

		Log.logger.info("New RMI client: " + name + ". He choosen configuration: " + configuration);

		founded = false;
		for (Game game : games) {
			if (game.getConfiguration().equals(configuration) && game.getStatus() == Game.State.WAITING) {
				if (game.newPlayer(rmiViewServer, name)) {
					founded = true;
					break;
				}
			}
		}

		if (!founded) {
			try {
				newGame(configuration, rmiViewServer, name);
			} catch (WrongInputException | IOException exception) {
				Log.logger.severe("Error on creation of game, configuration: " + configuration);
				return "Configuration not valid";
			}
		}

		Log.logger.info("Player " + name + " initialized");

		return "OK";
	}

	/**
	 * Create a new game and add it on list of list of game.
	 * 
	 * @param configuartion
	 *            Name of configuration of game.
	 * @throws WrongInputException
	 *             If configuration file isn't found or isn't a file xml.
	 * @throws IOException
	 *             If not found attributes 'Player' in tag element 'Game' in
	 *             Configuration.xml file.
	 */
	public synchronized void newGame(String configuartion) throws WrongInputException, IOException {
		Game game;
		Document document;
		Integer numberPlayers;

		try {
			document = FileUtility.openXML("Configuration//" + configuartion + "//Configuration.xml");
		} catch (Exception exception) {
			throw new WrongInputException("Error on creation of game");
		}

		try {
			numberPlayers = Integer
					.parseInt(((Element) (document.getElementsByTagName("Game").item(0))).getAttribute("Players"));
		} catch (Exception exception) {
			throw new IOException("Error on configuration file");
		}

		game = new Game(configuartion, numberPlayers, index);
		index++;
		games.add(game);

		if (index >= Long.MAX_VALUE - 5) {
			index = 0;
		}
	}

	/**
	 * Create a new game and add it on list of list of game. Then add a player.
	 * on game.
	 * 
	 * @param configuartion
	 *            Name of configuration of game.
	 * @param viewSocketServer
	 *            Socket view of player.
	 * @param name
	 *            Name of player.
	 * @throws WrongInputException
	 *             If configuration file isn't found or isn't a file xml.
	 * @throws IOException
	 *             If not found attributes 'Player' in tag element 'Game' in
	 *             Configuration.xml file.
	 */
	public synchronized void newGame(String configuartion, ViewSocketServer viewSocketServer, String name)
			throws WrongInputException, IOException {
		Game game;
		Document document;
		Integer numberPlayers;

		try {
			document = FileUtility.openXML("Configuration//" + configuartion + "//Configuration.xml");
		} catch (Exception exception) {
			throw new WrongInputException("Error on creation of game");
		}

		try {
			numberPlayers = Integer
					.parseInt(((Element) (document.getElementsByTagName("Game").item(0))).getAttribute("Players"));
		} catch (Exception exception) {
			throw new IOException("Error on configuration file");
		}

		game = new Game(configuartion, numberPlayers, index);
		index++;
		game.newPlayer(viewSocketServer, name);

		games.add(game);

		if (index >= Long.MAX_VALUE - 5) {
			index = 0;
		}

		Log.logger.info("New game " + game.getID() + " configuration: " + game.getConfiguration());
	}

	/**
	 * Create a new game and add it on list of list of game. Then add a player.
	 * on game.
	 * 
	 * @param configuartion
	 *            Name of configuration of game.
	 * @param rmiViewServer
	 *            RMI view of player.
	 * @param name
	 *            Name of player.
	 * @throws WrongInputException
	 *             If configuration file isn't found or isn't a file xml.
	 * @throws IOException
	 *             If not found attributes 'Player' in tag element 'Game' in
	 *             Configuration.xml file.
	 */
	public synchronized void newGame(String configuartion, RMIViewServer rmiViewServer, String name)
			throws WrongInputException, IOException {
		Game game;
		Document document;
		Integer numberPlayers;

		try {
			document = FileUtility.openXML("Configuration//" + configuartion + "//Configuration.xml");
		} catch (Exception exception) {
			throw new WrongInputException("Error on creation of game");
		}

		try {
			numberPlayers = Integer
					.parseInt(((Element) (document.getElementsByTagName("Game").item(0))).getAttribute("Players"));
		} catch (Exception exception) {
			throw new IOException("Error on configuration file");
		}

		game = new Game(configuartion, numberPlayers, index);
		index++;
		game.newPlayer(rmiViewServer, name);

		games.add(game);

		if (index >= Long.MAX_VALUE - 5) {
			index = 0;
		}
	}

	/**
	 * Close lobby and send signal to close gameManagement thread.
	 */
	public void closeLobby() {
		gameManagement.close();
		Log.logger.info("Close lobby");
	}

	/**
	 * Class to manage games on a thread. It checks if a game status is
	 * "FINISHED" or "ERROR" and removes it. Check all RMI and socket server
	 * view and if there are some errors it removes them.
	 * 
	 * @author Federico
	 *
	 */
	public class GameManagement extends Thread {
		private boolean close;

		public GameManagement() {
			close = false;
		}

		public void close() {
			close = true;
		}

		@Override
		public void run() {
			ViewSocketServer viewPlayerSocket;
			RMIViewServer viewPlayerRMI;
			Date now;
			long diff;
			Iterator<ViewSocketServer> iteViewSocket;
			Iterator<RMIViewServer> iteViewRMI;
			while (!close) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException exception) {
					Log.logger.severe("Error with sleep of thread GameManagement");
					break;
				}
				if (games.isEmpty()) {
					continue;
				}

				now = new Date();

				for (int i = 0; i < games.size(); i++) {
					synchronized (games.get(i)) {
						if (games.get(i).getStatus() == Game.State.FINISHED) {
							Log.logger.info("Game " + games.get(i).getID() + " finished and removed.");
							games.remove(i);
							i--;
							continue;
						} else if (games.get(i).getStatus() == Game.State.ERROR) {
							games.get(i).close();
							continue;
						}

						iteViewSocket = games.get(i).getViewPlayerSocket();

						while (iteViewSocket.hasNext()) {
							viewPlayerSocket = iteViewSocket.next();

							if (!"".equals(viewPlayerSocket.getError())) {
								games.get(i).removePlayer(viewPlayerSocket);
								iteViewSocket = games.get(i).getViewPlayerSocket();
							} else if (games.get(i).getStatus() == Game.State.RUNNING) {
								if (games.get(i).getNameOfCurrentPlayer()
										.equals(games.get(i).getNamePlayer(viewPlayerSocket))) {
									diff = now.getTime() - viewPlayerSocket.getLastActivity().getTime();
									if ((diff / 60000) % 60 > minuteLastActivity) {
										games.get(i).removePlayer(viewPlayerSocket);
										iteViewSocket = games.get(i).getViewPlayerSocket();
									}
								}
							}
						}

						iteViewRMI = games.get(i).getViewPlayerRMI();

						while (iteViewRMI.hasNext()) {
							viewPlayerRMI = iteViewRMI.next();
							if (!"".equals(viewPlayerRMI.getError())) {
								games.get(i).removePlayer(viewPlayerRMI);
								iteViewRMI = games.get(i).getViewPlayerRMI();
							} else if (games.get(i).getStatus() == Game.State.RUNNING) {
								if (games.get(i).getNameOfCurrentPlayer()
										.equals(games.get(i).getNamePlayer(viewPlayerRMI))) {
									diff = now.getTime() - viewPlayerRMI.getLastActivity().getTime();
									if ((diff / 60000) % 60 > minuteLastActivity) {
										games.get(i).removePlayer(viewPlayerRMI);
										games.get(i).getViewPlayerRMI();
									}
								}
							}
						}

						if (games.get(i).numberPlayer() == 0) {
							games.get(i).close();
						}
					}
				}
			}

			for (Game game : games) {
				game.close();
			}

		}
	}

	/**
	 * Class to initialized a new socket client on a thread.
	 * 
	 * @author Federico
	 *
	 */
	public class PlayerInitialize extends Thread {
		private Socket connection;
		private InputStream streamIn;
		private OutputStream streamOut;

		/**
		 * Create new thread to initialized the new player.
		 * 
		 * @param connectionPlayer
		 *            Socket of connection.
		 * @throws IOException
		 *             If there are some problem with connection.
		 */
		public PlayerInitialize(Socket connectionPlayer) throws IOException {
			connection = connectionPlayer;

			try {
				streamIn = connectionPlayer.getInputStream();
				streamOut = connectionPlayer.getOutputStream();
			} catch (IOException exception) {
				throw new IOException("Error in initialization of player");
			}
		}

		@Override
		public void run() {
			String message;
			String configuration;
			String name;
			String support;
			boolean founded;
			ViewSocketServer viewSocketServer;

			support = "What's your name?";

			do {
				try {
					send(support);
				} catch (IOException exception) {
					return;
				}

				try {
					message = receive();
				} catch (IOException | ClassNotFoundException exception) {
					return;
				}

				founded = checkName(message);
				support = "Name is already in use. Choose another name:";
			} while (founded);

			name = message;

			try {
				send("Choose a configuration:");
			} catch (IOException exception) {
				return;
			}

			try {
				message = receive();
			} catch (IOException | ClassNotFoundException exception) {
				return;
			}

			configuration = message;

			viewSocketServer = new ViewSocketServer(connection);

			Log.logger.info("Client " + connection.getRemoteSocketAddress().toString() + " is " + name
					+ ". He choosen configuration: " + configuration);

			founded = false;
			for (Game game : games) {
				if (game.getConfiguration().equals(configuration) && game.getStatus() == Game.State.WAITING) {
					if (game.newPlayer(viewSocketServer, name)) {
						founded = true;
						break;
					}
				}
			}

			if (!founded) {
				try {
					newGame(configuration, viewSocketServer, name);
				} catch (WrongInputException | IOException exception) {
					try {
						viewSocketServer.close("Configuration not valid");
					} catch (Exception exception2) {
						return;
					}
					Log.logger.severe("Error on creation of game, configuration: " + configuration);
					return;
				}
			}

			try {
				send("Waiting");
			} catch (IOException exception) {
				return;
			}
			Log.logger.info("Player " + name + " initialized");
		}

		/**
		 * Method to send a message to client as serialization object.
		 * 
		 * @param message
		 *            Text of message.
		 * @throws IOException
		 *             If there are some problem with connection.
		 */
		private void send(String message) throws IOException {
			ObjectOutputStream objectOutputStream;
			try {
				objectOutputStream = new ObjectOutputStream(streamOut);
				objectOutputStream.writeObject(message);
			} catch (IOException exception) {
				errorManagement(exception);
			}
		}

		/**
		 * Method to receive a string as serialization object from client.
		 * 
		 * @return return Message from client.
		 * @throws IOException
		 *             If there are some problems with connection.
		 * @throws ClassNotFoundException
		 *             If object received isn't convertible in String.
		 */
		private String receive() throws IOException, ClassNotFoundException {
			String message;
			ObjectInputStream objectInputStream;

			try {
				objectInputStream = new ObjectInputStream(streamIn);
				message = (String) objectInputStream.readObject();
			} catch (IOException | ClassNotFoundException exception) {
				errorManagement(exception);
				throw exception;
			}

			return message;
		}

		/**
		 * Manage an error.
		 * 
		 * @param exception
		 *            Exception receipt.
		 */
		private void errorManagement(Exception exception) {
			Log.logger.info("Error with client " + connection.getRemoteSocketAddress().toString() + ":"
					+ exception.getMessage());

			try {
				connection.close();
			} catch (IOException exception2) {
				connection = null;
			}
			streamIn = null;
			streamOut = null;
		}
	}

}
