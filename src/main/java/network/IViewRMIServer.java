package network;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface for RMI server view.
 * @author Federico
 *
 */
public interface IViewRMIServer extends Remote {
	/**
	 * RMI method called by RMI client view to send a message.
	 * @param message Text of message.
	 * @throws RemoteException	If there are some problems with RMI connection.
	 */
	void updateRMI(String message) throws RemoteException;
}
