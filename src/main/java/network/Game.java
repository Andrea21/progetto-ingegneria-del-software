package network;

import java.awt.Color;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import controller.Controller;
import controller.Controller.TypeRound;
import model.Model;
import view.Log;

/**
 * Class that contains all method to accept new network view server (socket or
 * RMI) and connects all them to play a game.
 * 
 * @author Federico
 *
 */
public class Game implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * State of game:<br>
	 * -"WAITING" when is waiting new player<br>
	 * -"FULL" when list of player is full and waiting that game start.<br>
	 * -"RUNNING" game in progress<br>
	 * -"FINISHED" game over<br>
	 * -"ERROR" there is some errors
	 * 
	 * @author Federico
	 *
	 */
	public enum State {
		WAITING, FULL, RUNNING, FINISHED, ERROR
	};

	private long ID;
	private State status;
	private String configuration;
	private Controller controller;
	private int maxPlayer;
	private Model model;
	private Map<ViewSocketServer, String> listViewSocket;
	private Map<RMIViewServer, String> listViewRMI;
	private transient Timer timer;

	private static int timeSpan = 40000;

	/**
	 * Constructor of class game. It set status to "WAITING".
	 * 
	 * @param configurationGame
	 *            Configuration of game.
	 * @param numberPlayer
	 *            Maximum number of players.
	 * @param IDgame
	 *            Unique identifier for game.
	 */
	public Game(String configurationGame, int numberPlayer, long IDgame) {
		listViewSocket = new HashMap<>();
		listViewRMI = new HashMap<>();
		status = State.WAITING;
		maxPlayer = numberPlayer;
		ID = IDgame;
		configuration = configurationGame;
		timer = null;

		Log.logger.info("New game: " + ID + ". Configuration: " + configurationGame);
	}

	/**
	 * Get unique identifier of game.
	 * 
	 * @return ID of game.
	 */
	public long getID() {
		return ID;
	}

	/**
	 * Get name of configuration of game.
	 * 
	 * @return Name of configuration.
	 */
	public String getConfiguration() {
		return configuration;
	}

	/**
	 * Get the actual number of players in the game.
	 * 
	 * @return Number of players.
	 */
	public int numberPlayer() {
		return listViewRMI.size() + listViewSocket.size();
	}

	/**
	 * Get name of the current player.
	 * 
	 * @return Name of player.
	 */
	public String getNameOfCurrentPlayer() {
		return model.getCurrentPlayer().getName();
	}

	/**
	 * Add a new player to game.
	 * 
	 * @param player
	 *            Socket server view of player.
	 * @param name
	 *            Name of player.
	 * @return Return True if player is added to game. Return False if player
	 *         isn't added to game because the game not accept new player
	 *         (Status isn't "WAITING").
	 */
	public synchronized boolean newPlayer(ViewSocketServer player, String name) {

		if (status != State.WAITING) {
			return false;
		}

		synchronized (this) {
			listViewSocket.put(player, name);
		}

		if (numberPlayer() >= maxPlayer) {
			status = State.FULL;
		}

		if (numberPlayer() >= 2 && timer == null) {
			timer = new Timer(true);
			timer.schedule(new StartGame(), timeSpan);
			Log.logger.info("Game " + ID + ": Timer start");
		}

		Log.logger.info("Player " + name + " entered in game " + ID);

		return true;
	}

	/**
	 * Add a new player to game.
	 * 
	 * @param player
	 *            RMI server view of player.
	 * @param name
	 *            Name of player.
	 * @return Return True if player is added to game. Return False if player
	 *         isn't added to game because the game not accept new player
	 *         (Status isn't "WAITING").
	 */
	public synchronized boolean newPlayer(RMIViewServer player, String name) {
		if (status != State.WAITING) {
			return false;
		}

		synchronized (this) {
			listViewRMI.put(player, name);
		}

		if (numberPlayer() >= maxPlayer) {
			status = State.FULL;
		}

		if (numberPlayer() >= 2 && timer == null) {
			timer = new Timer(true);
			timer.schedule(new StartGame(), timeSpan);
			Log.logger.info("Game " + ID + ": Timer start");
		}

		Log.logger.info("Player " + name + " entered in game " + ID);

		return true;
	}

	/**
	 * Remove a player from the game. Set player in model as inactive, but not
	 * remove him from list of player. Instead socket server view is removed
	 * from list of view.
	 * 
	 * @param playerView
	 *            Socket server view of player.
	 */
	public synchronized void removePlayer(ViewSocketServer playerView) {
		if (controller != null) {
			controller.removePlayer(listViewSocket.get(playerView));
		}
		Log.logger.info("Player " + listViewSocket.get(playerView) + " removed from game " + ID);
		synchronized (this) {
			listViewSocket.remove(playerView);
		}
	}

	/**
	 * Remove a player from the game. Set player in model as inactive, but not
	 * remove him from list of player. Instead RMI server view is removed from
	 * list of view.
	 * 
	 * @param playerView
	 *            Socket server view of player.
	 */
	public synchronized void removePlayer(RMIViewServer playerView) {
		if (controller != null) {
			controller.removePlayer(listViewRMI.get(playerView));
		}
		Log.logger.info("Player " + listViewRMI.get(playerView) + " removed from game " + ID);
		synchronized (this) {
			listViewRMI.remove(playerView);
		}
	}

	/**
	 * Get iterator of name of player of socket server view.
	 * 
	 * @return Iterator of name of player.
	 */
	public synchronized Iterator<String> getNamePlayerSocket() {
		return listViewSocket.values().iterator();
	}

	/**
	 * Get iterator of socket server view of list of socket server view.
	 * 
	 * @return Iterator of socket server view.
	 */
	public synchronized Iterator<ViewSocketServer> getViewPlayerSocket() {
		return listViewSocket.keySet().iterator();
	}

	/**
	 * Get iterator of name of player of RMI server view.
	 * 
	 * @return Iterator of name of player.
	 */
	public synchronized Iterator<String> getNamePlayerRMI() {
		return listViewRMI.values().iterator();
	}

	/**
	 * Get iterator of RMI server view of list of RMI server view.
	 * 
	 * @return Iterator of socket server view.
	 */
	public synchronized Iterator<RMIViewServer> getViewPlayerRMI() {
		return listViewRMI.keySet().iterator();
	}

	/**
	 * Get the name of player by his socket server view.
	 * 
	 * @param viewSocketServer
	 *            rmiViewServer Socket server view of player.
	 * @return Name of player. Return null if there isn't a socket server view
	 *         in list of socket server view.
	 */
	public synchronized String getNamePlayer(ViewSocketServer viewSocketServer) {
		for (ViewSocketServer view : listViewSocket.keySet()) {
			if (view.equals(viewSocketServer)) {
				return listViewSocket.get(view);
			}
		}
		return null;
	}

	/**
	 * Get the name of player by his RMI server view.
	 * 
	 * @param rmiViewServer
	 *            RMI server view of player.
	 * @return Name of player. Return null if there isn't a RMI server view in
	 *         list of RMI server view.
	 */
	public synchronized String getNamePlayer(RMIViewServer rmiViewServer) {
		for (RMIViewServer view : listViewRMI.keySet()) {
			if (view.equals(rmiViewServer)) {
				return listViewRMI.get(view);
			}
		}
		return null;
	}

	/**
	 * Method that start the game, creating model, controller and register they
	 * with views of players.
	 */
	public synchronized void createGame() {
		HashMap<String, Color> infoPlayers;
		Color color;

		infoPlayers = new HashMap<>();

		for (String name : listViewSocket.values()) {
			color = randomColor();

			while (infoPlayers.values().contains(color)) {
				color = randomColor();
			}

			infoPlayers.put(name, color);

		}

		for (String name : listViewRMI.values()) {
			color = randomColor();

			while (infoPlayers.values().contains(color)) {
				color = randomColor();
			}

			infoPlayers.put(name, color);

		}

		try {
			model = new Model(infoPlayers, "Configuration/" + configuration + "/Configuration.xml");
		} catch (Exception exception) {
			status = State.ERROR;
			Log.logger.severe("Error on creation of game " + ID + ", configuration: " + configuration + ": "
					+ exception.getMessage());
		}

		controller = new Controller(model);

		synchronized (this) {

			for (ViewSocketServer view : listViewSocket.keySet()) {
				view.register(controller);
				model.register(view);
			}
			for (RMIViewServer view : listViewRMI.keySet()) {
				view.register(controller);
				model.register(view);
			}
		}
		model.notifyObservers(model);
		model.notifyObservers("-!-Game start");
		status = State.RUNNING;
		Log.logger.info("Game " + ID + " started");
	}

	/**
	 * Get status of game.
	 * 
	 * @return Status of game.
	 */
	public synchronized State getStatus() {
		if (model != null && model.getTypeRound() == TypeRound.FINISHED) {
			status = State.FINISHED;
		}
		return status;
	}

	/**
	 * Method that close the game. It closes all view and set status to
	 * "FINISHED".
	 */
	public void close() {
		for (ViewSocketServer viewSocketServer : listViewSocket.keySet()) {
			viewSocketServer.close();
		}
		status = State.FINISHED;
		Log.logger.info("Game " + ID + " closed");
	}

	/**
	 * Task of timer that is called when timer finished. It calls the method for
	 * creation of game.
	 * 
	 * @author Federico
	 *
	 */
	public class StartGame extends TimerTask {
		@Override
		public void run() {
			if (numberPlayer() >= 2) {
				createGame();
			}
			timer = null;
		}
	}

	/**
	 * Method that generate a random color from static color of class
	 * java.awt.Color.
	 * 
	 * @return Return a random color.
	 */
	public Color randomColor() {

		Random random = new Random();
		int x;

		x = random.nextInt(13);

		switch (x) {
		case 0:
			return Color.BLACK;
		case 1:
			return Color.BLUE;
		case 2:
			return Color.CYAN;
		case 3:
			return Color.DARK_GRAY;
		case 4:
			return Color.GRAY;
		case 5:
			return Color.GREEN;
		case 6:
			return Color.LIGHT_GRAY;
		case 7:
			return Color.MAGENTA;
		case 8:
			return Color.ORANGE;
		case 9:
			return Color.PINK;
		case 10:
			return Color.RED;
		case 11:
			return Color.WHITE;
		case 12:
			return Color.YELLOW;
		default:
			return new Color(random.nextInt(255), random.nextInt(255), random.nextInt(255));
		}

	}
}
