package network;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.Scanner;

/**
 * Main method of this class sets server and lobby to create a complete server
 * that accepts RMI and socket connections.
 * 
 * @author Federico
 *
 */
public class StarterServer {

	private StarterServer() {
		super();
	}

	public static void main(String[] args) {
		Server server;
		Lobby lobby;
		int minutes;

		String input = "";
		Scanner scanner = new Scanner(System.in);

		try {
			LocateRegistry.createRegistry(1099);
		} catch (RemoteException exception) {
		}

		minutes = -1;
		while (minutes <= 0) {
			System.out.println("Minutes to remove player since his last activity");
			try {
				minutes = scanner.nextInt();
			} catch (Exception exception) {
				minutes = -1;
			}
		}

		try {
			lobby = new Lobby(minutes);
		} catch (RemoteException e) {
			System.out.println("Error with lobby");
			scanner.close();
			return;
		}

		try {
			Naming.rebind("Lobby", lobby);
		} catch (RemoteException | MalformedURLException exception) {
			System.out.println("Error with RMI lobby: " + exception.getMessage());
			scanner.close();
			return;
		}
		server = new Server(56789, lobby);
		server.start();

		System.out.println("Write 'ESC' to shutdown server");
		while (!"ESC".equals(input)) {
			input = scanner.nextLine();
		}
		server.close();
		scanner.close();
		System.out.println("Terminated");
	}

}
