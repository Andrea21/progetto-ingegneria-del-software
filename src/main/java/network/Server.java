package network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;

import view.Log;

/**
 * Class creates a server that listen the net. It's also created a lobby object.
 * When a client connects to server, it's transfered to lobby through
 * newPlayer() method of lobby class.
 * 
 * @author Federico
 *
 */
public class Server extends Thread {

	private ServerSocket serverSocket;
	private int port;
	private Lobby lobby;
	private boolean close;
	Socket connection;

	/**
	 * Constructor of server class. The method creates a lobby object and
	 * initializes static Log object. Method don't start server.
	 * 
	 * @param portNet
	 *            Port of server.
	 */
	public Server(int portNet, Lobby lobby) {
		try {
			Log.initialize("Log//");
		} catch (IOException exception) {
			System.out.println("Error with log file:" + exception.getMessage());
			return;
		} 
		port = portNet;
		close = false;
		this.lobby = lobby;
	}

	/**
	 * Close server. If server is running, method sends signal to interrupt it.
	 * Then closes the lobby.
	 */
	public void close() {
		close = true;
		lobby.closeLobby();
		Log.logger.info("Shutdown server");
	}

	/**
	 * Start server on new thread.
	 */
	@Override
	public synchronized void start() {
		Log.logger.info("Avvio server.");

		try {
			serverSocket = new ServerSocket(port);
		} catch (IOException exception) {
			System.out.println("Boot failure! " + exception.toString());
			Log.logger.severe("Boot failure!" + exception.toString());
			return;
		} catch (SecurityException exception) {
			System.out.println("Security error on server's boot!");
			Log.logger.severe("Security error on server's boot!");
			return;
		} catch (IllegalArgumentException exception) {
			System.out.println("Port not valid!");
			Log.logger.severe("Port not valid!");
			return;
		}
		super.start();
	}

	/**
	 * Method of thread. Server listen the network and when a client connects,
	 * method transfer the connection to lobby through method newPlayer() of
	 * Lobby class. Then become to listen the network.<br>
	 * To interrupt thread, call method close().
	 */
	@Override
	public void run() {
		int numberError;
		SocketAddress address;
		numberError = 0;

		while (!close) {
			try {
				connection = serverSocket.accept();
			} catch (IOException exception) {
				Log.logger.info("Error with acceptance a client");
				numberError++;
				if (numberError >= 10) {
					Log.logger.severe("Too many error. Server shut down!");
					close = true;
				}
				continue;
			}

			numberError = 0;
			address = connection.getRemoteSocketAddress();
			
			if (lobby.checkIp(address.toString())){
				try {
					connection.close();
				} catch (IOException exception) {
					Log.logger.severe("Error on closing connection with client" + address.toString()+ " : " + exception.getMessage());
				}
				continue;
			}
			
			
			Log.logger.info("New client: " + address.toString());
			try {
				lobby.newPlayer(connection);
			} catch (IOException exception) {
				Log.logger.info("Error with client " + address.toString());
			}
		}

		try {
			serverSocket.close();
		} catch (IOException exception) {
			Log.logger.severe("Error on closed server");
		}
	}

}
