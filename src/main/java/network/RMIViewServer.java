package network;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import model.Model;
import view.Log;
import view.Observer;

/**
 * Class to manage an RMI connection with a RMI client view (RMIClientView
 * class).
 * 
 * @author Federico
 *
 */
public class RMIViewServer extends UnicastRemoteObject implements IViewRMIServer, Observer {

	private List<Observer> observers;
	private transient IViewRMIClient viewClient;
	private String error;
	private String name;
	private String ipClient;
	private Date lastActivity;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor of class. Initialized server to be called by RMI client view
	 * and connect with it.
	 * 
	 * @param namePlayer
	 *            Name of player.
	 * @param ipClient
	 *            IP address of client.
	 * @throws Exception
	 *             If there are some problems with initialization or connection.
	 */
	public RMIViewServer(String namePlayer, String ipClient) throws Exception {
		observers = new ArrayList<>();
		error = "";
		name = namePlayer;
		this.ipClient = ipClient;

		try {
			viewClient = (IViewRMIClient) Naming.lookup("//" + ipClient + "/ClientRMI"+namePlayer);
		} catch (MalformedURLException | RemoteException | NotBoundException exception) {
			Log.logger.severe("Error on initialization of client RMI:" + exception.getMessage());
		}
		lastActivity = new Date();
		Log.logger.info("New view RMI with client " + ipClient);
	}

	/**
	 * Register a new observer.
	 * 
	 * @param observer
	 *            Observer to register.
	 */
	public void register(Observer observer) {
		observers.add(observer);
	}

	/**
	 * Unregister an observer.
	 * 
	 * @param observer
	 *            Observer to unregister.
	 */
	public void unregister(Observer observer) {
		observers.remove(observer);
	}

	/**
	 * Method called by RMI client view to send a message. Method notifies message
	 * to all observers. It also sets last activity with actual time.
	 */
	@Override
	public void updateRMI(String message) throws RemoteException {
		Log.logger.fine("Message from client RMI " + name + ": " + message);
		for (Observer observer : observers) {
			observer.update(message);
		}
		lastActivity = new Date();
	}

	/**
	 * Method called by RMI client view to send an object model. Method notifies object
	 * to all observers. It also sets last activity with actual time.
	 */
	@Override
	public void update(Model model) {
		try {
			viewClient.notifyModel(model);
		} catch (RemoteException exception) {
			Log.logger.severe("Error with client RMI " + ipClient + ": " + exception.getMessage());
			setError("Error with client RMI " + ipClient + ": " + exception.getMessage());
		}
		lastActivity = new Date();
	}

	/**
	 * Method called by an observer to send a message to a RMI client view.
	 */
	@Override
	public void update(String message) {
		try {
			viewClient.notifyMessage(message);
		} catch (RemoteException exception) {
			Log.logger.severe("Error with client RMI " + ipClient + ": " + exception.getMessage());
			setError("Error with client RMI " + ipClient + ": " + exception.getMessage());
		}
	}

	/**
	 * Get time of last activity of connection.
	 * @return	Time of last activity.
	 */
	public Date getLastActivity() {
		return lastActivity;
	}

	/**
	 * Set an error message.
	 * @param errorMessage	Text of error.
	 */
	public void setError(String errorMessage) {
		error = errorMessage;
	}

	/**
	 * Return an error message.
	 * @return Return "" if there aren't errors, else message of error.
	 */
	public String getError() {
		if ("".equals(error)) {
			try {
				viewClient.checkConnection();
			} catch (RemoteException exception) {
				return "Connection lost";
			}
		}
		return error;
	}

}
