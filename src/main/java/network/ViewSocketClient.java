package network;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Enumeration;

import javax.xml.ws.soap.Addressing;

import model.Model;
import view.Log;
import view.Observable;
import view.Observer;

/**
 * Class to create a socket view to connect with a socket server view
 * (ViewSocketServer class).
 * 
 * @author Federico
 *
 */
public class ViewSocketClient extends Observable implements Observer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private transient Socket connection;
	private transient InputStream streamIn;
	private transient OutputStream streamOut;
	private String error;
	private transient Listener listener;
	private String messageFromServer;

	/**
	 * Constructor of class. It connects with server and start a thread for
	 * listening of network.
	 * 
	 * @param host
	 *            IP Address of server.
	 * @param port
	 *            Port of listening of server.
	 * @param myPort
	 *            My port of connection.
	 * @throws IOException
	 *             If there are some problems with connection.
	 * @throws InterruptedException
	 *             If there are some problems with Thread.sleep()
	 */
	public ViewSocketClient(String host, int port) throws Exception {
		int myPort;
		myPort = 0;
		InetAddress address;
		address = null;
		do {
			try {
				if (address==null){
					connection = new Socket(host, port);
					address = connection.getLocalAddress();
				}
				else {
					connection = new Socket(host, port, address, 56089 + myPort);
				}
			} catch (UnknownHostException exception) {
				Log.logger.info("Host not found: " + host + ":" + port);
				throw exception;
			} catch (IOException exception) {
				Log.logger.info("Try to connection failed: " + host + ":" + port);
				throw exception;
			} catch (SecurityException exception) {
				Log.logger.info("Security error on connection to server");
				throw exception;
			} catch (IllegalArgumentException exception) {
				Log.logger.info("Port not valid");
				throw exception;
			}

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				Log.logger.info("Error with thread.");
				throw e;
			}

			myPort++;

		} while (!connection.isConnected() && myPort < 20);

		streamIn = connection.getInputStream();
		streamOut = connection.getOutputStream();

		listener = new Listener(streamIn);
		listener.start();

		messageFromServer = "";
		error = "";

		Log.logger.info("Connected to host " + host + ":" + port);
	}

	/**
	 * Manage an error on communication.
	 * 
	 * @param exception
	 *            Exception receipt.
	 */
	private void errorManagement(Exception exception) {
		error = exception.getMessage();
		Log.logger.severe("Error with client " + connection.getRemoteSocketAddress().toString() + ":" + error);
		try {
			connection.close();
		} catch (IOException exception2) {
			connection = null;
		}
		streamIn = null;
		streamOut = null;
	}

	/**
	 * Report errors to outside.
	 * 
	 * @return "" if there aren't errors, else text of error.
	 */
	public String getError() {
		return error;
	}

	/**
	 * Method of Observer interface. It's called by an observable and it sends
	 * model object to server.
	 */
	@Override
	public void update(Model model) {
		ObjectOutputStream objectOutputStream;
		try {
			objectOutputStream = new ObjectOutputStream(streamOut);
			objectOutputStream.writeObject(model);
		} catch (IOException | NullPointerException | SecurityException exception) {
			errorManagement(exception);
		}
		Log.logger.fine("To server " + connection.getRemoteSocketAddress().toString() + " object model");
	}

	/**
	 * Method of Observer interface. It's called by an observable and it sends
	 * string to server.
	 */
	@Override
	public void update(String message) {
		ObjectOutputStream objectOutputStream;
		try {
			objectOutputStream = new ObjectOutputStream(streamOut);
			objectOutputStream.writeObject(message);
		} catch (IOException | NullPointerException | SecurityException exception) {
			errorManagement(exception);
		}
		Log.logger.fine("To server " + connection.getRemoteSocketAddress().toString() + ": " + message);
	}

	/**
	 * Get message from server. A string from server is saved on this variable.
	 * 
	 * @return Text of message. "" if there aren't messages from server.
	 */
	public synchronized String getMessageFromServer() {
		String message = messageFromServer;
		messageFromServer = "";
		return message;
	}

	/**
	 * Close connection with server and send interrupt signal to thread
	 * listener.
	 */
	public void close() {
		listener.close();
		try {
			connection.close();
		} catch (IOException exception) {
			connection = null;
		}
		streamIn = null;
		streamOut = null;
	}

	/**
	 * Class to listen network on a separate thread.
	 * 
	 * @author Federico
	 *
	 */
	public class Listener extends Thread {
		InputStream streamIn;
		boolean close;

		/**
		 * Constructor of class.
		 * 
		 * @param inputStream
		 *            Input stream of socket connection.
		 */
		public Listener(InputStream inputStream) {
			streamIn = inputStream;
			close = false;
		}

		/**
		 * Set to True the variable "close" that is read by thread to know when
		 * it must end.
		 */
		public void close() {
			close = true;
		}

		/**
		 * Method that listens network and notifies to observers a string or a
		 * model. If object from server is a string it's also saved in
		 * "messageFromServer" variable.
		 */
		@Override
		public void run() {
			ObjectInputStream objectInputStream;
			Object object;

			Log.logger.info("Listener started");
			while (!close) {
				try {
					objectInputStream = new ObjectInputStream(streamIn);
					object = objectInputStream.readObject();
					if (object instanceof String) {
						notifyObservers((String) object);
						messageFromServer = (String) object;
						Log.logger.info("From server " + connection.getRemoteSocketAddress().toString() + ": "
								+ (String) object);
					} else if (object instanceof Model) {
						notifyObservers((Model) object);
						Log.logger.info(
								"From server " + connection.getRemoteSocketAddress().toString() + " object model");
					}
				} catch (Exception exception) {
					errorManagement(exception);
					return;
				}
			}
		}
	}
}
