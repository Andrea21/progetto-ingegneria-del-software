package utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * Class that has static methods to read a file.
 * @author Federico
 *
 */
public class FileUtility {
	
	/**
	 * Returns an object document created from content of a xml file.
	 * @param path	Pathname of file.
	 * @return	Return an object document.
	 * @throws ParserConfigurationException	 if a DocumentBuilder cannot be created which satisfies the configuration requested.
	 * @throws SAXException	If any parse errors occur.
	 * @throws IOException	If any IO errors occur.
	 */
	public static Document openXML(String path) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory;
		DocumentBuilder parser;
		Document document;

		factory = DocumentBuilderFactory.newInstance();
		parser = factory.newDocumentBuilder();
		document = parser.parse(new java.io.File(path));

		return document;
	}
	
	/**
	 * Returns content of file as text.
	 * @param path	Pathname of file.
	 * @return	Returns a string that contains content of file.
	 * @throws IOException	If file does not exist or if there are some errors on reading.
	 */
	public static String readAllFile(String path) throws IOException{
		FileReader fileReader;
		BufferedReader buffer;
		String content;
		
		if (path==null){
			throw new NullPointerException("Path of file cannot be null");
		}
		
		fileReader = new FileReader("IPServer.txt");
		buffer = new BufferedReader(fileReader);
		
		content="";
		
		while (buffer.ready()){
			content += buffer.readLine();
		}
		
		buffer.close();
		
		return content;
	}
	
	/**
	 * Returns a list of names of directories contained into directory specified by parameter path. Names of directories are just the last name in the pathname's name sequence. 
	 * @param path	Pathname of directory.
	 * @return	Return list of names of directories.
	 * @throws IOException	If path denotes a file and not a directory.
	 */
	public static List<String> getSubDirectories(String path) throws IOException{
		File dir;
		File[] listFile;
		List<String> listDirectories; 
		
		if (path==null){
			throw new NullPointerException("Path of file cannot be null");
		}
		
		dir = new File(path);
		
		listFile = dir.listFiles();
		
		if (listFile==null){
			throw new IOException("Path dosen't denote a directory");
		}
		
		listDirectories= new ArrayList<>();
		
		for (File file: listFile){
			if (file.isDirectory()){
				listDirectories.add(file.getName());
			}
		}
		
		return listDirectories;
	}
	
}
