package view;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Class Log, to write log message on file and console.
 * @author Federico
 *
 */
public class Log {
	public final static Logger logger = Logger.getLogger(Log.class.getName());
	private static boolean initialize = false;
	private static Level level = Level.INFO;
	
	/**
	 * Initialized Log file.
	 * @param path	Path of file.
	 * @throws IOException	If there are some problems with file.
	 * @throws SecurityException	If there are some problems with security of file.
	 */
	public static synchronized void initialize(String path) throws IOException{
		if(initialize){
			return;
		}
		FileHandler fileHandler;
		SimpleDateFormat format;
		
		format = new SimpleDateFormat("M-d_HHmmss");

        fileHandler = new FileHandler(path + format.format(Calendar.getInstance().getTime()) + ".log");

        
        fileHandler.setFormatter(new SimpleFormatter());
        logger.addHandler(fileHandler);
        logger.setLevel(level);
        initialize=true;
	}
}
