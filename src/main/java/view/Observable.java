package view;

import java.util.ArrayList;
import java.util.List;

import model.Model;

public abstract class Observable {

	protected List<Observer> observers;
	
	/**
	 * Constructor of Observable. 
	 */
	public Observable() {
		observers = new ArrayList<Observer>();
	}
	
	/**
	 * Add a new observer to the list of observers.
	 * @param observer
	 */
	public void register(Observer observer){
		observers.add(observer);
	}
	
	/**
	 * Remove an observer to the list of observers.
	 * @param observer
	 */
	public void unregister(Observer observer) {
		observers.remove(observer);
	}
	
	/**
	 * Notify a Model to the observers.
	 * @param model Model object.
	 */
	public void notifyObservers(Model model){
		for (Observer observer : observers) {
			observer.update(model);
		}
	}
	
	/**
	 * Notify a String message to the observers.
	 * @param message String message.
	 */
	public void notifyObservers(String message){
		for (Observer observer: observers){
			observer.update(message);
		}
	}
	
}
