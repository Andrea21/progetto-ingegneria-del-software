package view;

import java.io.Serializable;

import model.Model;

/**
 * Interface that represents an observer.
 * @author Federico
 *
 */
public interface Observer extends Serializable {
	/**
	 * Method called by an observable.
	 * @param model	Model of the game.
	 */
	public void update(Model model);
	/**
	 * Method called by an observable.
	 * @param message	A string message to observer.
	 */
	public void update(String message);
}
