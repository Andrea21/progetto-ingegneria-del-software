package view;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import bonusandcards.CardPolitic;
import bonusandcards.TileBusinessPermit;
import controller.Controller.TypeRound;
import model.City;
import model.ColorCouncillor;
import model.Emporium;
import model.Model;
import model.ObjectForSale;
import model.Player;
import model.Region;
import model.TypeRegion;
import network.RMIViewClient;
import network.ViewSocketClient;
import utility.FileUtility;

/**
 * ViewCLI class. It show the state of the game and takes actions from the player using the console
 * @author Federico
 *
 */
public class ViewCLI extends view.Observable implements view.Observer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Model gioco;
	private String nameOfThePlayer;
	private final static Scanner scanner = new Scanner(System.in);
	private boolean newModel;

	/**
	 * Class constructor. Sets up the view and register the model as an observer
	 * 
	 * @param gioco
	 *            The model that will be set up as an observer
	 */
	public ViewCLI(Model gioco) {
		this.gioco = gioco;
	}

	/**
	 * Method that shows the main menu of a normal round and notifies the
	 * controller about the choices of the player
	 */
	public void mainMenuNormalRound() {
		if(gioco.isLastRound()){
			System.out.println("---ATTENTION---");
			System.out.println("THIS IS THE LAST ROUND OF THE GAME\n");
		}
		showDetails();
		System.out.println("Press 1 to see the map");
		System.out.println("Press 2 to see the infos of other players");
		System.out.println("Press 3 to do a main action");
		System.out.println("Press 4 to do a secondary action");
		System.out.println("Press 0 if you don't want to do a secondary action");
		blockMenu();
		if (newModel) {
			return;
		}
		switch (scanner.nextInt()) {
		case 1:
			showMap();
			break;
		case 2:
			showOtherPlayersState();
			break;
		case 3:
			mainActionMenu();
			break;
		case 4:
			secondaryActionMenu();
			break;
		case 0:
			notifyObservers("0");
			break;
		default:
			System.out.println("Invalid Input!");
		}
	}

	/**
	 * Method that shows the secondary action that a player can choose and
	 * notifies the controller about the action picked and the parameters
	 * necessary to make that action
	 */
	// Controllata
	public void secondaryActionMenu() {
		// support variables
		int input = 0;
		String region = "";
		String color = "";
		System.out.println("Choose a secondary action: ");
		System.out.println("Press 5 to get an assistant ");
		System.out.println("Press 6 to change the business permit tile ");
		System.out.println("Press 7 to elect a councilor with an assistant ");
		System.out.println("Press 8 do another main action ");
		System.out.println("Press 0 to go back to the main menu");
		try {
			input = scanner.nextInt();
		} catch (Exception e) {
		    System.out.println("Invalid value!");
		    scanner.next(); 
		}
		switch (input) {
		case 5:
			notifyObservers("5");
			break;
		case 6:
			System.out.println("Select the region in which you want to change the permit tiles: ");
			System.out.println("Seaside\nHillside\nMountain");
			notifyObservers("6-" + scanner.next());
			break;
		case 7:
			System.out.println("Choose the region in which you want to elect the councilor: ");
			System.out.println("Seaside " + Arrays.toString(gioco.getBoard().getRegion(TypeRegion.SEASIDE).getCouncillors()));
			System.out.println("Hillside " + Arrays.toString(gioco.getBoard().getRegion(TypeRegion.HILLSIDE).getCouncillors()));
			System.out.println("Mountain " + Arrays.toString(gioco.getBoard().getRegion(TypeRegion.MOUNTAIN).getCouncillors()));
			System.out.println("King's Board " + Arrays.toString(gioco.getBoard().getKingBoard().getCouncillors()));
			region = scanner.next();
			System.out.println("Choose one of the avaiable councilor");
			for(ColorCouncillor c: gioco.getBoard().getCouncillors()){
				System.out.println(c);
			}
			color = scanner.next();
			notifyObservers("7-" + region + ":" + color);
			break;
		case 8:
			notifyObservers("8");
			break;
		case 0:
			break;
		default:
			System.out.println("Invalid Input!");

		}
	}

	/**
	 * Method that shows the main action that a player can choose and notifies
	 * the controller about the action picked and the parameters necessary to
	 * make that action
	 */
	// Controllata
	public void mainActionMenu() {
		// support variables
		String region = "";
		String color = "";
		String permit = "";
		String city = "";
		int tempInt = 0;
		int input;
		int temp = 1;
		int count = 0;
		String support = "";
		String support2 = "";
		String card = "";
		String cityKing = "";
		System.out.println("Choose a main action: ");
		System.out.println("Press 1 to elect a councilor ");
		System.out.println("Press 2 to buy a business permit tile ");
		System.out.println("Press 3 to build an emporium using a business permit tile ");
		System.out.println("Press 4 to build an emporium using the king's aid ");
		System.out.println("Press 0 to go back to the main menu");
		input = scanner.nextInt();
		switch (input) {
		case 1:
			System.out.println("Choose the region in which you want to elect the councilor: ");
			System.out.println("Seaside " + Arrays.toString(gioco.getBoard().getRegion(TypeRegion.SEASIDE).getCouncillors()));
			System.out.println("Hillside " + Arrays.toString(gioco.getBoard().getRegion(TypeRegion.HILLSIDE).getCouncillors()));
			System.out.println("Mountain " + Arrays.toString(gioco.getBoard().getRegion(TypeRegion.MOUNTAIN).getCouncillors()));
			System.out.println("King's Board " + Arrays.toString(gioco.getBoard().getKingBoard().getCouncillors()));
			region = scanner.next();
			System.out.println("Choose one of the avaiable councilor");
			for(ColorCouncillor c: gioco.getBoard().getCouncillors()){
				System.out.println(c);
			}
			color = scanner.next();
			notifyObservers("1-" + region + ":" + color);
			break;
		case 2:
			System.out.println("Choose the region from which you wanto to buy the permit tile: ");
			System.out.println("Seaside " + Arrays.toString(gioco.getBoard().getRegion(TypeRegion.SEASIDE).getCouncillors()));
			System.out.println("Hillside " + Arrays.toString(gioco.getBoard().getRegion(TypeRegion.HILLSIDE).getCouncillors()));
			System.out.println("Mountain " + Arrays.toString(gioco.getBoard().getRegion(TypeRegion.MOUNTAIN).getCouncillors()));
			region = scanner.next();
			if(!region.equalsIgnoreCase("SEASIDE") && !region.equalsIgnoreCase("HILLSIDE") && !region.equalsIgnoreCase("MOUNTAIN")){
				System.out.println("Invalid region type!");
				region = "";
				break;
			}
			System.out.println("Choose which of the two avaiable cards you want: ");
			System.out.println("[1] Tile permit A: "
					+ gioco.getBoard().getRegion(TypeRegion.parseIn(region)).getTileBusinessPermitA().toString()
					+ "\n");
			System.out.println("[2] Tile permit B: "
					+ gioco.getBoard().getRegion(TypeRegion.parseIn(region)).getTileBusinessPermitB().toString()
					+ "\n");
			try {
				tempInt = scanner.nextInt();
			} catch (Exception e) {
			    System.out.println("Invalid value!");
			    scanner.next(); 
			}
			if (tempInt == 1) {
				permit = gioco.getBoard().getRegion(TypeRegion.parseIn(region)).getTileBusinessPermitA().toString();
			}
			if (tempInt == 2) {
				permit = gioco.getBoard().getRegion(TypeRegion.parseIn(region)).getTileBusinessPermitB().toString();
			}
			System.out.println(permit);
			while (temp == 1 && count < 4) { // true until the player is done or
												// he picked the max number of
												// cards
				System.out.println("Choose the color of the card you want to use: ");
				card = scanner.next();
				support = card + ":" + support;
				count++;
				System.out.println("Press 1 if you want to use another card, 0 otherwise ");
				try {
					temp = scanner.nextInt();
				} catch (Exception e) {
				    System.out.println("Invalid value!");
				    scanner.next(); 
				}
			}
			temp = 1;
			count = 0;
			if (support.charAt(support.length() - 1) == ':') {
				support = support.substring(0, support.length() - 1);
			}
			notifyObservers("2-" + region + ":" + permit + ":" + support);
			break;
		case 3:
			System.out.println("Choose the permit tile you want to use: ");
			for (int i = 0; i < gioco.getCurrentPlayer().getTileBusinessPermit().size(); i++) {
				System.out.println("[" + i + "]" + gioco.getCurrentPlayer().getTileBusinessPermit().get(i).toString());
			}
			try {
				temp = scanner.nextInt();
			} catch (Exception e) {
			    System.out.println("Invalid value!");
			    scanner.next(); 
			}
			if(temp >= gioco.getCurrentPlayer().getTileBusinessPermit().size()){
				System.out.println("You don't have that permit tile!");
				break;
			}
			else {
			permit = gioco.getCurrentPlayer().getTileBusinessPermit().get(temp).toString();
			System.out.println("Choose the city where you want to build");
			city = scanner.next();
			notifyObservers("3-" + permit + ":" + city);
			break;
			}
		case 4:

			while (temp == 1 && count < 4) { // this gets the cards
				System.out.println("Choose the color of the card you want to use: ");
				card = scanner.next();
				support = card + ":" + support;
				count++;
				System.out.println("Press 1 if you want to use another card, 0 otherwise ");
				try {
					temp = scanner.nextInt();
				} catch (Exception e) {
				    System.out.println("Invalid value!");
				    scanner.next(); 
				}
			}
			temp = 1;
			count = 0;
			System.out.println("Insert 1 if you need to move the king, 0 otherwise");
			try {
				temp = scanner.nextInt();
			} catch (Exception e) {
			    System.out.println("Invalid value!");
			    scanner.next(); 
			}
			while (temp == 1) { // this gets the route of the king
				System.out.println("Insert the city where you want to move the king: ");
				cityKing = scanner.next();
				support2 += cityKing + ":";
				System.out.println("Press 1 if you want to keep inserting cities, 0 otherwise ");
				try {
					temp = scanner.nextInt();
				} catch (Exception e) {
				    System.out.println("Invalid value!");
				    scanner.next(); 
				}
			}
			temp = 1;
			if (support.charAt(support.length() - 1) == ':') {
				support = support.substring(0, support.length() - 1);
			}

			if (support2.length() != 0) {
				if (support2.charAt(support2.length() - 1) == ':') {
					support2 = support2.substring(0, support2.length() - 1);
				}
			}
			notifyObservers("4-" + support + ":" + support2);
			break;
		case 0:
			break;
		default:
			System.out.println("Invalid Input!");
		}
	}

	/**
	 * Method that shows the details of all the players
	 */
	public void showOtherPlayersState() {

		for (Player p : gioco.getPlayers()) {
			if (p.getName().equals(nameOfThePlayer)) {
				continue;
			}
			System.out.println("Name: " + p.getName());
			System.out.println("Color: " + Model.getColorName(p.getColor()).toUpperCase());
			System.out.println("Coins: " + p.getPositionCoinsTrack());
			System.out.println("Number of assistants: " + p.getAssistants());
			System.out.println("Position nobility: " + p.getPositionNobility());
			System.out.println("Position victory: " + p.getPositionVictory());
			System.out.println("Number of avaiable emporium: " + p.getNumberAvailableEmporium());
			System.out.println("Tile business permit: ");
			for (TileBusinessPermit b : p.getTileBusinessPermit()) {
				System.out.println(b.toString());
			}
		}
		System.out.println("Insert anything to go back to the main menu");
		scanner.next();
	}

	/**
	 * Method that shows the details and the state of the map
	 */
	public void showMap() {
		System.out.println("Number of avaiable assistants: " + gioco.getBoard().getPoolAssistants());
		System.out.println("Theese are the avaiable councilor you can elect:");
		for (ColorCouncillor c : gioco.getBoard().getCouncillors()) {
			System.out.println(c);
		}
		for (Region r : gioco.getBoard().getRegion()) {
			System.out.println();
			System.out.println(r.getTypeOfRegion() + " REGION\n");
			System.out.println("Color of the councilor in the balcony:");
			for (int i = 0; i < r.getCouncillors().length; i++) {
				System.out.println(r.getCouncillors()[i]);
			}
			// visualizzare tessere permesso
			System.out.println();
			System.out.println("Tile A: " + r.getTileBusinessPermitA());
			System.out.println("Tile B: " + r.getTileBusinessPermitB());
			for (City c : r.getCity()) {
				System.out.println();
				System.out.println("City name: " + c.getName().toUpperCase());
				System.out.println("City color: " + c.getColor());
				System.out.println("City token reward:" + c.getTokenReward());
				if (c.getEmporiums().size() != 0) {
					for (Emporium e : c.getEmporiums()) {
						System.out.println("Emporium owned by: " + Model.getColorName(e.getColor()).toUpperCase());
					}
				}

				System.out.println("City emporium built: " + c.getNumberOfBuildedEmporium());
				for (City connectedCity : c.getNearbyCities()) {
					System.out.println("Nearby cities: " + connectedCity.getName());
				}
			}
		}
		System.out.println();
		System.out.println("KING'S BOARD:\n ");
		for (int i = 0; i < gioco.getBoard().getKingBoard().getCouncillors().length; i++) {
			System.out.println(gioco.getBoard().getKingBoard().getCouncillors()[i]);
		}
		System.out.println("The king is in " + gioco.getBoard().getKingBoard().getKingCity().getName());
		System.out.println("Insert anything to go back to the main menu");
		scanner.next();
	}

	/**
	 * Method that shows the details of the current player
	 */
	public void showDetails() {

		System.out.println("Your details:");
		System.out.println("Name: " + nameOfThePlayer);
		System.out.println(
				"Color: " + Model.getColorName(gioco.getPlayerByName(nameOfThePlayer).getColor()).toUpperCase());
		System.out.println("Coins: " + gioco.getPlayerByName(nameOfThePlayer).getPositionCoinsTrack());
		System.out.println("Number of assistants: " + gioco.getPlayerByName(nameOfThePlayer).getAssistants());
		System.out.println(
				"Number of avaiable emporiums: " + gioco.getPlayerByName(nameOfThePlayer).getNumberAvailableEmporium());
		System.out.println("Position nobility: " + gioco.getPlayerByName(nameOfThePlayer).getPositionNobility());
		System.out.println("Position victory: " + gioco.getPlayerByName(nameOfThePlayer).getPositionVictory());
		for (TileBusinessPermit b : gioco.getPlayerByName(nameOfThePlayer).getTileBusinessPermit()) {
			System.out.println("Tile business permit: ");
			System.out.println(b.toString());
		}
		for (CardPolitic c : gioco.getPlayerByName(nameOfThePlayer).getcardsPolitic()) {
			if (c.isSpecial()) {
				System.out.println("Card politic: SPECIAL");
			} else
				System.out.println("Card politic: " + c.getColor());

		}
	}

	/**
	 * Method that shows the menu for the special bonus in the nobility path
	 * Notifies the controller about the parameters needed to pick the bonus
	 */
	public void menuBonus1() {
		String temp;
		System.out.println("CONGRATULATION! You got a special nobility bonus!");
		System.out.println("Theese are the city from which you can choose: ");
		for(Region r: gioco.getBoard().getRegion()){
			for(City c: r.getCity()){
				if(c.hasEmporium(gioco.getCurrentPlayer()) && !c.getTokenReward().toString().equalsIgnoreCase("Bonus nobility [1]")){
					System.out.println(c.getName() + c.getTokenReward().toString());
				}
			}
		}
		System.out.println("Coose the city of which you want the token");
		temp = scanner.next();
		if(temp.equalsIgnoreCase("0")){
			notifyObservers("9-");
		}
		else notifyObservers("9-" + temp);
	}

	/**
	 * Method that shows the menu for the special bonus in the nobility path
	 * Notifies the controller about the parameters needed to pick the bonus
	 */
	public void menuBonus2() {
		String region = "";
		String permit = "";
		int tempInt = 0;
		System.out.println("CONGRATULATION! You got a special nobility bonus!");
		System.out.println("Choose the region from which you want to buy the permit tile: ");
		System.out.println("Seaside\nHillside\nMountain");
		region = scanner.next();
		if(!region.equalsIgnoreCase("SEASIDE") && !region.equalsIgnoreCase("HILLSIDE") && !region.equalsIgnoreCase("MOUNTAIN")){
			System.out.println("Invalid region type!");
			notifyObservers("10-" + permit);
		}
		System.out.println("Choose which of the two avaiable cards you want: ");
		System.out.println("[1] Tile permit A: " + gioco.getBoard().getRegion(TypeRegion.parseIn(region)).getTileBusinessPermitA().toString() + "\n");
		System.out.println("[2] Tile permit B: " + gioco.getBoard().getRegion(TypeRegion.parseIn(region)).getTileBusinessPermitB().toString() + "\n");
		try {
			tempInt = scanner.nextInt();
		} catch (Exception e) {
		    System.out.println("Invalid value!");
		    scanner.next(); 
		}
		if (tempInt == 1) {
			permit = gioco.getBoard().getRegion(TypeRegion.parseIn(region)).getTileBusinessPermitA().toString();
		}
		if (tempInt == 2) {
			permit = gioco.getBoard().getRegion(TypeRegion.parseIn(region)).getTileBusinessPermitB().toString();
		}
		notifyObservers("10-" + permit);
	}

	/**
	 * Method that shows the menu for the special bonus in the nobility path
	 * Notifies the controller about the parameters needed to pick the bonus
	 */
	public void menuBonus3() {
		String permit = "";
		int index = gioco.getCurrentPlayer().getTileBusinessPermit().size();
		int temp =0;
		System.out.println("CONGRATULATION! You got a special nobility bonus!");
		System.out.println("Choose the permit tile you own that you want the bonus of");
		for (int i = 0; i < gioco.getCurrentPlayer().getTileBusinessPermit().size(); i++) {
			System.out.println("[" + i + "]" + gioco.getCurrentPlayer().getTileBusinessPermit().get(i).toString());
		}
		for (int j = 0; j < gioco.getCurrentPlayer().getRefuseTileBusinessPermit().size(); j++) {
			System.out.println(
					"[" + index + "]" + gioco.getCurrentPlayer().getRefuseTileBusinessPermit().get(j).toString());
			index++;
		}
		try {
			temp = scanner.nextInt();
		} catch (Exception e) {
		    System.out.println("Invalid value!");
		    scanner.next(); 
		}
		if (temp < gioco.getCurrentPlayer().getTileBusinessPermit().size()) {
			permit = gioco.getCurrentPlayer().getTileBusinessPermit().get(temp).toString();
		} else {
			permit = gioco.getCurrentPlayer().getRefuseTileBusinessPermit()
					.get(temp - gioco.getCurrentPlayer().getTileBusinessPermit().size()).toString();
		}
		notifyObservers("11-" + permit);
	}

	/**
	 * Method that shows the menu for the special bonus in the nobility path
	 * Notifies the controller about the parameters needed to pick the bonus
	 */
	public void menuBonus4() {
		String city = ""; // support variable
		System.out.println("CONGRATULATION! You got a special nobility bonus!");
		System.out.println("Theese are the city from which you can choose: ");
		for(Region r: gioco.getBoard().getRegion()){
			for(City c: r.getCity()){
				if(c.hasEmporium(gioco.getCurrentPlayer())){
					System.out.println(c.getName() + c.getTokenReward().toString());
				}
			}
		}
		System.out.println("Coose two cities of which you want the token");
		city = scanner.next();
		notifyObservers("12-" + city + scanner.next());
	}

	/**
	 * Method that shows the main menu of the first part of the market round
	 * where the players can choose to sell or not some of his things and
	 * notifies the controller about the choices of the player
	 */
	public void mainMenuMarket1() {
		showDetails();
		System.out.println("IT'S THE FIRST ROUND OF THE MARKET!");
		System.out.println("Theese are the object on sale: ");
		objectOnSale();
		System.out.println("Press 1 to choose what to sell ");
		System.out.println("Press 2 if you don't want to sell anything ");
		blockMenu();
		if (newModel) {
			return;
		}
		switch (scanner.nextInt()) {
		case 1:
			menuSells();
			break;
		case 2:
			notifyObservers("--");
			break;
		default:
			System.out.println("Invalid Input!");
		}
	}

	/**
	 * Method that shows the menu to choose what the player wants to sell and
	 * the prices of them. Notifies the controller about the choices of the
	 * player
	 */
	// Controllata
	public void menuSells() {
		// support variables
		int temp = 1;
		int temp2 = 1;
		int maxNumberOfAssistant = 0;
		int priceAssistants = 0;
		int priceCard = 0;
		int priceTile = 0;
		String colorCard = "";
		String assistant = "";
		String permitChosen = "";
		String permit = "";
		String card = "";
		while (temp == 1) { // true until the player is done selling
			System.out.println("Choose  what to sell: ");
			System.out.println("Press 1 for assistants ");
			System.out.println("Press 2 for politic cards ");
			System.out.println("Press 3 for permit tiles ");
			System.out.println("Press 4 when you are done");
			switch (scanner.nextInt()) {
			case 1:
				System.out.println("Choose the max number of assistants you want to sell: ");
				maxNumberOfAssistant = scanner.nextInt();
				System.out.println("Choose the price for the assistants: ");
				priceAssistants = scanner.nextInt();
				assistant = maxNumberOfAssistant + ":" + priceAssistants;
				break;
			case 2:
				while (temp2 == 1) {
					System.out.println("Choose the color of the card you want to sell: ");
					colorCard = scanner.next();
					System.out.println("Choose the price of the card: ");
					priceCard = scanner.nextInt();
					card = colorCard + ":" + priceCard + ":" + card;
					System.out.println("Press 1 if you want to sell another card, 0 otherwise ");
					try {
						temp2 = scanner.nextInt();
					} catch (Exception e) {
					    System.out.println("Invalid value!");
					    scanner.next(); 
					}
				}
				temp2 = 1;
				break;
			case 3:
				while (temp2 == 1) {
					System.out.println("Choose the number of the permit tile you want to sell: ");
					for (int i = 0; i < gioco.getCurrentPlayer().getTileBusinessPermit().size(); i++) {
						System.out.println(
								"[" + i + "]" + gioco.getCurrentPlayer().getTileBusinessPermit().get(i).toString());
					}
					permitChosen = gioco.getCurrentPlayer().getTileBusinessPermit().get(scanner.nextInt()).toString();
					System.out.println("Choose the price of the permit tile: ");
					priceTile = scanner.nextInt();
					permit = permitChosen + ":" + priceTile + ":" + permit;
					System.out.println("Press 1 if you want to sell another tile, 0 otherwise ");
					try {
						temp2 = scanner.nextInt();
					} catch (Exception e) {
					    System.out.println("Invalid value!");
					    scanner.next(); 
					}
				}
				temp2 = 1;
				break;
			case 4: // when the player is done it shows a recap of what he want
					// to sell
				System.out.println("You chose to sell: ");
				System.out.println("Assistant:Prize " + assistant);
				System.out.println("Card:Prize " + card);
				System.out.println("Permit Tile:Prize " + permit);
				System.out.println("Is that correct?\n[1] YES\n[2] NO");
				if (scanner.nextInt() == 1) { // if the player is ok with what
												// he decided to sell the method
												// notifies the controller
					if (card.length() > 0 && card.charAt(card.length() - 1) == ':') {
						card = card.substring(0, card.length() - 1);
					}
					if (permit.length() > 0 && permit.charAt(permit.length() - 1) == ':') {
						permit = permit.substring(0, permit.length() - 1);
					}
					notifyObservers(assistant + "-" + card + "-" + permit);
					temp = 0;
					break;
				} else { // the method reset everything and show the menu for
							// the selling part
					menuSells();
					break;
				}
			default:
				System.out.println("Invalid Input!");
			}
		}
	}

	/**
	 * Method that shows the current object for sale for every player
	 */
	public void objectOnSale() {

		for (Player p : gioco.getPlayers()) {
			if (!p.getName().equalsIgnoreCase(gioco.getCurrentPlayer().getName())) {
				if (p.getObjectForSale().isEmpty()) {
					System.out.println(p.getName() + " doesn't sell anything!");
				} else {
					System.out.println(p.getName() + " sells: ");
					for (ObjectForSale o : p.getObjectForSale()) {
						if (o.isAssistants()) {
							System.out.println("Assistants for sale: " + o.getAssistants() + " cost: " + o.getPrice());
						}
						if (o.isCardPolitc()) {
							if(o.getCardPolitic().isSpecial()){
								System.out.println("Card for sale: SPECIAL cost: " + o.getPrice());
							}
							else
								System.out.println("Card for sale: " + o.getCardPolitic().getColor().toString() + " cost: "
									+ o.getPrice());
						}
						if (o.isTileBusinessPermit()) {
							System.out.println("[" + p.getObjectForSale().indexOf(o) + "] Tile for sale: "
									+ o.getTileBusinessPermit().toString() + " cost: " + o.getPrice());
						}
					}
				}
			}
		}
	}

	/**
	 * Method that shows the main menu of the second part of the market round
	 * where the players can choose to buy or not some of the things other
	 * players sold and notifies the controller about the choices of the player
	 */
	public void mainMenuMarket2() {
		showDetails();
		System.out.println("IT'S THE SECOND ROUND OF THE MARKET!");
		System.out.println("These are the object on sale: ");
		objectOnSale();
		System.out.println("Press 1 if you want to buy something");
		System.out.println("Press 2 if you don't want to buy");
		blockMenu();
		if (newModel) {
			return;
		}
		switch (scanner.nextInt()) {
		case 1:
			menuBuy();
			break;
		case 2:
			notifyObservers("");
			break;
		default:
			System.out.println("Invalid Input!");
		}
	}

	/**
	 * Method that shows the menu to choose what the player wants to buy.
	 * Notifies the controller about the choices of the player
	 */
	public void menuBuy() {
		int temp = 1;
		String support = "";
		String nameOfThePlayer = "";
		String thingIWantToBuy = "";
		String mySelection = "";
		while (temp == 1) {
			System.out.println("Select the name of the player who you want to buy from");
			nameOfThePlayer = scanner.next();
			System.out.println("Select what you want to buy: ");
			System.out.println("Press A for assistants, C for card, T for permit tile");
			thingIWantToBuy = scanner.next();
			if (thingIWantToBuy.equalsIgnoreCase("A")) {
				System.out.println("Choose the number of assistants you want");
				mySelection = scanner.next();
			}
			if (thingIWantToBuy.equalsIgnoreCase("C")) {
				System.out.println("Choose the color of the card you want");
				mySelection = scanner.next();
			}
			if (thingIWantToBuy.equalsIgnoreCase("T")) {
				System.out.println("Choose the permit tile you want");
				mySelection = gioco.getPlayerByName(nameOfThePlayer).getObjectForSale().get(scanner.nextInt())
						.getTileBusinessPermit().toString();
			}

			support = nameOfThePlayer + ":" + thingIWantToBuy + ":" + mySelection + "-" + support;
			System.out.println("Select 1 if you want to buy more, 0 otherwise");
			try {
				temp = scanner.nextInt();
			} catch (Exception e) {
			    System.out.println("Invalid value!");
			    scanner.next(); 
			}
		}
		// add a method to check what the player wants to buy ---> Done
		System.out.println("You chose to buy: ");
		System.out.println("Name of the player:Thing you want:Number of things\n " + support);
		System.out.println("Is that correct?\n[1] YES\n[2] NO");
		if (scanner.nextInt() == 1) {

			if (support.charAt(support.length() - 1) == '-') {
				support = support.substring(0, support.length() - 1);
			}
			notifyObservers(support);
		} else { // the method reset everything and show the menu for
			// the buying part
			menuBuy();
		}
	}

	/**
	 * Method that use the right type of menus according to the type of round
	 * saved in the model
	 */
	public void useTheRightMenu() {
		boolean game = true;
		System.out.print("Waiting...\n");
		while (gioco == null) {

			try {
				Thread.sleep(100);
			} catch (InterruptedException exception) {
				System.out.println("Unexpected error: " + exception.getMessage());
				return;
			}
		}
		while (game) {
			newModel = false;
			if (gioco.getTypeRound() == TypeRound.FINISHED) {
				showRankings();
				game = false;
			} else if (!gioco.getCurrentPlayer().getName().equals(nameOfThePlayer)) {
				menuNotYourTurn();
			} else if (gioco.getTypeRound() == TypeRound.NORMAL) {
				if(gioco.getCurrentPlayer().getSpecialNobilityBonus().equals("BonusTokenReward1")) {
					menuBonus1();
				}
				if (gioco.getCurrentPlayer().getSpecialNobilityBonus().equals("BonusTakePermitTile")) {
					menuBonus2();
				}
				if (gioco.getCurrentPlayer().getSpecialNobilityBonus().equals("BonusTakePreviousBonus")) {
					menuBonus3();
				}
				if (gioco.getCurrentPlayer().getSpecialNobilityBonus().equals("BonusTokenReward2")) {
					menuBonus4();
				}
				if(gioco.getCurrentPlayer().getSpecialNobilityBonus().equals("")){
				mainMenuNormalRound();
				}
			} else if (gioco.getTypeRound() == TypeRound.MARKET1) {
				mainMenuMarket1();
			} else if (gioco.getTypeRound() == TypeRound.MARKET2) {
				mainMenuMarket2();
			}
		}
	}

	/**
	 * Method that prints the error string received by the model and then prints
	 * the right menu type
	 */
	public void update(String error) {
		String subString;
		String tempString = error.substring(0, 3);
		
		do {
			try {
				Thread.sleep(100);
			} catch (InterruptedException exception) {
				System.out.println("Unexpected error: " + exception.getMessage());
				return;
			}
			try {
				if (tempString.equalsIgnoreCase("-!-")) {
					subString = error.substring(3);
					System.out.println("--MESSAGE TO ALL PLAYERS--");
					System.out.println(subString);
				} else if (gioco.getCurrentPlayer().getName().equalsIgnoreCase(nameOfThePlayer)) {
					System.out.println("--ERROR--");
					System.out.println(error);
				}
			} catch (Exception exception) {
				System.out.println("Unexpected error: " + exception.getMessage());
				return;
			}
		} while (newModel);
	}

	/**
	 * Method that prints right menu type when received an update from the model
	 */
	public void update(Model model) {
		gioco = model;
		newModel = true;
	}

	/**
	 * Set the name of the player
	 * 
	 * @param name
	 *            name of the player
	 */
	public void setNameOfThePlayer(String name) {
		nameOfThePlayer = name;
	}

	/**
	 * Method that prints the menu if it's not your turn
	 */
	public void menuNotYourTurn() {
		System.out.println("It's " + gioco.getCurrentPlayer().getName() + "'s turn!");
		showDetails();
		System.out.println("Press 1 to see the map");
		System.out.println("Press 2 to see the infos of other players");
		blockMenu();
		if (newModel) {
			return;
		}
		switch (scanner.nextInt()) {
		case 1:
			showMap();
			break;
		case 2:
			showOtherPlayersState();
			break;
		}
	}

	/**
	 * Method that blocks the menu waiting for an eventual update from the model
	 */
	private void blockMenu() {
		int avaiableChar = 0;
		do {
			try {
				Thread.sleep(100);
			} catch (InterruptedException exception) {
				System.out.println("Unexpected error: " + exception.getMessage());
				return;
			}
			try {
				avaiableChar = System.in.available();
			} catch (IOException exception) {
				System.out.println("Unexpected error: " + exception.getMessage());
				return;
			}
		} while (avaiableChar == 0 && !newModel);
	}

	/**
	 * Method that shows the final rankings of a game
	 */
	private void showRankings() {
		if (gioco.getRanking().get(1).getName().equalsIgnoreCase(nameOfThePlayer)) {
			System.out.println("CONGRATULATION! YOU WON!");
		}
		for (int i = 1; i <= gioco.getRanking().size(); i++) {
			System.out.println("[" + i + "]" + gioco.getRanking().get(i).getName());
		}

	}
	
	public static void main(String[] args) throws IOException {
		String answer;
		String host;
		String typeConnection;
		String name;
		String configuration;
		int choosen;
		String supportString;
		List<String> configurations;
		ViewCLI view = new ViewCLI(null);
		ViewSocketClient viewSocket;
		RMIViewClient viewRMI;

		host = FileUtility.readAllFile("IPServer.txt");

		configurations = FileUtility.getSubDirectories("Configuration");

		choosen = -1;
		while (choosen == -1) {
			System.out.println("Choose a configuration (indicate the number):");
			for (int i = 0; i < configurations.size(); i++) {
				System.out.println("[" + i + "] " + configurations.get(i));
			}

			supportString = scanner.next();

			try {
				choosen = Integer.parseInt(supportString);
			} catch (Exception exception) {
				System.out.println("Input not valid");
				choosen = -1;
				continue;
			}

			if (choosen < 0 || choosen >= configurations.size()) {
				System.out.println("Input not valid");
				choosen = -1;
			}
		}

		configuration = configurations.get(choosen);

		typeConnection = "";

		while (!"1".equals(typeConnection) && !"2".equals(typeConnection)) {
			System.out.println("[1] socket");
			System.out.println("[2] RMI");
			typeConnection = scanner.next();
		}

		if ("1".equals(typeConnection)) {

			try {
				viewSocket = new ViewSocketClient(host, 56789);
			} catch (Exception exception) {
				System.out.println("Error on connection: " + exception.getMessage());
				return;
			}

			answer = "";
			while ("".equals(answer)) {
				answer = viewSocket.getMessageFromServer();

				if (!"".equals(viewSocket.getError())) {
					System.out.println("Error with server: " + viewSocket.getError());
					return;
				}

				try {
					Thread.sleep(100);
				} catch (InterruptedException exception) {
					System.out.println("Unexpected error: " + exception.getMessage());
					return;
				}
			}

			do {
				System.out.println(answer);
				name = scanner.next();
				viewSocket.update(name);

				answer = "";
				while ("".equals(answer)) {
					answer = viewSocket.getMessageFromServer();

					if (!"".equals(viewSocket.getError())) {
						System.out.println("Error with server: " + viewSocket.getError());
						return;
					}

					try {
						Thread.sleep(100);
					} catch (InterruptedException exception) {
						System.out.println("Unexpected error: " + exception.getMessage());
						return;
					}
				}
			} while ("Name is already in use. Choose another name:".equals(answer));

			viewSocket.update(configuration);

			answer = "";
			while ("".equals(answer)) {
				answer = viewSocket.getMessageFromServer();

				if (!"".equals(viewSocket.getError())) {
					System.out.println("Error with server: " + viewSocket.getError());
					return;
				}

				try {
					Thread.sleep(100);
				} catch (InterruptedException exception) {
					System.out.println("Unexpected error: " + exception.getMessage());
					return;
				}
			}
			
			view.setNameOfThePlayer(name);
			view.register(viewSocket);
			viewSocket.register(view);
			view.useTheRightMenu();

		} else {

			try {
				LocateRegistry.createRegistry(1099);
			} catch (RemoteException exception) {
			}
			
			try {
				viewRMI = new RMIViewClient(host);
			} catch (Exception exception) {
				System.out.println("Error on connection: " + exception.getMessage());
				return;
			}

			name = "";
			while ("".equals(name)) {
				System.out.println("What's your name?");
				name = scanner.next();
			}

			answer = viewRMI.initialize(name, configuration);

			while (!"Waiting".equals(answer)) {
				System.out.println("Name is already in use. Choose another name");
				name = scanner.next();
				while ("".equals(name)) {
					System.out.println("What's your name?");
					name = scanner.next();
				}
				answer = viewRMI.initialize(name, configuration);
			}

			view.setNameOfThePlayer(name);
			view.register(viewRMI);
			viewRMI.register(view);
			view.useTheRightMenu();
		}
	}

}
