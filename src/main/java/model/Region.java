package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import bonusandcards.*;

/**
 * Class that represents a region. It has cities, business permit tile and a
 * balcony of councillors
 * 
 * @author Federico
 *
 */
public class Region implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TypeRegion type;
	private LinkedList<City> cities;
	private Deck<TileBusinessPermit> tilesBusinessPermit;
	private TileBusinessPermit tileBusinessPermitA;
	private TileBusinessPermit tileBusinessPermitB;
	private ColorCouncillor[] councillors;
	private TileBonusRegion tileBonusRegion;
	private boolean tileBonusRegionPresence;

	/**
	 * Constructor of class. It draws the firsts 2 permit tile from deck of
	 * permit tile. At the end balcony will be empty.
	 * 
	 * @param type
	 *            Type of region.
	 * @param cities
	 *            List of cities of region.
	 * @param tileBonusRegion
	 *            Bonus tile of region.
	 * @param deckTileBusinessPermit
	 *            Deck of business permit tile.
	 * @throws InfinityLoopException
	 *             If there are some problem when it draws permit tile from
	 *             deck.
	 */
	public Region(TypeRegion type, List<City> cities, TileBonusRegion tileBonusRegion,
			Deck<TileBusinessPermit> deckTileBusinessPermit) throws InfinityLoopException {

		this.cities = (LinkedList<City>) cities;
		this.type = type;
		this.tileBonusRegion = tileBonusRegion;

		tilesBusinessPermit = deckTileBusinessPermit;
		tilesBusinessPermit.mixMainStack();

		tileBusinessPermitA = tilesBusinessPermit.draw();
		tileBusinessPermitB = tilesBusinessPermit.draw();

		this.councillors = new ColorCouncillor[4];

		tileBonusRegionPresence = true;
	}

	/**
	 * Checks if there is bonus tile of region.
	 * 
	 * @return True is present, else False.
	 */
	public boolean isPresentTileBonusRegion() {
		return tileBonusRegionPresence;
	}

	/**
	 * Take bonus tile of region and set to False isPresentTileBonusRegion.
	 * @return	Bonus tile of region.
	 */
	public TileBonusRegion takeTileBonus() {
		tileBonusRegionPresence = false;
		return this.tileBonusRegion;
	}
	
	/**
	 * Returns type of region.
	 * 
	 * @return Type of region.
	 */
	public TypeRegion getTypeOfRegion() {
		return type;
	}

	/**
	 * Returns list of cities of region.
	 * 
	 * @return List of cities.
	 */
	public List<City> getCity() {
		return cities;
	}

	/**
	 * Searches a city by its name and returns it.
	 * 
	 * @param name
	 *            Name of city.
	 * @return City searched.
	 */
	public City getCity(String name) {
		for (City c : cities) {
			if (c.getName().equalsIgnoreCase(name))
				return c;
		}
		return null;
	}

	/**
	 * Returns an array with 4 element that represents the 4 councillors of
	 * balcony.
	 * 
	 * @return Array of color of councillor.
	 */
	public ColorCouncillor[] getCouncillors() {
		return councillors;
	}

	/**
	 * Returns permit tile A.
	 * 
	 * @return Business permit tile.
	 */
	public TileBusinessPermit getTileBusinessPermitA() {
		return tileBusinessPermitA;
	}

	/**
	 * Returns permit tile B.
	 * 
	 * @return Business permit tile.
	 */
	public TileBusinessPermit getTileBusinessPermitB() {
		return tileBusinessPermitB;
	}

	// Controllata!
	/**
	 * Checks if politic cards meet council.
	 * 
	 * @param cards
	 *            List of politic cards.
	 * @return True, if meet council, else false.
	 * @throws NullPointerException
	 *             If list of cards is null.
	 */
	public boolean checkCardsMeetCouncil(List<CardPolitic> cards) {

		ArrayList<CardPolitic> copyCards;

		if (cards == null) {
			throw new NullPointerException("The ArrayList of CardPolitic cannot be null.");
		}

		if (cards.isEmpty()) {
			return false;
		}

		if (cards.size() > councillors.length) {
			return false;
		}

		copyCards = new ArrayList<>();

		for (CardPolitic card : cards) {
			copyCards.add(card);
		}

		for (int i = 0; i < councillors.length; i++) {
			for (int j = 0; j < copyCards.size(); j++) {
				if (!copyCards.get(j).isSpecial() && councillors[i] == copyCards.get(j).getColor()) {
					copyCards.remove(j);
					break;
				}
			}
		}

		for (int i = 0; i < copyCards.size(); i++) {
			if (copyCards.get(i).isSpecial()) {
				copyCards.remove(i);
				i = -1;
			}
		}

		if (!copyCards.isEmpty()) {
			return false;
		}

		return true;
	}

	/**
	 * Elects a councillors. The last councillor of balcony is lifted from array
	 * of councillors that represents the balcony. The elements of array slide
	 * on the right and the first element become the councillor that you want
	 * elect.
	 * 
	 * @param color	Color of councillor that you want elect.
	 * @return	Color of last councillor that is lifted from balcony.
	 */
	public ColorCouncillor electCouncillor(ColorCouncillor color) {

		ColorCouncillor lastCouncillor;

		lastCouncillor = councillors[councillors.length - 1];

		for (int i = 3; i >= 1; i--) {

			councillors[i] = councillors[i - 1];

		}

		councillors[0] = color;

		return lastCouncillor;
	}

	// Controllata!
	/**
	 * Acquire one of two possible business permit tile.
	 * 
	 * @param AorB
	 *            Selected business permit tile. "A" or "B".
	 * @return Selected business permit tile. If there isn't the tile, return
	 *         null.
	 * @throws WrongInputException
	 *             If the parameter AorB is different from "A" or "B".
	 */
	public TileBusinessPermit acquireTileBusinessPermit(String AorB) throws WrongInputException {

		TileBusinessPermit tileBusinessPermit;

		if ("A".equalsIgnoreCase(AorB)) {
			tileBusinessPermit = tileBusinessPermitA;

			try {
				tileBusinessPermitA = tilesBusinessPermit.draw();
			} catch (InfinityLoopException exception) {
				tileBusinessPermitA = null;
			}

		} else if ("B".equalsIgnoreCase(AorB)) {
			tileBusinessPermit = tileBusinessPermitB;

			try {
				tileBusinessPermitB = tilesBusinessPermit.draw();
			} catch (InfinityLoopException exception) {
				tileBusinessPermitB = null;
			}
		} else {
			throw new WrongInputException("The parameter AorB must be 'A' or 'B'");
		}

		return tileBusinessPermit;
	}

	/**
	 * Change business permit tile.
	 * 
	 * @throws NullPointerException
	 *             If there is less 2 tile on deck.
	 */
	public void changeBusinessPermitTile() {

		if (tileBusinessPermitA == null || tileBusinessPermitB == null) {
			throw new NullPointerException(
					"A business permit tile of region is null, so you can't change business permit tile");
		}
		tilesBusinessPermit.addToBottomDeck(tileBusinessPermitA);
		tilesBusinessPermit.addToBottomDeck(tileBusinessPermitB);

		try {
			tileBusinessPermitA = tilesBusinessPermit.draw();
			tileBusinessPermitB = tilesBusinessPermit.draw();
		} catch (InfinityLoopException e) {
			throw new NullPointerException(
					"A business permit tile of region is null, so you can't change business permit tile");
		}
	}

	@Override
	public String toString() {
		return "Region " + type.toString();
	}
}