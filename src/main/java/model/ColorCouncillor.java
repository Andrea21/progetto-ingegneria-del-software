package model;

import java.awt.Color;
import java.util.Random;

/**
 * Enumerator of color of councillor.<br>
 * BLACK<br>
 * BLUE<br>
 * FUCHSIA<br>
 * ORANGE<br>
 * PURPLE<br>
 * WHITE<br>
 * 
 * @author Federico
 *
 */
public enum ColorCouncillor {
	FUCHSIA("FUCHSIA", new Color(255, 0, 255)), BLACK("BLACK", new Color(0, 0, 0)), PURPLE("PURPLE",
			new Color(138, 43, 226)), WHITE("WHITE", new Color(255, 255, 255)), BLUE("BLUE",
					new Color(0, 0, 255)), ORANGE("ORANGE", new Color(255, 165, 0));

	private String name;
	private Color color;

	private ColorCouncillor(String name, Color color) {
		this.name = name;
		this.color = color;
	}

	/**
	 * Returns name of color of councillor.
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns color in RGB.<br>
	 * (0,0,0) "BLACK"<br>
	 * (0,0,255) "BLUE"<br>
	 * (255,0,255) "FUCHSIA"<br>
	 * (255,165,0) "ORANGE"<br>
	 * (138,43,226) "PURPLE"<br>
	 * (255,255,255) "WHITE"
	 * 
	 * @return
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Returns a random color of councillor.
	 * 
	 * @return Color of councillor.
	 */
	public static ColorCouncillor getRandomColor() {
		Random random = new Random();
		ColorCouncillor[] councillors = ColorCouncillor.values();

		return councillors[random.nextInt(councillors.length)];
	}

	/**
	 * Returns color of councillor by its color RGB.
	 * 
	 * @param input
	 *            RGB color:<br>
	 *            (0,0,0) "BLACK"<br>
	 *            (0,0,255) "BLUE"<br>
	 *            (255,0,255) "FUCHSIA"<br>
	 *            (255,165,0) "ORANGE"<br>
	 *            (138,43,226) "PURPLE"<br>
	 *            (255,255,255) "WHITE"
	 * @return Color of councillor.
	 */
	public static ColorCouncillor parseIn(Color input) {
		for (ColorCouncillor colorCouncillor : ColorCouncillor.values()) {
			if (colorCouncillor.getColor().equals(input)) {
				return colorCouncillor;
			}
		}
		return null;
	}

	/**
	 * Returns color of councillor by its name.
	 * 
	 * @param input
	 *            Name of color: "BLACK", "BLUE", "FUCHSIA", "ORANGE", "PURPLE",
	 *            "WHITE".
	 * @return Color of councillor.
	 */
	public static ColorCouncillor parseIn(String input) {
		for (ColorCouncillor colorCouncillor : ColorCouncillor.values()) {
			if (colorCouncillor.getName().equalsIgnoreCase(input)) {
				return colorCouncillor;
			}
		}
		return null;
	}
}
