package model;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import bonusandcards.*;

/**
 * Class that manages the data of a player and provides method to do main
 * actions and quick actions.
 * 
 * @author Federico
 *
 */
public class Player implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean active;
	private final String name;
	private final Color color;
	private int positionVictory;
	private int positionNobility;
	private int positionCoinsTrack;
	private List<CardPolitic> cardsPolitic;
	private List<TileBusinessPermit> tilesBusinessPermit;
	private List<TileBusinessPermit> refuseTilesBusinessPermit;
	private List<TileBonus> tilesBonus;
	private boolean doubleAction;
	private int assistants;
	private int numberAvailableEmporium;
	private List<ObjectForSale> objectsForSale;
	private String specialNobilityBonus;
	private final Deck<CardPolitic> globalDeck; // Necessary to draw politic
												// card when
	private final Map<Integer, ArrayList<Bonus>> bonusNobilityTrack;
	// I take a bonus

	/**
	 * Class contractor.
	 * @param namePlayer	Name of player.
	 * @param colorPlayer	Color of player.
	 * @param gameDeck	Deck of card politic of game.
	 * @param bonusNobility	List of bonus on nobility track on class kingboard.
	 * @param numberEmporiums Number of initial available emporiums.
	 */
	public Player(String namePlayer, Color colorPlayer, Deck<CardPolitic> gameDeck,
			Map<Integer, ArrayList<Bonus>> bonusNobility, int numberEmporiums) {
		active = true;
		name = namePlayer;
		color = colorPlayer;
		globalDeck = gameDeck;
		bonusNobilityTrack = bonusNobility;
		cardsPolitic = new ArrayList<>();
		for (int i = 0; i < 6; i++) {
			drawCardPolitic();
		}
		tilesBusinessPermit = new ArrayList<>();
		refuseTilesBusinessPermit = new ArrayList<>();
		tilesBonus = new ArrayList<>();
		numberAvailableEmporium = numberEmporiums;
		objectsForSale = new ArrayList<ObjectForSale>();
		specialNobilityBonus = "";
	}

	/**
	 * Checks if a player is active or not.
	 * 
	 * @return True is active, False not.
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets if player is active or not.
	 * 
	 * @param isActive
	 *            True player is active, False not.
	 */
	public void setActive(boolean isActive) {
		active = isActive;
	}

	/**
	 * Gets the name of player.
	 * 
	 * @return Name of player.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the color of player.
	 * 
	 * @return Color of player.
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Sets the position of player on victory track.
	 * 
	 * @param position
	 *            Position of player on victory track.
	 * @exception NegativeException
	 *                If position is negative.
	 */
	public void setPositionVictory(int position) {
		if (position < 0) {
			throw new NegativeException("The position of victory cannot be negative");
		}

		positionVictory = position;
	}

	/**
	 * Gets the position of player on victory track.
	 * 
	 * @return Position of player on victory track.
	 */
	public int getPositionVictory() {
		return positionVictory;
	}

	/**
	 * Sets the position of player on nobility track and checks if there are some
	 * bonus on new position of nobility track.
	 * 
	 * @param position
	 *            Position of player on nobility track.
	 * @exception NegativeException
	 *                If position  is negative.
	 */
	public void setPositionNobility(int position) {
		if (position < 0) {
			throw new NegativeException("The position of nobility cannot be negative");
		}

		positionNobility = position;

		if (bonusNobilityTrack.get(positionNobility) != null) {
			for (Bonus bonus : bonusNobilityTrack.get(positionNobility)) {
				bonus.takeBonus(this);
			}
		}

	}

	/**
	 * Gets the position of player on nobility track.
	 * 
	 * @return Position of player on nobility track.
	 */
	public int getPositionNobility() {
		return positionNobility;
	}

	/**
	 * Sets the position of player on coins track. (Richness of player)
	 * 
	 * @param position
	 *            Position of player on coins track.
	 * @exception NegativeException
	 *                If position is negative.
	 */
	public void setPositionCoinsTrack(int position) {
		if (position < 0) {
			throw new NegativeException("The position of coins track cannot be negative");
		}

		positionCoinsTrack = position;
	}

	/**
	 * Gets the position of player on coins track. (Richness of player)
	 * 
	 * @return Position of player on coins track.
	 */
	public int getPositionCoinsTrack() {
		return positionCoinsTrack;
	}

	/**
	 * Adds coins to the player quickly.
	 *
	 * @param number
	 *            Number of coins. It can be negative if you want subtract
	 *            coins.
	 * @exception NegativeException
	 *                If the sum of actual position on coins track and _Number
	 *                parameter is negative.
	 */
	public void addCoins(int number) {
		if (positionCoinsTrack + number < 0) {
			throw new NegativeException("After the operation AddCoins the number of assistants cannot be negative.");
		}

		positionCoinsTrack += number;
	}

	/**
	 * Signals if player can do another main action or not.
	 * @param yesNo	True if he can do, False if he can't.
	 */
	public void setDoubleAction(boolean yesNo) {
		doubleAction = yesNo;
	}

	/**
	 * To know if player can do another main action.
	 * @return	Returns True if he can, else False.
	 */
	public boolean getDoubleAction() {
		return doubleAction;
	}

	/**
	 * Set number of assistants owned by player.
	 * @param number	Number of assistants.
	 * @throws	NegativeException If number is negative.
	 */
	public void setAssistants(int number) {
		if (number < 0) {
			throw new NegativeException("The number of assistants cannot be negative");
		}

		assistants = number;
	}

	/**
	 * Gets number of assistants owned by player.
	 * @return	Number of assistants.
	 */
	public int getAssistants() {
		return assistants;
	}

	/**
	 * Adds assistants to player.
	 * @param number	Number of assistants.
	 */
	public void addAssistants(int number) {
		if (assistants + number < 0) {
			throw new NegativeException(
					"After the operation AddAssistants the number of assistants cannot be negative.");
		}

		assistants += number;

	}

	/**
	 * Gets number of available emporiums of player.
	 * @return	Number of available emporiums.
	 */
	public int getNumberAvailableEmporium() {
		return numberAvailableEmporium;
	}

	/**
	 * Returns list of politic cards of player.
	 * @return	List of politic cards.
	 */
	public List<CardPolitic> getcardsPolitic() {
		return cardsPolitic;
	}

	// Testata!
	/**
	 * Checks if player has all politic cards of the list.
	 * 
	 * @param cards
	 *            List of politic cards.
	 * @return True if player has all politic cards, else false.
	 * @throws NullPointerException
	 *             If list of politic cards is null.
	 */
	public boolean haveCardsPolitic(List<CardPolitic> cards) {
		ArrayList<CardPolitic> supportList;

		if (cards == null) {
			throw new NullPointerException("The list of politic cards cannot be negative");
		}

		supportList = new ArrayList<>();

		for (CardPolitic card : cards) {
			supportList.add(card);
		}

		for (CardPolitic playerCard : cardsPolitic) {
			for (CardPolitic localCard : supportList) {
				if (playerCard.equals(localCard)) {
					supportList.remove(localCard);
					break;
				}
			}
		}

		return supportList.isEmpty();
	}

	/**
	 * Gets list of business permit tiles.
	 * @return	Returns list of business permit tiles.
	 */
	public List<TileBusinessPermit> getTileBusinessPermit() {
		return tilesBusinessPermit;
	}

	/**
	 * Adds a tile bonus to player.
	 * 
	 * @param tileBonus
	 *            Tile bonus to add.
	 */
	public void addTileBonus(TileBonus tileBonus) {
		tilesBonus.add(tileBonus);
	}

	/**
	 * Gets list of tile bonus. Region tile bonus, color tile bonus, king tile bonus.
	 * @return	Returns list of tile bonus.
	 */
	public List<TileBonus> getTilesBonus() {
		return tilesBonus;
	}

	// Controllata!
	/**
	 * Gets the business permit tile associated to identifier.
	 * 
	 * @param identifier
	 *            identifier of business permit tile.
	 * @return business permit tile. Returns null if player haven't the tile.
	 */
	public TileBusinessPermit getTileBusinessPermit(String identifier) {
		for (TileBusinessPermit tileBusinessPermit : tilesBusinessPermit) {
			if (identifier.equalsIgnoreCase(tileBusinessPermit.toString())) {
				return tileBusinessPermit;
			}
		}
		return null;
	}

	public List<TileBusinessPermit> getRefuseTileBusinessPermit() {
		return refuseTilesBusinessPermit;
	}

	public String getSpecialNobilityBonus() {
		return specialNobilityBonus;
	}

	public void setSpecialNobilityBonus(String specialNobilityBonus) {
		this.specialNobilityBonus = specialNobilityBonus;
	}

	// Testata!
	/**
	 * Draws a politic card from game's deck and adds it to deck of player. If
	 * deck of game is empty, desn't add a card to player's deck.
	 */
	public void drawCardPolitic() {
		try {
			cardsPolitic.add(globalDeck.draw());
		} catch (InfinityLoopException exception) {
			// If there isn't politic card, beh, don't take bonus.
		}
	}

	// Testata!
	/**
	 * Acquires selected business permit tile. Removes politic cards, adds they to
	 * refuse deck of board and pays coins. Takes bonus of tile.
	 * 
	 * @param tileBusinessPermit
	 *            Selected business permit tile.
	 * @param cards
	 *            List of politic cards to remove.
	 * @param numberCoins
	 *            Number of coins to pay
	 * @throws NullPointerException
	 *             If list of politic cards or business permit tile is null.
	 * @throws NegativeException
	 *             If number of coins to pay is negative
	 * @throws NotFoundException
	 *             If player hasn't a politic card that it's selected.
	 */
	public void aquireTileBusinessPermit(TileBusinessPermit tileBusinessPermit, List<CardPolitic> cards,
			Integer numberCoins) {

		if (tileBusinessPermit == null) {
			throw new NullPointerException("The tile business permit cannot be null.");
		}

		if (cards == null) {
			throw new NullPointerException("The list of politic cards cannot be null.");
		}

		if (numberCoins < 0) {
			throw new NegativeException("The number of coins that player must pay, cannot be negative.");
		}

		if (positionCoinsTrack - numberCoins < 0) {
			throw new NegativeException("The number of coins of player cannot be negative.");
		}

		if (!haveCardsPolitic(cards)) {
			throw new NotFoundException("I try to remove a politic card that player haven't.");
		}

		for (CardPolitic localCard : cards) {
			for (CardPolitic playerCard : cardsPolitic) {
				if (playerCard.equals(localCard)) {
					cardsPolitic.remove(playerCard);
					globalDeck.addRefuse(playerCard);
					break;
				}
			}
		}

		tileBusinessPermit.doBonus(this);
		tilesBusinessPermit.add(tileBusinessPermit);
		addCoins(-numberCoins);
	}

	/**
	 * Elects a councillor. In particular for player, adds 4 coins to
	 * him.
	 */
	public void electCouncillor() {
		addCoins(4);
	}

	// Testata!
	/**
	 * Builds an emporium using a business permit tile. Removes business permit
	 * tile and pays assistants.
	 * 
	 * @param tileBusinessPermit
	 *            Selected business permit tile.
	 * @param numberAssistants
	 *            Number of assistants that player must pay.
	 * @throws NegativeException
	 *             If player hasn't enough available emporium or enough
	 *             assistants.
	 * @throws NullPointerException
	 *             If player hasn't the selected business permit tile.
	 */
	public void buildEmporiumPermitTile(TileBusinessPermit tileBusinessPermit, Integer numberAssistants) {
		boolean founded;

		if (numberAvailableEmporium - 1 < 0) {
			throw new NegativeException("The number of available emporium cannot be negative");
		}
		if (assistants - numberAssistants < 0) {
			throw new NegativeException("The number of assistants cannot be negative");
		}

		founded = false;
		for (TileBusinessPermit tile : tilesBusinessPermit) {
			if (tile.toString().equalsIgnoreCase(tileBusinessPermit.toString())) {
				tilesBusinessPermit.remove(tile);
				refuseTilesBusinessPermit.add(tile);
				founded = true;
				break;
			}
		}

		// Remove and check if player has business permit tile
		if (!founded) {
			throw new NullPointerException("Player haven't the business permit tile");
		}

		numberAvailableEmporium--;
		addAssistants(-numberAssistants);
	}

	// Testata!
	/**
	 * Builds an emporium with the help of king. Removes politic cards, adds they
	 * to refuse deck of board, pays assistants and coins.
	 * 
	 * @param numberCoins
	 *            Number of coins that player must pay.
	 * @param numberAssistants
	 *            Number of assistants that player must pay.
	 * @param cards
	 *            List of politic cards that player uses to build.
	 * @throws NegativeException
	 *             If player hasn't enough available emporium or enough
	 *             assistants or coins.
	 * @throws NullPointerException
	 *             If list of politic cards is null.
	 * @throws NotFoundException
	 *             If player hasn't a politic card that it's selected.
	 */
	public void buildEmporiumKing(Integer numberCoins, Integer numberAssistants, List<CardPolitic> cards) {

		if (numberAvailableEmporium - 1 < 0) {
			throw new NegativeException("The number of available emporium cannot be negative");
		}
		if (assistants - numberAssistants < 0) {
			throw new NegativeException("The number of assistants cannot be negative");
		}
		if (positionCoinsTrack - numberCoins < 0) {
			throw new NegativeException("The number of coins cannot be negative");
		}

		if (cards == null) {
			throw new NullPointerException("The list of politic cards cannot be null.");
		}

		if (!haveCardsPolitic(cards)) {
			throw new NotFoundException("I try to remove a politic card that player haven't.");
		}

		for (CardPolitic localCard : cards) {
			for (CardPolitic playerCard : cardsPolitic) {
				if (playerCard.equals(localCard)) {
					cardsPolitic.remove(playerCard);
					globalDeck.addRefuse(playerCard);
					break;
				}
			}
		}

		numberAvailableEmporium--;
		addAssistants(-numberAssistants);
		addCoins(-numberCoins);
	}

	/**
	 * Engages a new assistant. Pays 3 coins.
	 */
	public void engageAssistant() {
		addCoins(-3);
		addAssistants(1);
	}

	/**
	 * Changes business permit tile. Pays an assistant.
	 */
	public void changeBusinessPermitTile() {
		addAssistants(-1);
	}

	/**
	 * Sends an assistant to elect a councillor. Pays an assistant.
	 */
	public void assistantForCouncillor() {
		addAssistants(-1);
	}

	/**
	 * Adds assistants for sale. Method does NOT check if player has the number of assistants specified. 
	 * 
	 * @param numberOfAssistants
	 *            Number of assistants for sale.
	 * @param price
	 *            Price for each assistant.
	 */
	public void addAssistantsForSale(int numberOfAssistants, int price) {
		if (price < 0) {
			throw new NegativeException("The price of an onject cannot be negative");
		}
		objectsForSale.add(new ObjectForSale(numberOfAssistants, price));
	}

	/**
	 * Adds a politic card for sale. Method does NOT check if player has the politic cards specified.
	 * 
	 * @param cardPolitic
	 *            Politic card for sale.
	 * @param price
	 *            Price of politic card.
	 */
	public void addCardPoliticForSale(CardPolitic cardPolitic, int price) {
		if (price < 0) {
			throw new NegativeException("The price of an onject cannot be negative");
		}
		if (cardPolitic == null) {
			throw new NullPointerException("The politic card for sale cannot be null.");
		}
		objectsForSale.add(new ObjectForSale(cardPolitic, price));
	}

	/**
	 * Adds a business permit tile for sale. Method does NOT check if player has the business permit tile specified.
	 * 
	 * @param businessPermit
	 *            Business permit tile for sale.
	 * @param price
	 *            Price of permit tile.
	 */
	public void addTileBusinessPermitForSale(TileBusinessPermit businessPermit, int price) {
		if (price < 0) {
			throw new NegativeException("The price of an onject cannot be negative");
		}
		if (businessPermit == null) {
			throw new NullPointerException("The business permit tile for sale cannot be null.");
		}
		objectsForSale.add(new ObjectForSale(businessPermit, price));
	}

	/**
	 * Returns list of objects for sale of player.
	 * @return
	 */
	public List<ObjectForSale> getObjectForSale() {
		return objectsForSale;
	}

	/**
	 * Clears list of objects for sale of player.
	 */
	public void emptiesObjectForSale() {
		objectsForSale.clear();
	}

	// Testata!
	/**
	 * Checks if an object is on sale.
	 * 
	 * @param objectForSale
	 *            Object to see if it's for sale.
	 * @return True if object is on sale. Else false.
	 */
	public boolean haveForSale(ObjectForSale objectForSale) {
		for (ObjectForSale obj : objectsForSale) {
			if (objectForSale.equals(obj))
				return true;
		}
		return false;
	}

	// Testata!
	/**
	 * Checks if objects are on sale.
	 * 
	 * @param objectsForSale
	 *            List of objects to see if they're for sale.
	 * @return True if objects are all on sale. Else false.
	 */
	public boolean haveForSale(List<ObjectForSale> listObjects) {
		ArrayList<ObjectForSale> tempList;
		int totalAssistants;

		if (objectsForSale == null) {
			throw new NullPointerException("The list of object for sale to be checked cannot be null.");
		}

		totalAssistants = 0;
		
		tempList = new ArrayList<>();
		for (ObjectForSale object: listObjects){
			tempList.add(object);
		}

		for (int i = 0; i < tempList.size(); i++) {
			if (tempList.get(i).isAssistants()) {
				totalAssistants += tempList.get(i).getAssistants();
				tempList.remove(i);
				i--;
			}
		}

		for (ObjectForSale obj : objectsForSale) {
			if (obj.isAssistants()) {
				if (obj.getAssistants() < totalAssistants)
					return false;
			} else {
				for (int i = 0; i < tempList.size(); i++) {
					if (obj.equals(tempList.get(i))) {
						tempList.remove(i);
						i = tempList.size();
					}
				}
			}
		}

		return tempList.isEmpty();
	}

	// Testata!
	/**
	 * Gets the price of an object. First the most economical if two objects are
	 * equal.
	 * 
	 * @param objects
	 *            Object for sale.
	 * @return Price of object.
	 */
	public int getPriceOfObjects(List<ObjectForSale> objects) {
		// Restituisco sempre il prezzo dell'oggetto più economico se ci sono
		// doppioni
		int totalCoins;
		List<ObjectForSale> tempList;
		ObjectForSale tempElement;
		
		if (objects == null) {
			throw new NullPointerException("The list of object for sale to be checked cannot be null.");
		}
		
		totalCoins = 0;

		tempList = new ArrayList<>();
		for (ObjectForSale object: objectsForSale){
			tempList.add(object);
		}

		for (ObjectForSale obj : objects) {
			tempElement = null;
			for (int i = 0; i < tempList.size(); i++) {
				if (obj.isAssistants() && tempList.get(i).isAssistants()) {
					tempElement = tempList.get(i);
				} else if (obj.equals(tempList.get(i))) {
					if (tempElement == null) {
						tempElement = tempList.get(i);
					}
					if (tempElement.getPrice() > tempList.get(i).getPrice()) {
						tempElement = tempList.get(i);
					}
				}
			}
			if (tempElement != null) {
				if (tempElement.isAssistants()) {
					totalCoins += tempElement.getPrice() * obj.getAssistants();
				} else {
					totalCoins += tempElement.getPrice();
					tempList.remove(tempElement);
				}
			}
		}

		return totalCoins;
	}

	// Testata!
	/**
	 * Buys object. Removes object and adds coins of price.
	 * 
	 * @param objectForSale
	 *            Object that you want buy.
	 * @return Price of object.
	 * @throws NegativeException
	 *             If selected object isn't in the list of objects for sale.
	 */
	public int buyObject(ObjectForSale objectForSale) {
		// Compro sempre l'oggetto al minor prezzo
		int price;
		ObjectForSale tempElement;

		price = -1;

		if (objectForSale.isAssistants()) {
			for (ObjectForSale obj : objectsForSale) {
				if (obj.isAssistants()) {
					price = obj.getPrice() * objectForSale.getAssistants();
					objectsForSale.remove(obj);
					obj = new ObjectForSale(obj.getAssistants() - objectForSale.getAssistants(), obj.getPrice());
					objectsForSale.add(0, obj);
					break;
				}
			}
			addAssistants(-objectForSale.getAssistants());
		} else {
			tempElement = null;
			for (ObjectForSale obj : objectsForSale) {
				if (obj.equals(objectForSale)) {
					if (tempElement == null) {
						tempElement = obj;
					}
					if (tempElement.getPrice() > obj.getPrice()) {
						tempElement = obj;
					}
				}
			}
			if (tempElement != null) {
				price = tempElement.getPrice();
				objectsForSale.remove(tempElement);

				if (objectForSale.isCardPolitc()) {
					cardsPolitic.remove(objectForSale.getCardPolitic());
				} else if (objectForSale.isTileBusinessPermit()) {
					tilesBusinessPermit.remove(objectForSale.getTileBusinessPermit());
				}
			}
		}
		if (price == -1) {
			price = 0;
		}
		addCoins(price);
		return price;
	}

	/**
	 * Adds a politic card to player.
	 * @param card	Politic card.
	 */
	public void addCardPolitic(CardPolitic card) {
		cardsPolitic.add(card);
	}

	/**
	 * Adds a business permit tile to player.
	 * @param tile	Permit tile.
	 */
	public void addBusinessPermit(TileBusinessPermit tile) {
		this.tilesBusinessPermit.add(tile);
	}
}
