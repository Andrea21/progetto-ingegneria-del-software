package model;

public class WrongInputException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public WrongInputException()
	{ 
		super(); 
	} 
	public WrongInputException(String s)
	{ 
		super(s);
	} 
}
