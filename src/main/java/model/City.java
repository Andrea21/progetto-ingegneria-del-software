package model;

import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.NoSuchElementException;

import bonusandcards.*;

/**
 * City class it generates and manages the cities in the map
 * @author Federico
 *
 */
public class City implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private ColorCity color;
	private TokenReward reward;
	private ArrayList<Emporium> emporiums;
	private LinkedList<City> nearbyCities;
	private Point position;

	static LinkedList<City> listaVisita = new LinkedList<>();
	
	/**
	 * Class constructor. Generates a city
	 * 
	 * @param name
	 * 			The name of the city
	 * @param color
	 * 			The color of the city
	 * @param position
	 * 			The position of the city
	 * @param tokenReward
	 * 			The token reward of the city
	 */
	public City(String name, ColorCity color, Point position, TokenReward tokenReward) {
		
		this.name = name;
		this.color = color;
		this.position = position;
		this.nearbyCities = new LinkedList<City>();
		this.emporiums = new ArrayList<>();

		reward = tokenReward;
	}
	
	/**
	 * Class constructor. Generates a city without a token reward
	 * 
	 * @param name
	 * 			The name of the city
	 * @param color
	 * 			The color of the city
	 * @param position
	 * 			The position of the city
	 */
	public City(String name, ColorCity color, Point position) {
		this.name = name;
		this.color = color;
		this.position = position;
		this.nearbyCities = new LinkedList<City>();
		this.emporiums = new ArrayList<>();
		
		reward = null;
	}

	/**
	 * Return the city nearby this city with the name "nameCity"
	 * 
	 * @param nameCity
	 * 			The name of the city you want to be returned
	 * 
	 *  @return The city you selected
	 *  		
	 */
	public City getNearbyCity(String nameCity) {
		
		
	
		for (City c : nearbyCities) {
			if (c.getName().equalsIgnoreCase(nameCity)) {
				return c;
			}
		}
		return null;
	}

	/**
	 * Returns a list containing the cities connected to this city where there is an emporium owned by the parameter "player"
	 * 
	 * @param player
	 * 			The name of the player 
	 * 
	 *  @return The list of the cities containing an emporium owned by the player
	 *  		
	 */
	public LinkedList<City> getConnectedCity(Player player) { 
		
	
		if (listaVisita == null) {
			listaVisita = new LinkedList<>();
		}

		else {
			listaVisita.clear();
			listaVisita.add(this);
		}

		cityVisit(this, player);
		listaVisita.remove(this);
		return listaVisita;

	}

	/**
	 * Performs a visit of the Map, adding to a list the cities containing an emporium owned by the parameter "player".
     * It Stops when the city visited hasn't an emporium owned by the player. 
	 * 
	 * @param startingCity
	 * 			The city where you start the visit		
	 * 
	 * @param player
	 * 			The name of the player 	
	 */
	public static void cityVisit(City startingCity, Player player) { 

		for (City city : startingCity.nearbyCities) {
			for (Emporium e: city.getEmporiums()) {
				if (e.isOccupied() && e.getColor().equals(player.getColor())
						&& !listaVisita.contains(city)) {
					listaVisita.add(city);
					cityVisit(city, player);
				}
			}

		}
	}

	/**
	 * Returns token rewards of the city.
	 * 
	 * @return Token rewards of the city.
	 */
	public TokenReward getTokenReward() { 
		return reward;
	}

	/**
	 * Returns name of the city.
	 * 
	 * @return The name of the city.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Adds the selected city to the list of nearby city.
	 * 
	 * @param nearbyCity
	 * 		The nearby city you want to add
	 */
	public void addNearbyCities(City nearbyCity) {
		this.nearbyCities.push(nearbyCity);
	}

	/**
	 * Returns the color of the city.
	 * 
	 * @return The color of the city.
	 */
	public ColorCity getColor() {
		return this.color;
	}

	/**
	 * Returns the x position of the city.
	 * 
	 * @return The x position of the city.
	 */
	public int getPosX() {
		return this.position.x;
	}

	/**
	 * Returns the y position of the city.
	 * 
	 * @return The y position of the city.
	 */
	public int getPosY() {
		return this.position.y;
	}
	
	/**
	 * Returns the list of emporiums of the city.
	 * 
	 * @return The list of emporiums of the city.
	 */
	public ArrayList<Emporium> getEmporiums(){
		return emporiums;
	}

	/**
	 * Returns the list of nearby cities of the city.
	 * 
	 * @return The list of nearby cities of the city.
	 */
	public LinkedList<City> getNearbyCities() {
		return this.nearbyCities;
	}
	
	/**
	 * Returns the lista visita of the city.
	 * 
	 * @return The lista visita of the city.
	 */
	public LinkedList<City> getListaVisita() {
		return City.listaVisita;
	}

	/**
	 * Build an emporium in the city with the color of the selected player
	 * 
	 * @param player
	 * 			Teh selected player 		
	 */
	public void buildEmporium(Player player) throws NoSuchElementException { 
		if (!hasEmporium(player)) {
			Emporium emporium = new Emporium(player.getColor());
			emporium.setOccupaid();
			emporiums.add(emporium);
		}
	}

	/**
	 * Checks if the player has an emporium in the city
	 * 
	 * @param player
	 * 			The player you want to check
	 * 
	 * @return True if the player has an emporium, false otherwise
	 */
	public boolean hasEmporium(Player player) { 
		
		boolean flag = false;
		for (Emporium emporium : emporiums) {
			if (emporium.isOccupied() && emporium.getColor().equals(player.getColor())) {
				flag = true;
			}
		}
		return flag;
	}
	
	/**
	 * Returns the number of builded emporium if the city.
	 * 
	 * @return The number of builded emporium if the city.
	 */
	public int getNumberOfBuildedEmporium(){
		return emporiums.size();
	}
	
	/**
	 * Sets the token reward of the city
	 * 
	 * @param tokenReward
	 * 		The token you want to set
	 */
	public void setTokenReward(TokenReward tokenReward) {
		reward = tokenReward;
	}

	@Override
	public String toString() {
		return "City " + name + " color " + color.toString();
	}
}