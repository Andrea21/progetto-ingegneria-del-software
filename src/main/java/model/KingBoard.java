package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import bonusandcards.*;

/**
 * Class that represents king's board. It has position of king, nobility track, deck of king's reward and balcony.
 * @author Federico
 *
 */
public class KingBoard implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private City kingCity;
	private ColorCouncillor[] councillors;
	private Map<Integer, ArrayList<Bonus>> bonusNobilityTrack;
	private Deck<TileKingsReward> tilesKingReward;
	
	/**
	 * Constructor of class.
	 * @param purpleCity	Start position of king.
	 * @param tilesKingsRewards	List of king's reward tiles.
	 */
	public KingBoard(City purpleCity, List<TileKingsReward> tilesKingsRewards) {
		this.kingCity = purpleCity;

		bonusNobilityTrack = new HashMap<>();
		this.tilesKingReward = new Deck<>(false);

		this.councillors = new ColorCouncillor[4];

		for (int i = 0; i < councillors.length; i++) {
			councillors[i] = ColorCouncillor.getRandomColor();
		}

		for (int i = tilesKingsRewards.size() - 1; i > 0; i--) {
			if (tilesKingsRewards.get(i - 1).getNumber() == i)
				this.tilesKingReward.add(tilesKingsRewards.get(i - 1));
		}
	}

	/**
	 * Draws a king's reward tile.
	 * @return	king's reward tile.
	 * @throw InfinityLoopException if deck of tile is empty.
	 */
	public TileKingsReward drawTileKingReward() {
		try {
			return tilesKingReward.draw();
		} catch (InfinityLoopException exception) {
			return null;
		}
	}

	/**
	 * Returns reward bonus tile of king.
	 * @return	Iterator of TileKingsReward.
	 */
	public Iterator<TileKingsReward> getTileKingsReward(){
		return tilesKingReward.getItem();
	}
	
	/**
	 * Get bonus of specific cell of nobility track.
	 * @return	Return list of bonus. Return null if the cell hasn't a bonus.
	 */
	public Map<Integer, ArrayList<Bonus>> getBonusNobilityTrack() {
		return bonusNobilityTrack;
	}

	/**
	 * Return the bonus of the cell of nobility track specified.
	 * @param cellNumber	Number of cell of nobility track.
	 * @return	Bonus of cell.
	 */
	public List<Bonus> getBonusNobilityTrack(int cellNumber) {
		return bonusNobilityTrack.get(cellNumber);
	}

	/**
	 * Returns an array with 4 element that represents the 4 councillors of
	 * balcony.
	 * 
	 * @return Array of color of councillor.
	 */
	public ColorCouncillor[] getCouncillors() {
		return councillors;
	}
	
	/**
	 * Returns the city where is the king.
	 * @return	City where is king.
	 */
	public City getKingCity() {
		return kingCity;
	}

	/**
	 * Moves the king to a city.
	 * @param city	City where moves the king.
	 */
	public void moveKing(City city) {
		this.kingCity = city;
	}

	/**
	 * Elects a councillors. The last councillor of balcony is lifted from array
	 * of councillors that represents the balcony. The elements of array slide
	 * on the right and the first element become the councillor that you want
	 * elect.
	 * 
	 * @param color	Color of councillor that you want elect.
	 * @return	Color of last councillor that is lifted from balcony.
	 */
	public ColorCouncillor electCouncillor(ColorCouncillor councillor) {
		ColorCouncillor lastCouncillor;

		lastCouncillor = councillors[councillors.length - 1];

		for (int i = 3; i >= 1; i--) {
			councillors[i] = councillors[i - 1];
		}
		councillors[0] = councillor;

		return lastCouncillor;
	}

	/**
	 * Checks if politic cards meet council.
	 * 
	 * @param cards
	 *            List of politic cards.
	 * @return True, if meet council, else false.
	 * @throws NullPointerException
	 *             If list of cards is null.
	 */
	public boolean checkCardsMeetCouncil(List<CardPolitic> cards) {

		ArrayList<CardPolitic> copyCards;

		if (cards == null) {
			throw new NullPointerException("The ArrayList of CardPolitic cannot be null.");
		}

		if (cards.isEmpty()) {
			return false;
		}

		if (cards.size() > councillors.length) {
			return false;
		}

		copyCards = new ArrayList<>();

		for (CardPolitic card : cards) {
			copyCards.add(card);
		}

		for (int i = 0; i < councillors.length; i++) {
			for (int j = 0; j < copyCards.size(); j++) {
				if (!copyCards.get(j).isSpecial() && councillors[i] == copyCards.get(j).getColor()) {
					copyCards.remove(j);
					break;
				}
			}
		}

		for (int i = 0; i < copyCards.size(); i++) {
			if (copyCards.get(i).isSpecial()) {
				copyCards.remove(i);
				i = -1;
			}
		}

		if (!copyCards.isEmpty()) {
			return false;
		}

		return true;
	}
}