package model;

import java.awt.Point;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import bonusandcards.Bonus;
import bonusandcards.CardPolitic;
import bonusandcards.Deck;
import bonusandcards.InfinityLoopException;
import bonusandcards.TileBonusColor;
import bonusandcards.TileBonusRegion;
import bonusandcards.TileBusinessPermit;
import bonusandcards.TileKingsReward;
import bonusandcards.TokenReward;

/**
 * Board class it generates and manages some aspects of the map
 * @author Federico
 *
 */
public class Board implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Region[] regions;
	private KingBoard kingBoard;
	private Deck<CardPolitic> deckCardPolitic;
	private ArrayList<TileBonusColor> tilesBonusColor;
	private int poolAssistant;
	private ArrayList<ColorCouncillor> councillors;

	/**
	 * Class constructor. Generates the map from a configuration file
	 * 
	 * @param pathFile
	 * 			The path to the configuration file
	 */
	public Board(String pathFile) throws FactoryConfigurationError, ParserConfigurationException, SAXException,
			IOException, WrongInputException, InfinityLoopException {

		regions = new Region[3];
		tilesBonusColor = new ArrayList<TileBonusColor>();
		councillors = new ArrayList<ColorCouncillor>();
		deckCardPolitic = new Deck<CardPolitic>(true);
		generateMap(pathFile);
	}
	
	/**
	 * Method that creates the map
	 * 
	 * @param path
	 * 			The path to the configuation file
	 */
	private void generateMap(String path) throws FactoryConfigurationError, ParserConfigurationException, SAXException,
			IOException, WrongInputException, InfinityLoopException {
		handleDocument(utility.FileUtility.openXML(path));
	}

	/**
	 * Create regions, cities of regions and king board from xml file
	 * configuration.
	 * 
	 * @param document
	 *            document of xml file. Look DocumentBuilderFactory.
	 * @throws InfinityLoopException
	 *             Error of Deck class. It isn't likely.
	 * @throws IOException
	 *             Error if file xml has an invalid format of data.
	 */
	private void handleDocument(Document document) throws InfinityLoopException, IOException {
		/* Variables to extract business permit tile */
		NodeList nodeListTileBusinessPermit;
		NodeList nodeListInitialCities;
		Element elementTile;
		ArrayList<String> inititalOfCities;
		TileBusinessPermit tileBusinessPermit;
		HashMap<TypeRegion, Deck<TileBusinessPermit>> decksTileBusinessPermit;

		/* Variables to extract cities */
		NodeList nodeListRegion;
		NodeList nodeListCity;
		NodeList nodeListNearbyCity;
		NodeList nodeListToken;

		Element elementMap;
		Element elementRegion;
		Element elementcity;
		Element elementBonusRegion;

		Region region;
		TypeRegion typeRegion;
		City city;
		ColorCity colorCity;
		String nameCity;
		LinkedList<City> citiesInRegion;
		Point positionCity;
		HashMap<City, ArrayList<String>> nearbyCity;
		TileBonusRegion tileBonusRegion;

		/* Variables to extract reward tokens */
		Random rnd;
		Integer x;
		TokenReward tokenReward;
		ArrayList<City> allCities;
		Boolean exit;

		/* Variables to extract color bonus tiles */
		NodeList nodeListTileBonusColor;

		/* Variables to extract king board */
		Element elementKingBoard;
		NodeList nodeListCell;
		NodeList nodeListTileKingsReward;
		ArrayList<TileKingsReward> tilesKingsReward;
		int number;

		/* Variable to extract number of assistants and councillors */
		NodeList nodeListCouncillors;
		Element elementDetails;
		Element elementAssistants;
		Element elementCouncillor;

		/* Variable to extract politic cards */
		NodeList nodeListCardsPolitic;
		Element elementCardPolitic;

		/* Support variables */
		NodeList nodeListBonus;
		Element elementBonus;
		String temp;
		Bonus bonus;
		
		/* Initialize some variables */
		rnd = new Random();
		nearbyCity = new HashMap<City, ArrayList<String>>();
		allCities = new ArrayList<City>();
		tilesKingsReward = new ArrayList<TileKingsReward>();
		decksTileBusinessPermit = new HashMap<TypeRegion, Deck<TileBusinessPermit>>();
		inititalOfCities = new ArrayList<String>();

		/* Extract business permit tile nodes */
		try {
			nodeListTileBusinessPermit = document.getElementsByTagName("TileBusinessPermit");
		} catch (Exception e) {
			throw new IOException("Error on reading of xml file in Board class.");
		}

		/* Loop for each business permit tile node */
		for (int i = 0; i < nodeListTileBusinessPermit.getLength(); i++) {

			try {
				elementTile = (Element) nodeListTileBusinessPermit.item(i);
				temp = elementTile.getAttribute("Region");

				/*
				 * Extract the node list of initial of cities and node list of
				 * bonus
				 */
				nodeListInitialCities = elementTile.getElementsByTagName("City");
				nodeListBonus = elementTile.getElementsByTagName("Bonus");
			} catch (Exception e) {
				throw new IOException("Error on reading of xml file in Board class.");
			}

			if(!inititalOfCities.isEmpty()){
				inititalOfCities.clear();
			}
			/* Loop for each initial of city node */
			for (int j = 0; j < nodeListInitialCities.getLength(); j++) {
				try {
					inititalOfCities.add(nodeListInitialCities.item(j).getTextContent().split("\n")[0]);
				} catch (Exception e) {
					throw new IOException("Error on reading of xml file in Board class.");
				}
			}

			try {
				/* Create the tile without bonus */
				tileBusinessPermit = new TileBusinessPermit(TypeRegion.parseIn(temp));
				for(String c: inititalOfCities){
				tileBusinessPermit.getCities().add(c);
				}
			} catch (Exception e) {
				throw new IOException("Error on reading of xml file in Board class.");
			}

			/* Loop for each bonus node */
			for (int j = 0; j < nodeListBonus.getLength(); j++) {
				try {
					elementBonus = (Element) nodeListBonus.item(j);
					bonus = Bonus.bonusByString(elementBonus.getAttribute("Type"),
							Integer.parseInt(elementBonus.getTextContent().split("\n")[0]));
					/* Add the new bonus to business permit tile */
					tileBusinessPermit.getBonus().add(bonus);
				} catch (Exception e) {
					throw new IOException("Error on reading of xml file in Board class.");
				}
			}

			/*
			 * Add the new business permit tile to the hash map that it will be
			 * used when we create the regions
			 */
			if (!decksTileBusinessPermit.containsKey(TypeRegion.parseIn(temp))) {
				try {
					decksTileBusinessPermit.put(TypeRegion.parseIn(temp), new Deck<>(false));
				} catch (Exception e) {
					throw new IOException("Error on reading of xml file in Board class.");
				}
			}
			try {
				decksTileBusinessPermit.get(TypeRegion.parseIn(temp)).add(tileBusinessPermit);
			} catch (Exception e) {
				throw new IOException("Error on reading of xml file in Board class.");
			}
		}

		/* Get Map node and extract region node */
		try {
			elementMap = (Element) document.getElementsByTagName("Map").item(0);
			nodeListRegion = elementMap.getElementsByTagName("Region");
		} catch (Exception e) {
			throw new IOException("Error on reading of xml file in Board class.");
		}

		/* Loop for each region node */
		for (int i = 0; i < nodeListRegion.getLength(); i++) {

			bonus = null;
			citiesInRegion = new LinkedList<City>();

			try {
				/* Extract attribute of region */
				elementRegion = (Element) nodeListRegion.item(i);
				temp = elementRegion.getAttribute("Type");
				typeRegion = TypeRegion.parseIn(temp);

				/* Extract cities of region */
				nodeListCity = elementRegion.getElementsByTagName("City");

				/* Extract bonus of region */
				elementBonusRegion = (Element) elementRegion.getElementsByTagName("Bonus").item(0);
			} catch (Exception e) {
				throw new IOException("Error on reading of xml file in Board class.");
			}

			/* Loop for each city node of region */
			for (int j = 0; j < nodeListCity.getLength(); j++) {
				try {
					/* Extract element city and his attribute */
					elementcity = (Element) nodeListCity.item(j);

					colorCity = ColorCity.valueOf(elementcity.getAttribute("Color"));
					nameCity = elementcity.getTextContent().split("\n")[0];
					temp = elementcity.getAttribute("Position");
					positionCity = new Point(Integer.parseInt(temp.substring(0, temp.indexOf(';'))),
							Integer.parseInt(temp.substring(temp.indexOf(';') + 1)));

					city = new City(nameCity, colorCity, positionCity);
					citiesInRegion.push(city);
					allCities.add(city);

					/* Extract the names of nearby cities */
					nodeListNearbyCity = elementcity.getElementsByTagName("nc");
					nearbyCity.put(city, new ArrayList<String>());

				} catch (Exception e) {
					throw new IOException("Error on reading of xml file in Board class.");
				}

				/*
				 * Add the names of nearby cities to hash map, using as key
				 * city.
				 */
				for (int k = 0; k < nodeListNearbyCity.getLength(); k++) {
					try {
						nearbyCity.get(city).add(nodeListNearbyCity.item(k).getTextContent().split("\n")[0]);
					} catch (Exception e) {
						throw new IOException("Error on reading of xml file in Board class.");
					}
				}

			}

			try {
				/* Construct the bonus depending of type of bonus */
				bonus = Bonus.bonusByString(elementBonusRegion.getAttribute("Type"),
						Integer.parseInt(elementBonusRegion.getTextContent().split("\n")[0]));
			} catch (Exception e) {
				throw new IOException("Error on reading of xml file in Board class.");
			}
			/* Create region bonus tile */
			tileBonusRegion = new TileBonusRegion(bonus, typeRegion);

			/* Create the region */
			region = new Region(typeRegion, citiesInRegion, tileBonusRegion, decksTileBusinessPermit.get(typeRegion));

			/* Assigned region to array of regions */
			regions[i] = region;
		}

		/*
		 * Now I add the nearby cities to city objects by names of nearby city
		 */
		for (City c : nearbyCity.keySet()) {
			for (String s : nearbyCity.get(c)) {
				c.addNearbyCities(getCity(s));
			}
		}

		try {
			/*
			 * Extract list of token and add to all city randomly (except purple
			 * city)
			 */
			nodeListToken = elementMap.getElementsByTagName("TokenReward");
		} catch (Exception e) {
			throw new IOException("Error on reading of xml file in Board class.");
		}

		/* Loop for each token node */
		for (int i = 0; i < nodeListToken.getLength(); i++) {
			/* Get list of bonus of token */
			try {
				nodeListBonus = ((Element) (nodeListToken.item(i))).getElementsByTagName("Bonus");
			} catch (Exception e) {
				throw new IOException("Error on reading of xml file in Board class.");
			}
			tokenReward = new TokenReward(new ArrayList<Bonus>());

			/* Loop for each node of bonus */
			for (int k = 0; k < nodeListBonus.getLength(); k++) {

				bonus = null;

				try {
					/* Extract element bonus */
					elementBonus = (Element) nodeListBonus.item(k);
					/* Construct the bonus depending of type of bonus */
					bonus = Bonus.bonusByString(elementBonus.getAttribute("Type"),
							Integer.parseInt(elementBonus.getTextContent().split("\n")[0]));

				} catch (Exception e) {
					throw new IOException("Error on reading of xml file in Board class.");
				}
				/* Add the bonus to reward token */
				tokenReward.getBonus().add(bonus);

			}

			/* Add reward token to a random city that hasn't it */
			exit = false;
			while (exit == false) {
				x = rnd.nextInt(allCities.size());
				if (allCities.get(x).getTokenReward() == null && allCities.get(x).getColor() != ColorCity.PURPLE) {
					allCities.get(x).setTokenReward(tokenReward);
					exit = true;
				}
			}

		}

		try {
			/* Extract list of color bonus tile */
			nodeListTileBonusColor = elementMap.getElementsByTagName("BonusColor");
		} catch (Exception e) {
			throw new IOException("Error on reading of xml file in Board class.");
		}
		/* Loop for each color bonus tile node */
		for (int i = 0; i < nodeListTileBonusColor.getLength(); i++) {
			try {
				/* Get attribute of node */
				elementBonus = (Element) nodeListTileBonusColor.item(i);
				temp = elementBonus.getAttribute("Color");

				/* Construct the bonus depending of type of bonus */
				bonus = Bonus.bonusByString(elementBonus.getAttribute("Type"),
						Integer.parseInt(elementBonus.getTextContent().split("\n")[0]));
			} catch (Exception e) {
				throw new IOException("Error on reading of xml file in Board class.");
			}

			/* Add the new color bonus tile to list of color bonus tiles */
			tilesBonusColor.add(new TileBonusColor(bonus, ColorCity.parseIn(temp)));
		}

		try {
			/*
			 * Extract KingBoard node, list of cell node and list of king's
			 * reward tile
			 */
			elementKingBoard = (Element) elementMap.getElementsByTagName("KingBoard").item(0);

			nodeListCell = elementKingBoard.getElementsByTagName("CellNobility");
			nodeListTileKingsReward = elementKingBoard.getElementsByTagName("BonusVictory");

		} catch (Exception e) {
			throw new IOException("Error on reading of xml file in Board class.");
		}

		/* Loop for each king's reward tile node */
		for (int i = 0; i < nodeListTileKingsReward.getLength(); i++) {
			try {
				/* Get bonus node */
				elementBonus = (Element) nodeListTileKingsReward.item(i);

				/* Read number of tile */
				temp = elementBonus.getAttribute("Number");

				/* Construct the bonus depending of type of bonus */
				bonus = Bonus.bonusByString(elementBonus.getAttribute("Type"),
						Integer.parseInt(elementBonus.getTextContent().split("\n")[0]));

				/* Add king's reward tile to a temporary list of tile */
				tilesKingsReward.add(new TileKingsReward(bonus, Integer.parseInt(temp)));
			} catch (Exception e) {
				throw new IOException("Error on reading of xml file in Board class.");
			}
		}

		/* Create king board (I need found purple city to create king board) */
		kingBoard = null;
		for (Region r : regions) {
			for (City c : r.getCity()) {
				if (c.getColor() == ColorCity.PURPLE) {
					kingBoard = new KingBoard(c, tilesKingsReward);
					break;
				}
			}
		}

		/* Loop for each nobility cell node */
		for (int i = 0; i < nodeListCell.getLength(); i++) {
			try {
				/* Get nobility cell node and read the attributes */
				elementKingBoard = (Element) nodeListCell.item(i);
				number = Integer.parseInt(elementKingBoard.getAttribute("Number"));

				/* Extract list of bonus of cell node */
				nodeListBonus = elementKingBoard.getElementsByTagName("Bonus");
			} catch (Exception e) {
				throw new IOException("Error on reading of xml file in Board class.");
			}

			/* Loop for each bonus node */
			for (int j = 0; j < nodeListBonus.getLength(); j++) {

				try {
					/* Get bonus node */
					elementBonus = (Element) nodeListBonus.item(j);

					/* Construct the bonus depending of type of bonus */
					bonus = Bonus.bonusByString(elementBonus.getAttribute("Type"),
							Integer.parseInt(elementBonus.getTextContent().split("\n")[0]));
				} catch (Exception exception) {
					throw new IOException("Error on reading of xml file in Board class.");
				}
				/*
				 * I have to add new bonus to specific nobility cell, but i must
				 * check if list of cell is initialize
				 */
				if (!kingBoard.getBonusNobilityTrack().keySet().contains(number)) {
					kingBoard.getBonusNobilityTrack().put(number, new ArrayList<Bonus>());
					kingBoard.getBonusNobilityTrack().get(number).add(bonus);
				} else {
					kingBoard.getBonusNobilityTrack().get(number).add(bonus);
				}
			}
		}

		/* Extract number of assistants and councillors */
		try {
			elementDetails = (Element) document.getElementsByTagName("Details").item(0);

			nodeListCouncillors = elementDetails.getElementsByTagName("Councillor");
			elementAssistants = (Element) elementDetails.getElementsByTagName("Assistants").item(0);

			poolAssistant = Integer.parseInt(elementAssistants.getTextContent().split("\n")[0]);

		} catch (Exception e) {
			throw new IOException("Error on reading of xml file in Board class.");
		}

		/* Loop for each councillor node */
		for (int i = 0; i < nodeListCouncillors.getLength(); i++) {
			try {
				elementCouncillor = (Element) nodeListCouncillors.item(i);
				number = Integer.parseInt(elementCouncillor.getTextContent().split("\n")[0]);

				/* Add number of councillors to list of councillors */
				for (int j = 0; j < number; j++) {
					councillors.add(ColorCouncillor.parseIn(elementCouncillor.getAttribute("Color")));
				}
			} catch (Exception e) {
				throw new IOException("Error on reading of xml file in Board class.");
			}
		}
		//Mix councillor to extract councillor for balconies randomly.
		Collections.shuffle(councillors);
		
		//Extract councillor for balconies
		for (Region r : regions) {
			for (int i = 0; i < 4; i++) {
				if (councillors.isEmpty()){
					throw new IOException("Error on reading of xml file in Board class.");
				}
				r.electCouncillor(councillors.get(0));
				councillors.remove(0);
			}
		}
		for (int i = 0; i < 4; i++) {
			if (councillors.isEmpty()){
				throw new IOException("Error on reading of xml file in Board class.");
			}
			kingBoard.electCouncillor(councillors.get(0));
			councillors.remove(0);
		}

		/* Extract number of politic cards */
		try {
			nodeListCardsPolitic = document.getElementsByTagName("PoliticCard");
		} catch (Exception e) {
			throw new IOException("Error on reading of xml file in Board class.");
		}

		/* Loop for each politic card nodes */
		for (int i = 0; i < nodeListCardsPolitic.getLength(); i++) {

			try {
				elementCardPolitic = (Element) nodeListCardsPolitic.item(i);

				number = Integer.parseInt(elementCardPolitic.getTextContent().split("\n")[0]);
				temp = elementCardPolitic.getAttribute("Color");
			} catch (Exception e) {
				throw new IOException("Error on reading of xml file in Board class.");
			}

			if ("SPECIAL".equalsIgnoreCase(temp)) {
				/* Add number special politic cards */
				for (int j = 0; j < number; j++) {
					deckCardPolitic.add(new CardPolitic(null, true));
				}
			} else {
				/* Add number politic cards */
				for (int j = 0; j < number; j++) {
					try {
						deckCardPolitic.add(new CardPolitic(ColorCouncillor.parseIn(temp), false));
					} catch (Exception e) {
						throw new IOException("Error on reading of xml file in Board class.");
					}
				}
			}
		}
		/* Mix the deck */
		deckCardPolitic.mixMainStack();

	}

	/**
	 * Returns array of regions.
	 * 
	 * @return Array of regions.
	 */
	public Region[] getRegion() {
		return regions;
	}

	/**
	 * Returns the king board.
	 * 
	 * @return The king board.
	 */
	public KingBoard getKingBoard() {
		return kingBoard;
	}

	/**
	 * Returns a specific region using the parameter.
	 * 
	 * @param typeRegion 
	 * 			The region you want to be returned
	 * 
	 * @return The selected region.
	 */
	public Region getRegion(TypeRegion typeRegion) {
		for (Region r : regions) {
			if (r.getTypeOfRegion() == typeRegion)
				return r;
		}
		return null;
	}

	/**
	 * Returns a specific city using the parameter.
	 * 
	 * @param _Name 
	 * 			The city you want to be returned
	 * 
	 * @return The selected city.
	 */
	public City getCity(String _Name) {
		for (Region region : regions) {
			if (region.getCity(_Name) != null) {
				return region.getCity(_Name);
			}
		}
		return null;
	}

	/**
	 * Returns a specific tile bonus color using the parameter.
	 * 
	 * @param _ColorCity 
	 * 			The color of the bonus you want to be returned
	 * 
	 * @return The selected tile.
	 */
	public TileBonusColor getTileBonusColor(ColorCity _ColorCity) {
		for (TileBonusColor tileBonusColor : tilesBonusColor) {
			if (tileBonusColor.getColor() == _ColorCity)
				return tileBonusColor;
		}
		return null;
	}

	
	/**
	 * Returns and uses a specific tile bonus color.
	 * 
	 * @param color 
	 * 			The color of the tile bonus you want to be returned
	 * 
	 * @return The selected tile bonus.
	 */
	public TileBonusColor takeTileBonusColor(ColorCity color) {
		TileBonusColor tileBonusColor;
		for (TileBonusColor tile : tilesBonusColor) {
			if (tile.getColor() == color) {
				tileBonusColor = tile;
				tilesBonusColor.remove(tile);
				return tileBonusColor;
			}
		}
		return null;
	}

	
	/**
	 * Returns the pool of available assistants.
	 * 
	 * @return The pool of available assistants.
	 */
	public int getPoolAssistants() {
		return poolAssistant;
	}

	
	/**
	 * Takes a number of assistants from the available pool.
	 * 
	 * @param _Number
	 * 		The number of assistants you want to take
	 */
	public void TakeAssistants(int _Number) {

		if (poolAssistant - _Number < 0) {
			throw new NegativeException("The number of assistant pool cannot be negative");
		}
		poolAssistant = _Number;
	}

	
	/**
	 * Adds a number of assistants to the available pool.
	 * 
	 * @param _Number
	 * 		The number of assistants you want to add
	 */
	public void AddAssistants(int _Number) {
		poolAssistant += _Number;
	}

	
	/**
	 * Returns an array list containing the available councillors.
	 * 
	 * @return An array list containing the available councillors.
	 */
	public ArrayList<ColorCouncillor> getCouncillors() {
		return councillors;
	}
	

	// Controllata!
	/**
	 * Elect a councillor of the selected color to the selected balcony.
	 * 
	 * @param color
	 *            Color of councillor.
	 * @param balcony
	 *            Name of balcony of region or king board: "KINGBOARD",
	 *            "SEASIDE", "HILLSIDE", "MOUTAIN".
	 * @throws WrongInputException
	 *             If name of balcony isn't valid.
	 * @throws NotFoundException
	 *             If there isn't a councillor of selected color.
	 */
	public void electCouncillor(ColorCouncillor color, String balcony) throws WrongInputException {
		TypeRegion typeRegion;

		if (councillors.indexOf(color) == -1) {
			throw new NotFoundException("There isn't a " + color.toString() + " councillor");
		}

		if ("KINGBOARD".equalsIgnoreCase(balcony)) {
			councillors.add(kingBoard.electCouncillor(color));
		} else {
			typeRegion = TypeRegion.parseIn(balcony);
			if (typeRegion == null) {
				throw new WrongInputException("Name of balcony isn't valid");
			}

			councillors.add(getRegion(typeRegion).electCouncillor(color));
		}

		councillors.remove(color);
	}
	

	// Controllata!
	/**
	 * Pay an assistant for elect a councillor.
	 * 
	 * @param color
	 *            Color of councillor.
	 * @param balcony
	 *            Name of balcony. "SEASIDE", "HILLSIDE", "MOUNTAIN",
	 *            "KINGBOARD".
	 * @throws WrongInputException
	 *             If name of balcony is wrong.
	 * @throws NotFoundException
	 *             If there isn't a councillor available of selected color.
	 */
	public void assistantForCouncillor(ColorCouncillor color, String balcony) throws WrongInputException {
		TypeRegion typeRegion;

		if (councillors.indexOf(color) == -1) {
			throw new NotFoundException("There isn't a " + color.toString() + " councillor");
		}

		if ("KINGBOARD".equalsIgnoreCase(balcony)) {
			councillors.add(kingBoard.electCouncillor(color));
		} else {
			typeRegion = TypeRegion.parseIn(balcony);
			if (typeRegion == null) {
				throw new WrongInputException("Name of balcony isn't valid");
			}

			councillors.add(getRegion(typeRegion).electCouncillor(color));
		}

		councillors.remove(color);
	}

	
	/**
	 * Adds a councillor to the available pool.
	 * 
	 * @param _Color
	 * 		The color of the councillor you want to add
	 */
	public void AddCouncillor(ColorCouncillor _Color) {
		councillors.add(_Color);
	}

	
	/**
	 * Returns the deck of politic cards.
	 * 
	 * @return The deck of politic cards.
	 */
	public Deck<CardPolitic> getDeckCardPolitic() {
		return deckCardPolitic;
	}

	
	/**
	 * Draws a politic card from the deck of politic cards.
	 * 
	 * @throws InfinityLoopException
	 *             If deck is empty.
	 *             
	 * @return The drawn card.
	 */
	public CardPolitic DrawCardPolitic() throws InfinityLoopException {
		return deckCardPolitic.draw();
	}

	
	/**
	 * Adds a card to the refuse stack of politic cards.
	 * 
	 * @param _CardPolitic
	 * 		The card politi you want to add to the refuse stack
	 */
	public void AddToRafuseCardPolitic(CardPolitic _CardPolitic) {
		deckCardPolitic.addRefuse(_CardPolitic);
	}

	

	/*public static void main(String[] args) throws InfinityLoopException {

		Board board = null;
		try {
			board = new Board("Configuration.xml");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return;
		}

		for (Region region : board.regions) {
			System.out.println(region.getTypeOfRegion().toString());
			for (City city : region.getCity()) {
				System.out.println("-->" + city.getName());
				for (City nearbyCity : city.getNearbyCities()) {
					System.out.println("   -->" + nearbyCity.getName());

				}
				if (city.getTokenReward() != null) {
					for (Bonus bonus : city.getTokenReward().getBonus()) {
						if (bonus != null)
							System.out.println("   -->" + bonus.toString());
					}
				}
			}
		}

		System.out.println("\nTile: ");
		for (TileBonusColor tileBonusColor : board.tilesBonusColor) {
			System.out.println(tileBonusColor.getColor().toString() + "-->" + tileBonusColor.getBonus().toString());
		}

		System.out.println("\nCard politic:");
		for (int i = 0; i < 10; i++) {
			System.out.println(board.getDeckCardPolitic().draw().getColor());
		}

		System.out.println("\nCouncillor:");
		for (ColorCouncillor color : board.getCouncillors()) {
			System.out.println(color.toString());
		}
		System.out.println("OK");

	}*/
}