package model;

import java.awt.Color;

/**
 * Enumerator of color of city.<br>
 * BRONZE<br>
 * GOLD<br>
 * IRON<br>
 * PURPLE<br>
 * SILVER<br>
 * 
 * @author Federico
 *
 */
public enum ColorCity {
	SILVER("SILVER", new Color(192, 192, 192)), GOLD("GOLD", new Color(255, 215, 0)), IRON("IRON",
			new Color(120, 120, 150)), BRONZE("BRONZE",
					new Color(205, 127, 50)), PURPLE("PURPLE", new Color(138, 43, 226));

	private String name;
	private Color color;

	private ColorCity(String name, Color color) {
		this.name = name;
		this.color = color;
	}

	/**
	 * Returns name of color of city.
	 * 
	 * @return Name of color.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns color in RGB of color of city.<br>
	 * (205,127,50) BRONZE<br>
	 * (255,115,0) GOLD<br>
	 * (120,120,150) IRON<br>
	 * (138,43,226) PURPLE<br>
	 * (192,192,192) SILVER<br>
	 * 
	 * @return Color in RGB.
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Return color of city by its name.
	 * 
	 * @param input
	 *            Name of color: "BRONZE", "GOLD", "IRON", "PURPLE", "SILVER".
	 * @return Color of city.
	 */
	public static ColorCity parseIn(String input) {
		for (ColorCity colorCity : ColorCity.values()) {
			if (colorCity.getName().equalsIgnoreCase(input)) {
				return colorCity;
			}
		}

		return null;
	}

}
