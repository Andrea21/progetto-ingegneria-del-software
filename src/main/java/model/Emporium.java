package model;


import java.awt.Color;
import java.io.Serializable;

/**
 * Emporium class it generates and manages the objects emporium
 * @author Federico
 *
 */
public class Emporium implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Color color;
	private boolean isOccupied;
	
	/**
	 * Class constructor. Generates the emporium
	 * 
	 * @param color
	 * 			The color of the emporium
	 */
	public Emporium(Color color) {
		
		this.color = color;
		this.isOccupied = false;
			
	}

	/**
	 * Returns the color of the emporium.
	 * 
	 * @return The color of the emporium.
	 */
	public Color getColor(){
		return color;
	}
	
	/**
	 * Checks if the emporium is occupied
	 * 
	 * @return True if the emporium is occupied, false otherwise
	 */
	public boolean isOccupied(){
		return isOccupied;
	}
	
	/**
	 * Sets the occupation of the emporium
	 */
	public void setOccupaid(){
		this.isOccupied = true;
	}

}
