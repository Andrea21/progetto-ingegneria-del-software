package model;

import java.awt.Color;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import bonusandcards.*;
import controller.Controller.TypeRound;
import utility.FileUtility;

/**
 * Class of model that contains all resources of game.
 * 
 * @author Federico
 *
 */
public class Model extends view.Observable implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Board board;
	private List<Player> players;
	private Player currentPlayer;
	private TypeRound typeRound;
	private boolean lastRound;
	private Map<Integer, Player> ranking;

	/**
	 * Class constructor. Creates players and reads configurations file to
	 * creates boar, regions, king's board and cities and for all bonus, cards
	 * and tiles.
	 * 
	 * @param infoPlayer
	 *            Map with keys the names of players and values the colors of
	 *            players.
	 * @param path
	 *            Path of xml configuration file.
	 * @throws Exception
	 *             If there are some problems on reading of file.
	 */
	public Model(Map<String, Color> infoPlayer, String path) throws Exception {
		Document document;
		int numberEmporium;
		players = new ArrayList<>();
		

		try {
			board = new Board(path);
		} catch (IOException exception) {
			throw new Exception("Configuration file not found or not valid.");
		} catch (Exception exception) {
			throw new Exception("An unexpected error on opening file.");
		}

		try {
			document = FileUtility.openXML(path);
		} catch (Exception exception) {
			throw new WrongInputException("Error on creation of game");
		}

		numberEmporium = Integer
				.parseInt(((Element) (document.getElementsByTagName("Game").item(0))).getAttribute("NumberEmporiums"));

		if (infoPlayer == null) {
			throw new NullPointerException("The hashmap of players cannot be null.");
		}

		for (String player : infoPlayer.keySet()) {
			players.add(new Player(player, infoPlayer.get(player), board.getDeckCardPolitic(),
					board.getKingBoard().getBonusNobilityTrack(),numberEmporium));
		}

		Collections.shuffle(players);

		int i = 0;
		players.get(0).drawCardPolitic();
		for (Player p : players) {
			p.addCoins(10 + i);
			i++;
		}

		ranking = null;
		lastRound = false;
		typeRound = TypeRound.NORMAL;
		currentPlayer = players.get(0); // Provisional. When Controller will be
										// initialized, it'll set current
										// player.
	}

	/**
	 * Sets if it's last round.
	 * 
	 * @param isLastround
	 *            True is last round, else False.
	 */
	public void isLastRound(boolean isLastround) {
		lastRound = isLastround;
	}

	/**
	 * Checks if it's last round.
	 * 
	 * @return True is last round, else false.
	 */
	public boolean isLastRound() {
		return lastRound;
	}

	/**
	 * Returns board.
	 * 
	 * @return Board object.
	 */
	public Board getBoard() {
		return board;
	}

	/**
	 * Returns list of players.
	 * 
	 * @return List of players.
	 */
	public List<Player> getPlayers() {
		return players;
	}

	/**
	 * Returns the current player.
	 * 
	 * @return Current player.
	 */
	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	// Testata!
	/**
	 * Returns a player by his name.
	 * 
	 * @param nameOfThePlayer
	 *            Name of player.
	 * @return Player object. Returns null if there isn't a player with that
	 *         name.
	 */
	public Player getPlayerByName(String nameOfThePlayer) {
		for (Player p : players) {
			if (p.getName().equals(nameOfThePlayer)) {
				return p;
			}
		}
		return null;
	}

	/**
	 * Sets type of round.
	 * 
	 * @param type
	 *            Type of round.
	 */
	public void setTypeRound(TypeRound type) {
		typeRound = type;
	}

	/**
	 * Gets type of round.
	 * 
	 * @return Type of round.
	 */
	public TypeRound getTypeRound() {
		return typeRound;
	}

	/**
	 * Returns a map of ranking of game. The keys are positions and values are
	 * players.
	 * 
	 * @return Map of positions and players.
	 */
	public Map<Integer, Player> getRanking() {
		return ranking;
	}

	/**
	 * Sets map of ranking of game. The keys are positions and values are
	 * players.
	 * 
	 * @param finalRanking
	 *            Map of positions and players.
	 */
	public void setRanking(Map<Integer, Player> finalRanking) {
		ranking = finalRanking;
	}

	/**
	 * Notifies to observers an error message.
	 * 
	 * @param message
	 *            Text of message.
	 */
	public void error(String message) {
		notifyObservers(message);
	}

	/**
	 * Notify to observers this model object.
	 */
	public void notifyView() {
		notifyObservers(this);
	}

	// Testata!
	/**
	 * Returns name of a color RGB. Returns only static colors of class
	 * java.awt.Color.
	 * 
	 * @param c
	 *            Color object.
	 * @return Returns name of color. Return unknown if color isn't a static
	 *         color of class java.awt.Color.
	 */
	public static String getColorName(Color c) {
		for (Field f : Color.class.getFields()) {
			try {
				if (f.getType() == Color.class && f.get(null).equals(c)) {
					return f.getName();
				}
			} catch (java.lang.IllegalAccessException e) {
				return "unknown";
			}
		}
		return "unknown";
	}

	// Testata!
	/**
	 * Sets the current player by his index on list of players.
	 * 
	 * @param index
	 *            Index on list of players.
	 */
	public void setCurrentPlayer(Integer index) {
		if (index == null) {
			throw new NullPointerException("The index of player cannot be null");
		}
		if (index < 0) {
			throw new NegativeException("The index of player cannot be negative");
		}
		if (index >= players.size()) {
			throw new IndexOutOfBoundsException("The index of player isn't valid");
		}
		currentPlayer = players.get(index);
	}

	// Testata!
	/**
	 * Sets specified player as current player.
	 * 
	 * @param player
	 *            Player that you want as current player.
	 * @throws NullPointerException
	 *             If player specified isn't in the list of player.
	 */
	public void setCurrentPlayer(Player player) {
		if (!players.contains(player)) {
			throw new NullPointerException("The chosen player isn't in the list of player");
		}
		currentPlayer = player;
	}

	/**
	 * Sets a player as inactive.
	 * 
	 * @param namePlayer
	 *            Name of player.
	 */
	public void removePlayer(String namePlayer) {
		getPlayerByName(namePlayer).setActive(false);
	}

	// Testata!
	/**
	 * Adds assistants, politic cards and business permit tile owned the current
	 * player for sale. Method do NOT check if current player actually has the
	 * objects.
	 * 
	 * @param numberAssistants
	 *            Number of assistants for sale.
	 * @param priceAssistants
	 *            Price of each assistant.
	 * @param cards
	 *            List of politic cards and their prices
	 * @param tiles
	 *            List of business permit tiles and their prices.
	 */
	public void addObjectsForSale(int numberAssistants, int priceAssistants, Map<CardPolitic, List<Integer>> cards,
			HashMap<TileBusinessPermit, List<Integer>> tiles) {
		if (numberAssistants > 0) {
			currentPlayer.addAssistantsForSale(numberAssistants, priceAssistants);
		}

		for (CardPolitic card : cards.keySet()) {
			for (Integer price : cards.get(card)) {
				currentPlayer.addCardPoliticForSale(card, price);
			}
		}

		for (TileBusinessPermit tile : tiles.keySet()) {
			for (Integer price : tiles.get(tile)) {
				currentPlayer.addTileBusinessPermitForSale(tile, price);
			}
		}
	}

	// Testata!
	/**
	 * Buys objects for current player. It does NOT check if the players
	 * specified in the hash map already have the objects on sale. It does NOT
	 * check if current player has enough coins.
	 * 
	 * @param sellers
	 *            Hash map with keys the players and values list of objects that
	 *            current player wants buy from them.
	 */
	public void buyObjects(Map<Player, List<ObjectForSale>> sellers) {
		for (Player player : sellers.keySet()) {
			for (ObjectForSale obj : sellers.get(player)) {
				currentPlayer.addCoins(-player.buyObject(obj));
				if (obj.isAssistants()) {
					currentPlayer.addAssistants(obj.getAssistants());
				} else if (obj.isCardPolitc()) {
					currentPlayer.getcardsPolitic().add(obj.getCardPolitic());
				} else if (obj.isTileBusinessPermit()) {
					currentPlayer.getTileBusinessPermit().add(obj.getTileBusinessPermit());
				}
			}
		}
	}

	/**
	 * Delete all objects for sale for each player.
	 */
	public void clearObjectForSale() {
		for (Player player : players) {
			player.emptiesObjectForSale();
		}
	}

	// Fatto!
	/**
	 * Elects a councillors in the balcony specified. The last councillor of
	 * balcony is lifted from array of councillors that represents the balcony.
	 * The elements of array slide on the right and the first element become the
	 * councillor that you want elect.
	 * 
	 * @param color
	 *            Color of councillor that you want elect.
	 * @param balcony
	 *            Name of balcony: "SEASIDE", "HILLSIDE", "MOUNTAIN",
	 *            "KINGBOARD".
	 * @throws WrongInputException
	 *             If name of balcony isn't valid.
	 * @throws NotFoundException
	 *             If there isn't an available councillor of color specified.
	 */
	public void electCouncillor(ColorCouncillor color, String balcony) throws WrongInputException, NotFoundException {
		try {
			board.electCouncillor(color, balcony);
		} catch (WrongInputException exception) {
			throw exception;
		} catch (NotFoundException exception) {
			throw exception;
		}

		currentPlayer.electCouncillor();
	}

	// Fatto!
	/**
	 * Acquires a business permit tile.
	 * 
	 * @param chosenTile
	 *            String representation of permit tile.
	 * @param typeRegion
	 *            Type of region of region from which current player wants take
	 *            permit tile.
	 * @param cards
	 *            List of cards that current player wants use to acquire tile.
	 * @param numberCoins
	 *            Number of coins that current player must pay.
	 * @throws WrongInputException
	 *             If type of string representation of tile isn't valid.
	 * @throws NullPointerException
	 *             If tile that currents player wants isn't available or list of
	 *             cards is null.
	 * @throws NegativeException
	 *             If current player hasn't enough coins.
	 */
	public void aquireTileBusinessPermit(String chosenTile, TypeRegion typeRegion, List<CardPolitic> cards,
			int numberCoins) throws WrongInputException, NullPointerException, NegativeException {
		TileBusinessPermit tileBusinessPermit;

		try {
			tileBusinessPermit = board.getRegion(typeRegion).acquireTileBusinessPermit(chosenTile);
		} catch (WrongInputException exception) {
			throw new WrongInputException("Unexpected error in AquireTileBusinessPermit");
		}

		try {
			currentPlayer.aquireTileBusinessPermit(tileBusinessPermit, cards, numberCoins);
		} catch (NullPointerException exception) {
			throw exception;
		} catch (NegativeException exception) {
			throw exception;
		} catch (NotFoundException exception) {
			throw exception;
		}
	}

	// Fatto!
	/**
	 * Builds an emporium with business permit tile.
	 * 
	 * @param tileBusinessPermit
	 *            String representation of permit tile.
	 * @param city
	 *            City where current player wants build.
	 */
	public void buildEmporiumPermitTile(TileBusinessPermit tileBusinessPermit, City city) {
		currentPlayer.buildEmporiumPermitTile(tileBusinessPermit, city.getNumberOfBuildedEmporium());
		board.AddAssistants(city.getNumberOfBuildedEmporium());

		city.buildEmporium(currentPlayer);

		city.getTokenReward().doBonus(currentPlayer);
		for (City c : city.getConnectedCity(currentPlayer)) {
			c.getTokenReward().doBonus(currentPlayer);
		}

		checkAndTakeBonusBuild();
	}

	// Fatto!
	/**
	 * Builds an emporium with the help of king.
	 * 
	 * @param city
	 *            City where current player wants build.
	 * @param cards
	 *            List of cards that current player wants use to satisfy king's
	 *            board balcony.
	 * @param numberCoins
	 *            Number of coins that current player must pay.
	 */
	public void buildEmporiumKing(City city, List<CardPolitic> cards, Integer numberCoins) {

		currentPlayer.buildEmporiumKing(numberCoins, city.getNumberOfBuildedEmporium(), cards);
		board.AddAssistants(city.getNumberOfBuildedEmporium());
		board.getKingBoard().moveKing(city);

		city.buildEmporium(currentPlayer);

		if (!(city.getTokenReward() == null)) {
			city.getTokenReward().doBonus(currentPlayer);
		}
		for (City c : city.getConnectedCity(currentPlayer)) {
			c.getTokenReward().doBonus(currentPlayer);
		}

		checkAndTakeBonusBuild();
	}

	// Fatto!
	/**
	 * Engages an assistant.
	 */
	public void engageAssistant() {
		currentPlayer.engageAssistant();
		board.TakeAssistants(1);
	}

	// Fatto!
	/**
	 * Changes business permit tile of a region.
	 * 
	 * @param typeRegion
	 *            Type of regions of which change permit tile.
	 * @throws NullPointerException
	 *             If there is less 2 tile on deck of permit tile of region.
	 */
	public void changeBusinessPermitTile(TypeRegion typeRegion) throws NullPointerException {

		try {
			board.getRegion(typeRegion).changeBusinessPermitTile();
		} catch (NullPointerException exception) {
			throw new NullPointerException("The deck of business permit tile is empty");
		}

		currentPlayer.changeBusinessPermitTile();
		board.AddAssistants(1);
	}

	// Fatto!
	/**
	 * Sends an assistant to elect a councillor.
	 * 
	 * @param balcony
	 *            Name of balcony where elect the new councillor. "SEASIDE",
	 *            "HILLSIDE", "MOUNTAIN", "KINGBOARD".
	 * @param color
	 *            Color of councillor.
	 * @throws WrongInputException
	 *             If name of balcony isn't valid.
	 */
	public void assistantForCouncillor(String balcony, ColorCouncillor color) throws WrongInputException {
		try {
			board.assistantForCouncillor(color, balcony);
		} catch (WrongInputException exception) {
			throw new WrongInputException("Invalid input format");
		}
		currentPlayer.addAssistants(-1);
	}

	// Fatto!
	/**
	 * Takes bonus for current player from a list of bonus.
	 * 
	 * @param listBonus
	 *            List of bonus.
	 */
	public void takeBonusTokenReward(List<Bonus> listBonus) {
		for (Bonus bonus : listBonus) {
			bonus.takeBonus(currentPlayer);
		}
	}

	// Fatto!
	/**
	 * Adds a business permit tile to current player.
	 * 
	 * @param tileBusinessPermit
	 *            String representation of permit tile.
	 */
	public void takeBonusTakePermitTile(TileBusinessPermit tileBusinessPermit) {
		currentPlayer.getTileBusinessPermit().add(tileBusinessPermit);
	}

	// Fatto!
	/**
	 * Takes bonus for current player from a business permit tile used of
	 * current player.
	 * 
	 * @param tileBusinessPermit
	 *            Representation of permit tile.
	 */
	public void takeBonusTakePreviousBonus(TileBusinessPermit tileBusinessPermit) {
		tileBusinessPermit.doBonus(currentPlayer);
	}

	// Controllata!
	/**
	 * Check if current player can take a color bonus or a region bonus,
	 * eventually a king's bonus
	 */
	private void checkAndTakeBonusBuild() {

		TileBonusRegion tileBonusRegion;
		TileBonusColor tileBonusColor;
		boolean check;

		tileBonusRegion = null;
		tileBonusColor = null;

		for (Region region : board.getRegion()) {

			check = true;

			for (City city : region.getCity()) {
				if (!city.hasEmporium(currentPlayer)) {
					check = false;
					break;
				}
			}

			/*
			 * Non è possibile che check sia True e sia presente il bonus se il
			 * bonus non è quello che devo prendere! Se non fosse la regione
			 * della città che ho costruito e se avessi tutte le città nella
			 * regione, non ci sarebbe più il bonus.
			 */
			if (check && region.isPresentTileBonusRegion()) {
				tileBonusRegion = region.takeTileBonus();
			}
		}

		for (ColorCity color : ColorCity.values()) {
			check = true;
			for (Region region : board.getRegion()) {
				for (City city : region.getCity()) {
					if (city.getColor() == color && !city.hasEmporium(currentPlayer)) {
						check = false;
						break;
					}
				}
				if (!check) {
					break;
				}
			}

			if (check && board.getTileBonusColor(color) != null) {
				tileBonusColor = board.takeTileBonusColor(color);
			}
		}

		if (tileBonusColor != null) {
			currentPlayer.addTileBonus(tileBonusColor);
			currentPlayer.addTileBonus(board.getKingBoard().drawTileKingReward());
		}

		if (tileBonusRegion != null) {
			currentPlayer.addTileBonus(tileBonusRegion);
			currentPlayer.addTileBonus(board.getKingBoard().drawTileKingReward());
		}

	}

}