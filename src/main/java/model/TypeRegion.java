package model;

/**
 * Enumerator of type of region.<br>
 * SEASIDE<br>
 * HILLSIDE<br>
 * MOUTAIN
 * @author Federico
 *
 */
public enum TypeRegion {
	SEASIDE ("SEASIDE"), HILLSIDE("HILLSIDE"), MOUNTAIN("MOUNTAIN");
	
	private String name;
	
	private TypeRegion(String name){
		this.name = name;
	}
	
	/**
	 * Returns a type of region by its name.
	 * @param input	Name of type of region: "SEASIDE", "HILLSIDE", "MOUNTAIN".
	 * @return	Type of region.
	 */
	public static TypeRegion parseIn(String input) {
		for (TypeRegion typeRegion: TypeRegion.values()){
			if (typeRegion.name.equalsIgnoreCase(input)){
				return typeRegion;
			}
		}
		return null;
	}
}
