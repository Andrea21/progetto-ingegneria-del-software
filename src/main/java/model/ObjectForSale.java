package model;

import java.io.Serializable;

import bonusandcards.CardPolitic;
import bonusandcards.TileBusinessPermit;

/**
 * Class that represents an object on sale. It can be assistants, politic card
 * or a business permit tile.
 * 
 * @author Federico
 *
 */
public class ObjectForSale implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TileBusinessPermit tileBusinessPermit;
	private CardPolitic cardPolitic;
	private Integer assistants;
	private Integer price;

	/**
	 * Constructor of class. Object for sale is a number of assistants.
	 * 
	 * @param assistants
	 *            Number of assistants.
	 * @param price
	 *            Price for each assistants.
	 */
	public ObjectForSale(Integer assistants, Integer price) {
		if (assistants < 0) {
			throw new NegativeException("The number of assistants on sale, cannot be negative");
		}
		if (price < 0) {
			throw new NegativeException("The price of an object cannot be negative.");
		}

		this.assistants = assistants;
		this.price = price;
		cardPolitic = null;
		tileBusinessPermit = null;
	}

	/**
	 * Constructor of class. Object for sale is a politic card.
	 * 
	 * @param cardPolitic
	 *            Politic card.
	 * @param price
	 *            Price of politic card.
	 */
	public ObjectForSale(CardPolitic cardPolitic, Integer price) {
		if (cardPolitic == null) {
			throw new NullPointerException("The politic card on sale, cannot be null.");
		}
		if (price < 0) {
			throw new NegativeException("The price of an object cannot be negative.");
		}

		this.cardPolitic = cardPolitic;
		this.price = price;
		assistants = null;
		tileBusinessPermit = null;
	}

	/**
	 * Constructor of class. Object for sale is a business permit tile.
	 * 
	 * @param tileBusinessPermit
	 *            Business permit tile.
	 * @param price
	 *            Price of permit tile.
	 */
	public ObjectForSale(TileBusinessPermit tileBusinessPermit, Integer price) {
		if (tileBusinessPermit == null) {
			throw new NullPointerException("The business permit tile on sale, cannot be null.");
		}
		if (price < 0) {
			throw new NegativeException("The price of an object cannot be negative.");
		}

		this.tileBusinessPermit = tileBusinessPermit;
		this.price = price;
		assistants = null;
		cardPolitic = null;
	}

	/**
	 * Checks if object is a set of assistants.
	 * 
	 * @return True if it's a sets of assistants, else False.
	 */
	public boolean isAssistants() {
		return assistants != null;
	}

	/**
	 * Checks if object is a politic card.
	 * 
	 * @return True if it's a politic card, else False.
	 */
	public boolean isCardPolitc() {
		return cardPolitic != null;
	}

	/**
	 * Checks if object is a business permit tile.
	 * 
	 * @return True if it's a permit tile, else False.
	 */
	public boolean isTileBusinessPermit() {
		return tileBusinessPermit != null;
	}

	/**
	 * Returns number of assistants on sale.
	 * 
	 * @return Number of assistants. Returns null if object isn't a set of
	 *         assistants.
	 */
	public Integer getAssistants() {
		return assistants;
	}

	/**
	 * Returns politic card on sale.
	 * 
	 * @return Politic card. Returns null if object isn't a politic card.
	 */
	public CardPolitic getCardPolitic() {
		return cardPolitic;
	}

	/**
	 * Returns business permit tile.
	 * 
	 * @return Permit tile. Returns null if object isn't a permit tile.
	 */
	public TileBusinessPermit getTileBusinessPermit() {
		return tileBusinessPermit;
	}

	/**
	 * Returns price of object.
	 * 
	 * @return Price of object.
	 */
	public Integer getPrice() {
		return price;
	}

	/**
	 * Checks if this object is equal to object given. It doesn't check prices
	 * of them.<br>
	 * If objects are sets of assistants, it checks that is the same number of
	 * assistants.<br>
	 * If objects are politic cards, it checks that politic cards are equal.<br>
	 * If objects are permit tile, it checks that permit tiles are equal.
	 * 
	 * @param obj
	 *            Object to check.
	 * @return True if objects are of the same type. Else False.
	 */
	public boolean equals(ObjectForSale obj) {
		if (obj == null) {
			return false;
		}
		
		if (obj.isAssistants() && this.isAssistants()) {
			if (obj.getAssistants() == this.getAssistants())
				return true;
		} else if (obj.isCardPolitc() && this.isCardPolitc()) {
			if (obj.getCardPolitic().equals(this.getCardPolitic()))
				return true;
		} else if (obj.isTileBusinessPermit() && this.isTileBusinessPermit()) {
			if (obj.getTileBusinessPermit().equals(this.getTileBusinessPermit()))
				return true;
		}

		return false;
	}
	
	@Override
	public String toString() {
		if (isAssistants()) {
			return "ObjectForsale assistants " + assistants.toString() + " price " + price.toString();
		} else if (isCardPolitc()) {
			return "ObjectForsale [" + cardPolitic.toString() + "] price " + price.toString();
		} else if (isTileBusinessPermit()) {
			return "ObjectForsale [" + tileBusinessPermit.toString() + "] price " + price.toString();
		}

		return "ObjectForSale";
	}
}
