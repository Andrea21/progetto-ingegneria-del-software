package model;

public class NegativeException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public NegativeException()
	{ 
		super(); 
	} 
	public NegativeException(String s)
	{ 
		super(s);
	} 
}
