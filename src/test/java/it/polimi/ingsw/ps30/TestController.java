package it.polimi.ingsw.ps30;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;

import bonusandcards.Bonus;
import bonusandcards.BonusCoins;
import bonusandcards.CardPolitic;
import bonusandcards.TileBusinessPermit;
import controller.Controller;
import controller.Controller.TypeRound;
import junit.framework.TestCase;
import model.ColorCouncillor;
import model.Model;
import model.Player;
import model.TypeRegion;

public class TestController extends TestCase {

	private Controller controller;
	private Model model;

	public TestController(String name) {
		super(name);
	}

	@Before
	protected void setUp() {

		HashMap<String, Color> infoPlayers;

		infoPlayers = new HashMap<>();

		infoPlayers.put("Federico", Color.red);
		infoPlayers.put("Alessandro", Color.blue);
		infoPlayers.put("Andrea", Color.green);

		try {
			model = new Model(infoPlayers, "Configuration/Configurazione 1/Configuration.xml");
		} catch (IOException exception) {
			TestCase.fail("Errore nell'apertura del file di configurazione: " + exception.getMessage());
			return;
		} catch (Exception exception) {
			fail("Errore nella creazione del model: " + exception.getMessage());
			return;
		}

		controller = new Controller(model);
		resetCardPolitic();

	}

	private void resetCardPolitic() {
		for (Player player : model.getPlayers()) {
			player.getcardsPolitic().clear();
		}
	}

	/**
	 * Add a card politic to specific player.
	 * 
	 * @param namePlayer
	 *            Name of player.
	 * @param card
	 *            "FUCHSIA", "BLACK", "PURPLE", "BLUE", "ORANGE", "WHITE",
	 *            "SPECIAL".
	 */
	private void addCardPolitic(String namePlayer, String card) {
		if ("SPECIAL".equalsIgnoreCase(card)) {
			model.getPlayerByName(namePlayer).addCardPolitic(new CardPolitic(null, true));
		} else {
			model.getPlayerByName(namePlayer).addCardPolitic(new CardPolitic(ColorCouncillor.parseIn(card), false));
		}
	}

	/**
	 * Add a business permit tile to specific player. Tile hasn't bonus and
	 * city, it has only type of region
	 * 
	 * @param namePlayer
	 *            Name of player.
	 * @param typeRegion
	 *            "SEASIDE","HILLSIDE","MOUNTAIN".
	 */
	private void addBusinessPermitTile(String namePlayer, String typeRegion) {
		model.getPlayerByName(namePlayer).addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn(typeRegion)));
	}

	/**
	 * Set current player by his name.
	 * 
	 * @param name
	 *            Name of player
	 */
	private void setCurrentPlayer(String name) {
		model.setCurrentPlayer(model.getPlayerByName(name));

	}

	/**
	 * Remove all object for sale of all players.
	 */
	private void resetObjectForSale() {
		for (Player player : model.getPlayers()) {
			player.getObjectForSale().clear();
		}
	}

	/**
	 * Add assistants to a specific player.
	 * 
	 * @param namePlayer
	 *            Name of player.
	 * @param numberAssistants
	 *            Number of assistants to add to the player.
	 */
	private void addAssistants(String namePlayer, int numberAssistants) {
		model.getPlayerByName(namePlayer).addAssistants(numberAssistants);
	}

	/**
	 * Sets the color of a specific balcony in a region
	 * 
	 * @param color
	 *            The color in which you want the balcony.
	 * @param region
	 *            The region where you want to change the balcony.
	 */
	private void setBalconyRegion(ColorCouncillor color, TypeRegion region) {
		for (int i = 0; i < 4; i++) {
			model.getBoard().getRegion(region).electCouncillor(color);
		}
	}

	/**
	 * Sets the color of the balcony in the king's board
	 * 
	 * @param color
	 *            The color in which you want the balcony.
	 */
	private void setBalconyKingsBoard(ColorCouncillor color) {
		for (int i = 0; i < 4; i++) {
			model.getBoard().getKingBoard().electCouncillor(color);
		}
	}

	/**
	 * Add coins to a specific player.
	 * 
	 * @param namePlayer
	 *            Name of player.
	 * @param numberOfCoins
	 *            Number of coins to add to the player.
	 */
	private void addCoins(String namePlayer, int numberOfCoins) {
		model.getPlayerByName(namePlayer).addCoins(numberOfCoins);
	}

	/**
	 * Add some object to the object on sale list
	 * 
	 * @param playerThatOwnsTheObjects
	 *            Name of player that owns the object on sale.
	 */
	private void addObjectForSale(String playerThatOwnsTheObjects) {
		TileBusinessPermit tile = null;
		TileBusinessPermit tile2 = null;
		CardPolitic card = new CardPolitic(ColorCouncillor.BLACK, false);
		ArrayList<String> city = new ArrayList<>();
		ArrayList<String> city2 = new ArrayList<>();
		ArrayList<Bonus> bonus = new ArrayList<>();
		city.add("C");
		city2.add("D");
		bonus.add(new BonusCoins(10));
		tile = new TileBusinessPermit(TypeRegion.SEASIDE, city, bonus);
		tile2 = new TileBusinessPermit(TypeRegion.SEASIDE, city2, bonus);
		model.getPlayerByName(playerThatOwnsTheObjects).addAssistants(100);
		model.getPlayerByName(playerThatOwnsTheObjects).addCardPolitic(card);
		model.getPlayerByName(playerThatOwnsTheObjects).addBusinessPermit(tile);
		model.getPlayerByName(playerThatOwnsTheObjects).addBusinessPermit(tile2);
		model.getPlayerByName(playerThatOwnsTheObjects).addAssistantsForSale(6, 3);
		model.getPlayerByName(playerThatOwnsTheObjects).addCardPoliticForSale(card, 10);
		model.getPlayerByName(playerThatOwnsTheObjects).addTileBusinessPermitForSale(tile, 7);
		model.getPlayerByName(playerThatOwnsTheObjects).addTileBusinessPermitForSale(tile2, 7);
	}

	public void testRoundMarket1() {

		String output;

		controller.setTypeRound(TypeRound.MARKET1);
		setCurrentPlayer("Federico");

		addCardPolitic("Federico", "BLACK");
		addCardPolitic("Federico", "PURPLE");
		addCardPolitic("Federico", "SPECIAL");
		addCardPolitic("Federico", "WHITE");
		addCardPolitic("Federico", "WHITE");
		addCardPolitic("Federico", "PURPLE");
		addCardPolitic("Federico", "ORANGE");

		addBusinessPermitTile("Federico", "SEASIDE");
		addBusinessPermitTile("Federico", "MOUNTAIN");

		addAssistants("Federico", 10);

		output = controller.roundMarket1("--");
		assertEquals("OK", output);
		resetObjectForSale();
		setCurrentPlayer("Federico");

		output = controller.roundMarket1("5:10--");
		assertEquals("OK", output);
		resetObjectForSale();
		setCurrentPlayer("Federico");

		output = controller.roundMarket1("5:10-BLACK:4:PURPLE:5-");
		assertEquals("OK", output);
		resetObjectForSale();
		setCurrentPlayer("Federico");

		output = controller.roundMarket1("-SPECIAL:9:BLACK:5-");
		assertEquals("OK", output);
		resetObjectForSale();
		setCurrentPlayer("Federico");

		output = controller.roundMarket1("-SPECIAL:9:BLACK:5-SEASIDE|:5");
		assertEquals("OK", output);
		resetObjectForSale();
		setCurrentPlayer("Federico");

		output = controller.roundMarket1("5:5--SEASIDE|:5");
		assertEquals("OK", output);
		resetObjectForSale();
		setCurrentPlayer("Federico");

		output = controller.roundMarket1("--MOUNTAIN|:5");
		assertEquals("OK", output);
		resetObjectForSale();
		setCurrentPlayer("Federico");

		output = controller.roundMarket1(
				"10:1-BLACK:2:PURPLE:2:SPECIAL:2:WHITE:2:WHITE:2:PURPLE:2:ORANGE:3-MOUNTAIN|:5:SEASIDE|:4");
		assertEquals("OK", output);
		resetObjectForSale();
		setCurrentPlayer("Federico");

	}

	public void testMainActionBGetBusinessPermitTile() {

		String output;

		controller.setTypeRound(TypeRound.NORMAL);

		setCurrentPlayer("Alessandro");

		setBalconyRegion(ColorCouncillor.BLACK, TypeRegion.SEASIDE);

		addCardPolitic("Alessandro", "BLACK");
		addCardPolitic("Alessandro", "BLACK");
		addCardPolitic("Alessandro", "BLACK");
		addCardPolitic("Alessandro", "BLACK");
		addCardPolitic("Alessandro", "BLACK");
		addCardPolitic("Alessandro", "BLACK");
		addCardPolitic("Alessandro", "BLACK");
		addCardPolitic("Alessandro", "BLACK");

		addCoins("Alessandro", 100);

		output = controller.roundNormal(
				"2-SEASIDE:" + model.getBoard().getRegion(TypeRegion.SEASIDE).getTileBusinessPermitA().toString()
						+ ":BLACK:BLACK:BLACK:BLACK");
		assertEquals("OK", output);
		setCurrentPlayer("Alessandro");

		output = controller.roundNormal("8");
		output = controller.roundNormal(
				"2-SEASIDE:" + model.getBoard().getRegion(TypeRegion.SEASIDE).getTileBusinessPermitA().toString()
						+ ":BLACK:BLACK:BLACK:BLACK");
		assertEquals("OK", output);
		setCurrentPlayer("Alessandro");

	}

	public void testMainActionBuildEmporiumWithKing() {

		String output;

		controller.setTypeRound(TypeRound.NORMAL);

		setCurrentPlayer("Alessandro");
		addCoins("Alessandro", 100);

		setBalconyKingsBoard(ColorCouncillor.BLACK);

		addCardPolitic("Alessandro", "SPECIAL");
		addCardPolitic("Alessandro", "BLACK");
		addCardPolitic("Alessandro", "BLACK");
		addCardPolitic("Alessandro", "BLACK");

		output = controller.roundNormal("4-Special:Indur:Kultos");
		assertEquals("OK", output);
		setCurrentPlayer("Alessandro");
	}

	public void testMainActionBuildEmporiumPermitTile() {

		String output;
		TileBusinessPermit tile = null;
		TileBusinessPermit tile2 = null;
		ArrayList<String> city = new ArrayList<>();
		ArrayList<String> city2 = new ArrayList<>();
		ArrayList<Bonus> bonus = new ArrayList<>();

		controller.setTypeRound(TypeRound.NORMAL);

		setCurrentPlayer("Alessandro");

		city.add("C");
		bonus.add(new BonusCoins(10));
		tile = new TileBusinessPermit(TypeRegion.SEASIDE, city, bonus);
		city2.add("D");
		tile2 = new TileBusinessPermit(TypeRegion.SEASIDE, city2, bonus);

		model.getCurrentPlayer().addBusinessPermit(tile);
		model.getCurrentPlayer().addBusinessPermit(tile2);

		output = controller
				.roundNormal("3-" + model.getCurrentPlayer().getTileBusinessPermit().get(1) + ":Dorful" + "");
		assertEquals("OK", output);
		setCurrentPlayer("Alessandro");
	}

	public void testMarketRound2() {

		String output;

		controller.setTypeRound(TypeRound.MARKET2);

		setCurrentPlayer("Alessandro");

		addCoins("Alessandro", 1000);

		addObjectForSale("Federico");

		output = controller.roundMarket2("");
		assertEquals("OK", output);
		setCurrentPlayer("Alessandro");

		output = controller.roundMarket2("Federico:A:3");
		assertEquals("OK", output);
		setCurrentPlayer("Alessandro");
		addObjectForSale("Federico");

		output = controller.roundMarket2("Federico:C:Black");
		assertEquals("OK", output);
		setCurrentPlayer("Alessandro");
		addObjectForSale("Federico");

		output = controller.roundMarket2("Federico:T:SEASIDE|C Bonus coins [10]");
		assertEquals("OK", output);
		setCurrentPlayer("Alessandro");
		addObjectForSale("Federico");

		output = controller.roundMarket2("Federico:A:3-Federico:C:Black-Federico:T:SEASIDE|C Bonus coins [10]");
		assertEquals("OK", output);
		setCurrentPlayer("Alessandro");
		addObjectForSale("Federico");

		output = controller.roundMarket2("Federico:T:SEASIDE|C Bonus coins [10]-Federico:T:SEASIDE|D Bonus coins [10]");
		assertEquals("OK", output);
		setCurrentPlayer("Alessandro");
		addObjectForSale("Federico");

		output = controller.roundMarket2(
				"Federico:A:3-Federico:C:Black-Federico:T:SEASIDE|C Bonus coins [10]-Federico:T:SEASIDE|D Bonus coins [10]");
		assertEquals("OK", output);
		setCurrentPlayer("Alessandro");
		addObjectForSale("Federico");

	}

}