package it.polimi.ingsw.ps30;

import model.*;
import bonusandcards.*;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import junit.framework.TestCase;

public class TestPlayer extends TestCase {

	private Player player;
	Random rnd;

	public TestPlayer(String name) {
		super(name);
	}

	@Before
	public void setUp() {
		Deck<CardPolitic> deck;
		int n;

		rnd = new Random();
		deck = new Deck<CardPolitic>(true);

		for (ColorCouncillor color : ColorCouncillor.values()) {
			n = rnd.nextInt(14);
			for (int i = 0; i < n; i++) {
				deck.add(new CardPolitic(color, false));
			}
		}

		n = rnd.nextInt(14);
		for (int i = 0; i < n; i++) {
			deck.add(new CardPolitic(null, true));
		}

		player = new Player("Federico", Color.black, deck, new HashMap<>(),10);
	}

	public void testDrawPoliticCard() {
		boolean correct;

		for (int i = 0; i < 500; i++) {
			try {
				player.drawCardPolitic();
				correct = true;
			} catch (Exception exception) {
				correct = false;
			}

			assertTrue(correct);
		}
	}

	public void testHaveCardPolitic() {
		ArrayList<CardPolitic> cards;

		cards = new ArrayList<CardPolitic>();

		player.getcardsPolitic().clear();
		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.WHITE, false));
		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.WHITE, false));
		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.WHITE, false));
		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.WHITE, false));

		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.BLACK, false));
		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.BLACK, false));

		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.BLUE, false));

		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.ORANGE, false));
		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.ORANGE, false));
		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.ORANGE, false));

		player.getcardsPolitic().add(new CardPolitic(null, true));

		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));

		assertTrue("1", player.haveCardsPolitic(cards));

		cards = new ArrayList<CardPolitic>();

		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));

		assertFalse("2", player.haveCardsPolitic(cards));

		cards = new ArrayList<CardPolitic>();

		cards.add(new CardPolitic(ColorCouncillor.WHITE, false));
		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(ColorCouncillor.ORANGE, false));
		cards.add(new CardPolitic(ColorCouncillor.PURPLE, false));

		assertFalse("3", player.haveCardsPolitic(cards));

		cards = new ArrayList<CardPolitic>();

		cards.add(new CardPolitic(ColorCouncillor.WHITE, false));
		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(ColorCouncillor.ORANGE, false));
		cards.add(new CardPolitic(null, true));

		assertTrue("4", player.haveCardsPolitic(cards));

		cards = new ArrayList<CardPolitic>();

		cards.add(new CardPolitic(null, true));
		cards.add(new CardPolitic(null, true));

		assertFalse("5", player.haveCardsPolitic(cards));
	}

	public void testAquireTileBusinessPermit() {

		boolean correct;
		TileBusinessPermit tileBusinessPermit;
		List<CardPolitic> cards;
		int numberCoins;

		tileBusinessPermit = new TileBusinessPermit(TypeRegion.HILLSIDE, null);

		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.WHITE, false));
		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.WHITE, false));
		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.WHITE, false));
		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.WHITE, false));

		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.BLACK, false));
		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.BLACK, false));

		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.BLUE, false));

		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.ORANGE, false));
		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.ORANGE, false));
		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.ORANGE, false));
		player.getcardsPolitic().add(new CardPolitic(ColorCouncillor.ORANGE, false));

		player.getcardsPolitic().add(new CardPolitic(null, true));
		player.getcardsPolitic().add(new CardPolitic(null, true));

		player.addCoins(30);

		cards = new ArrayList<CardPolitic>();
		cards.add(new CardPolitic(ColorCouncillor.WHITE, false));

		numberCoins = 10;

		try {
			player.aquireTileBusinessPermit(tileBusinessPermit, cards, numberCoins);
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertTrue("1", correct);

		cards = new ArrayList<CardPolitic>();
		cards.add(new CardPolitic(ColorCouncillor.WHITE, false));
		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));

		numberCoins = 7;

		try {
			player.aquireTileBusinessPermit(tileBusinessPermit, cards, numberCoins);
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertTrue("2", correct);

		cards = new ArrayList<CardPolitic>();
		cards.add(new CardPolitic(ColorCouncillor.ORANGE, false));
		cards.add(new CardPolitic(null, true));

		numberCoins = 7;

		try {
			player.aquireTileBusinessPermit(tileBusinessPermit, cards, numberCoins);
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertTrue("3", correct);

		cards = new ArrayList<CardPolitic>();
		cards.add(new CardPolitic(ColorCouncillor.ORANGE, false));
		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(ColorCouncillor.WHITE, false));
		cards.add(new CardPolitic(ColorCouncillor.BLUE, false));

		numberCoins = 0;

		try {
			player.aquireTileBusinessPermit(tileBusinessPermit, cards, numberCoins);
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertTrue("4", correct);

		cards = new ArrayList<CardPolitic>();
		cards.add(new CardPolitic(ColorCouncillor.ORANGE, false));
		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(ColorCouncillor.WHITE, false));
		cards.add(new CardPolitic(ColorCouncillor.BLUE, false));

		numberCoins = 0;

		try {
			player.aquireTileBusinessPermit(tileBusinessPermit, cards, numberCoins);
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertFalse("5", correct);

		cards = new ArrayList<CardPolitic>();
		cards.add(new CardPolitic(ColorCouncillor.ORANGE, false));
		player.addCoins(10);

		numberCoins = 10;

		try {
			player.aquireTileBusinessPermit(tileBusinessPermit, cards, numberCoins);
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertTrue("6", correct);

		cards = new ArrayList<CardPolitic>();
		cards.add(new CardPolitic(ColorCouncillor.ORANGE, false));

		numberCoins = 10;

		try {
			player.aquireTileBusinessPermit(tileBusinessPermit, cards, numberCoins);
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertFalse("7", correct);
	}

	public void testBuildEmporiumPermitTile() {

		boolean correct;

		player.addAssistants(10);

		TileBusinessPermit tileBusinessPermit;

		for (int i = 0; i < 5; i++) {
			tileBusinessPermit = new TileBusinessPermit(TypeRegion.SEASIDE);
			player.getTileBusinessPermit().add(tileBusinessPermit);
		}
		for (int i = 0; i < 5; i++) {
			tileBusinessPermit = new TileBusinessPermit(TypeRegion.MOUNTAIN);
			player.getTileBusinessPermit().add(tileBusinessPermit);
		}
		for (int i = 0; i < 5; i++) {
			tileBusinessPermit = new TileBusinessPermit(TypeRegion.HILLSIDE);
			player.getTileBusinessPermit().add(tileBusinessPermit);
		}

		tileBusinessPermit = new TileBusinessPermit(TypeRegion.SEASIDE);

		try {
			player.buildEmporiumPermitTile(tileBusinessPermit, 5);
			correct = true;
		} catch (Exception e) {
			correct = false;
		}

		assertTrue(correct);

		tileBusinessPermit = new TileBusinessPermit(TypeRegion.SEASIDE);

		try {
			player.buildEmporiumPermitTile(tileBusinessPermit, 6);
			correct = true;
		} catch (Exception e) {
			correct = false;
		}

		assertFalse(correct);

		player.addAssistants(20);

		tileBusinessPermit = new TileBusinessPermit(TypeRegion.SEASIDE);

		try {
			player.buildEmporiumPermitTile(tileBusinessPermit, 1);
			player.buildEmporiumPermitTile(tileBusinessPermit, 1);
			correct = true;
		} catch (Exception e) {
			correct = false;
		}

		assertTrue(correct);

		tileBusinessPermit = new TileBusinessPermit(TypeRegion.HILLSIDE, null);

		try {
			player.buildEmporiumPermitTile(tileBusinessPermit, 1);
			player.buildEmporiumPermitTile(tileBusinessPermit, 1);
			player.buildEmporiumPermitTile(tileBusinessPermit, 1);
			player.buildEmporiumPermitTile(tileBusinessPermit, 1);
			correct = true;
		} catch (Exception e) {
			correct = false;
		}

		assertTrue(correct);

		tileBusinessPermit = new TileBusinessPermit(TypeRegion.MOUNTAIN, null);

		try {
			player.buildEmporiumPermitTile(tileBusinessPermit, 1);
			player.buildEmporiumPermitTile(tileBusinessPermit, 1);
			player.buildEmporiumPermitTile(tileBusinessPermit, 1);
			correct = true;
		} catch (Exception e) {
			correct = false;
		}

		assertTrue(correct);

		tileBusinessPermit = new TileBusinessPermit(TypeRegion.MOUNTAIN, null);

		try {
			player.buildEmporiumPermitTile(tileBusinessPermit, 1);
			correct = true;
		} catch (Exception e) {
			correct = false;
		}

		assertFalse(correct);

		tileBusinessPermit = new TileBusinessPermit(TypeRegion.SEASIDE, null);

		try {
			player.buildEmporiumPermitTile(tileBusinessPermit, 1);
			correct = true;
		} catch (Exception e) {
			correct = false;
		}

		assertFalse(correct);
	}

	public void testBuildEmporiumKing() {

		ArrayList<CardPolitic> cards;
		Boolean correct;

		cards = new ArrayList<>();
		player.addAssistants(10);
		player.addCoins(10);
		player.getcardsPolitic().clear();

		player.addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false));
		player.addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false));
		player.addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false));
		player.addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("ORANGE"), false));
		player.addCardPolitic(new CardPolitic(null, true));

		cards.add(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false));

		try {
			player.buildEmporiumKing(2, 2, cards);
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertTrue("1", correct);
		cards.clear();

		cards.add(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false));
		cards.add(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false));
		cards.add(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false));

		try {
			player.buildEmporiumKing(2, 2, cards);
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertFalse("1", correct);
		cards.clear();

		cards.add(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false));

		try {
			player.buildEmporiumKing(10, 2, cards);
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertFalse("2", correct);
		cards.clear();

		cards.add(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false));

		try {
			player.buildEmporiumKing(2, 10, cards);
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertFalse("3", correct);
		cards.clear();

		cards.add(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false));
		cards.add(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false));
		cards.add(new CardPolitic(ColorCouncillor.parseIn("ORANGE"), false));

		try {
			player.buildEmporiumKing(2, 2, cards);
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}
		assertTrue("4", correct);
		cards.clear();

	}

	public void testHaveForSale() {

		ArrayList<ObjectForSale> listObject;

		listObject = new ArrayList<>();

		player.addAssistants(10);
		player.addCoins(10);
		player.getcardsPolitic().clear();

		player.addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false));
		player.addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false));
		player.addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false));
		player.addCardPolitic(new CardPolitic(null, true));

		player.addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")));
		player.addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("SEASIDE")));

		player.addAssistantsForSale(5, 5);
		player.addCardPoliticForSale(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false), 5);
		player.addCardPoliticForSale(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false), 5);
		player.addTileBusinessPermitForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 5);

		assertTrue("1",
				player.haveForSale(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false), 5)));
		assertFalse("2",
				player.haveForSale(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("ORANGE"), false), 5)));
		assertTrue("3",
				player.haveForSale(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false), 10)));
		assertFalse("4", player.haveForSale(new ObjectForSale(new CardPolitic(null, true), 5)));

		assertTrue("5", player.haveForSale(new ObjectForSale(5, 5)));
		assertFalse("6", player.haveForSale(new ObjectForSale(6, 5)));
		assertTrue("7", player.haveForSale(new ObjectForSale(5, 15)));

		assertTrue("8",
				player.haveForSale(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 5)));
		assertTrue("9",
				player.haveForSale(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 8)));
		assertFalse("10",
				player.haveForSale(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("MOUNTAIN")), 5)));

		listObject.add(new ObjectForSale(5, 5));
		listObject.add(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("ORANGE"), false), 5));
		listObject.add(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 5));

		assertFalse("11", player.haveForSale(listObject));
		listObject.clear();

		listObject.add(new ObjectForSale(5, 5));
		listObject.add(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false), 5));
		listObject.add(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false), 5));
		listObject.add(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false), 5));
		listObject.add(new ObjectForSale(new CardPolitic(null, true), 5));
		listObject.add(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 5));
		listObject.add(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("SEASIDE")), 5));

		assertFalse("12", player.haveForSale(listObject));
		listObject.clear();

		listObject.add(new ObjectForSale(5, 5));
		listObject.add(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false), 5));
		listObject.add(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false), 5));
		listObject.add(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 5));

		assertTrue("13", player.haveForSale(listObject));
		listObject.clear();

		listObject.add(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 5));

		assertTrue("14", player.haveForSale(listObject));
		listObject.clear();

		listObject.add(new ObjectForSale(5, 5));

		assertTrue("15", player.haveForSale(listObject));
		listObject.clear();

		listObject.add(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false), 5));
		listObject.add(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false), 5));

		assertTrue("16", player.haveForSale(listObject));
		listObject.clear();
	}

	public void testGetPriceOfObjects() {

		Boolean correct;
		List<ObjectForSale> listObject;

		listObject = new ArrayList<>();
		
		player.addAssistants(10);
		player.addCoins(10);
		player.getcardsPolitic().clear();

		player.addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false));
		player.addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false));
		player.addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false));
		player.addCardPolitic(new CardPolitic(null, true));

		player.addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")));
		player.addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")));
		player.addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("SEASIDE")));
		player.addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("SEASIDE")));

		player.addAssistantsForSale(5, 5);
		player.addCardPoliticForSale(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false), 5);
		player.addCardPoliticForSale(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false), 5);
		player.addCardPoliticForSale(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false), 2);
		player.addTileBusinessPermitForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 5);
		player.addTileBusinessPermitForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 7);

		listObject.add(new ObjectForSale(1, 0));
		listObject.add(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false), 0));
		listObject.add(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false), 0));
		listObject.add(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 0));

		assertEquals(17, player.getPriceOfObjects(listObject));
		listObject.clear();

		listObject.add(new ObjectForSale(5, 0));

		assertEquals(25, player.getPriceOfObjects(listObject));
		listObject.clear();

		listObject.add(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false), 0));
		listObject.add(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false), 0));

		assertEquals(7, player.getPriceOfObjects(listObject));
		listObject.clear();

		listObject.add(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 0));
		listObject.add(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 0));

		assertEquals(12, player.getPriceOfObjects(listObject));
		listObject.clear();

		listObject.add(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 0));
		listObject.add(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("SEASIDE")), 0));

		try {
			assertEquals(5, player.getPriceOfObjects(listObject));
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}
		assertTrue(correct);
		listObject.clear();

		listObject.add(new ObjectForSale(10, 0));
		listObject.add(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 0));
		listObject.add(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("SEASIDE")), 0));

		try {
			assertEquals(55, player.getPriceOfObjects(listObject));
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}
		assertTrue(correct);
		listObject.clear();

	}

	public void testBuyObject() {

		boolean correct;

		player.addAssistants(10);
		player.addCoins(10);
		player.getcardsPolitic().clear();

		player.addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false));
		player.addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false));
		player.addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false));
		player.addCardPolitic(new CardPolitic(null, true));

		player.addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")));
		player.addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")));
		player.addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("SEASIDE")));
		player.addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("SEASIDE")));

		player.addAssistantsForSale(5, 5);
		player.addCardPoliticForSale(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false), 5);
		player.addCardPoliticForSale(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false), 5);
		player.addCardPoliticForSale(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false), 2);
		player.addTileBusinessPermitForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 5);
		player.addTileBusinessPermitForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 7);

		assertEquals(20, player.buyObject(new ObjectForSale(4, 0)));

		assertEquals(2,
				player.buyObject(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false), 0)));

		assertEquals(5,
				player.buyObject(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false), 0)));

		assertEquals(5,
				player.buyObject(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false), 0)));

		assertEquals(5, player.buyObject(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 0)));

		assertEquals(7, player.buyObject(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 0)));

		assertEquals(5, player.buyObject(new ObjectForSale(1, 0)));

		try {
			assertEquals(0, player.buyObject(new ObjectForSale(1, 0)));
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertFalse(correct);

		assertEquals(0,player.buyObject(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false), 0)));
		
		assertEquals(0, player.buyObject(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")), 0)));
	}
}
