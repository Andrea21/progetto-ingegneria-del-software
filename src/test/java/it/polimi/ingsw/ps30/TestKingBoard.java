package it.polimi.ingsw.ps30;

import junit.framework.TestCase;
import model.ColorCouncillor;
import model.KingBoard;
import model.Model;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;

import bonusandcards.CardPolitic;

public class TestKingBoard extends TestCase {

	KingBoard kingBoard;
	
	@Before
	public void setUp() throws IOException, Exception{
		Model model;

		HashMap<String, Color> infoPlayer;

		infoPlayer = new HashMap<>();

		infoPlayer.put("Federico", Color.red);
		infoPlayer.put("Riccardo", Color.black);

		model = new Model(infoPlayer, "Configuration/Configurazione 1/Configuration.xml");
		
		kingBoard = model.getBoard().getKingBoard();
	}
	
	public void testCheckCardsMeetCouncil() {

		List<CardPolitic> cards;

		kingBoard.electCouncillor(ColorCouncillor.BLACK);
		kingBoard.electCouncillor(ColorCouncillor.BLACK);
		kingBoard.electCouncillor(ColorCouncillor.WHITE);
		kingBoard.electCouncillor(ColorCouncillor.PURPLE);

		cards = new ArrayList<>();

		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		assertTrue("1", kingBoard.checkCardsMeetCouncil(cards));
		cards.clear();

		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(ColorCouncillor.WHITE, false));
		assertTrue("2", kingBoard.checkCardsMeetCouncil(cards));
		cards.clear();

		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(ColorCouncillor.WHITE, false));
		cards.add(new CardPolitic(ColorCouncillor.PURPLE, false));
		assertTrue("3", kingBoard.checkCardsMeetCouncil(cards));
		cards.clear();

		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(null, true));
		cards.add(new CardPolitic(ColorCouncillor.WHITE, false));
		cards.add(new CardPolitic(ColorCouncillor.PURPLE, false));
		assertTrue("4", kingBoard.checkCardsMeetCouncil(cards));
		cards.clear();

		cards.add(new CardPolitic(null, true));
		assertTrue("5", kingBoard.checkCardsMeetCouncil(cards));
		cards.clear();

		cards.add(new CardPolitic(ColorCouncillor.FUCHSIA, false));
		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(ColorCouncillor.WHITE, false));
		cards.add(new CardPolitic(ColorCouncillor.PURPLE, false));
		assertFalse("6", kingBoard.checkCardsMeetCouncil(cards));
		cards.clear();

		cards.add(new CardPolitic(ColorCouncillor.FUCHSIA, false));
		cards.add(new CardPolitic(null, true));
		assertFalse("7", kingBoard.checkCardsMeetCouncil(cards));
		cards.clear();

		cards.add(new CardPolitic(ColorCouncillor.FUCHSIA, false));
		cards.add(new CardPolitic(null, true));
		cards.add(new CardPolitic(null, true));
		cards.add(new CardPolitic(null, true));
		assertFalse("8", kingBoard.checkCardsMeetCouncil(cards));
		cards.clear();

		cards.add(new CardPolitic(null, true));
		cards.add(new CardPolitic(null, true));
		cards.add(new CardPolitic(null, true));
		assertTrue("9", kingBoard.checkCardsMeetCouncil(cards));
		cards.clear();

		assertFalse("10", kingBoard.checkCardsMeetCouncil(cards));
		cards.clear();

	}
	
	public void testElectCouncillor() {

		for (int i = 0; i < kingBoard.getCouncillors().length; i++) {
			kingBoard.getCouncillors()[i] = null;
		}

		kingBoard.getCouncillors()[0] = ColorCouncillor.WHITE;
		kingBoard.getCouncillors()[1] = ColorCouncillor.WHITE;
		kingBoard.getCouncillors()[2] = ColorCouncillor.WHITE;
		kingBoard.getCouncillors()[3] = ColorCouncillor.WHITE;

		kingBoard.electCouncillor(ColorCouncillor.BLACK);
		assertEquals(kingBoard.getCouncillors()[0], ColorCouncillor.BLACK);
		assertEquals(kingBoard.getCouncillors()[1], ColorCouncillor.WHITE);
		assertEquals(kingBoard.getCouncillors()[2], ColorCouncillor.WHITE);
		assertEquals(kingBoard.getCouncillors()[3], ColorCouncillor.WHITE);

		kingBoard.electCouncillor(ColorCouncillor.BLUE);
		assertEquals(kingBoard.getCouncillors()[0], ColorCouncillor.BLUE);
		assertEquals(kingBoard.getCouncillors()[1], ColorCouncillor.BLACK);
		assertEquals(kingBoard.getCouncillors()[2], ColorCouncillor.WHITE);
		assertEquals(kingBoard.getCouncillors()[3], ColorCouncillor.WHITE);

		kingBoard.electCouncillor(ColorCouncillor.ORANGE);
		assertEquals(kingBoard.getCouncillors()[0], ColorCouncillor.ORANGE);
		assertEquals(kingBoard.getCouncillors()[1], ColorCouncillor.BLUE);
		assertEquals(kingBoard.getCouncillors()[2], ColorCouncillor.BLACK);
		assertEquals(kingBoard.getCouncillors()[3], ColorCouncillor.WHITE);

		kingBoard.electCouncillor(ColorCouncillor.FUCHSIA);
		assertEquals(kingBoard.getCouncillors()[0], ColorCouncillor.FUCHSIA);
		assertEquals(kingBoard.getCouncillors()[1], ColorCouncillor.ORANGE);
		assertEquals(kingBoard.getCouncillors()[2], ColorCouncillor.BLUE);
		assertEquals(kingBoard.getCouncillors()[3], ColorCouncillor.BLACK);

	}
	
}
