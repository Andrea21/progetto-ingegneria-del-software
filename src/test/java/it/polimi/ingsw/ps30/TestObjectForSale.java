package it.polimi.ingsw.ps30;

import junit.framework.TestCase;
import model.ColorCouncillor;
import model.ObjectForSale;
import model.TypeRegion;

import org.junit.Before;

import bonusandcards.CardPolitic;
import bonusandcards.TileBusinessPermit;

public class TestObjectForSale extends TestCase {

	ObjectForSale objectAssistants;
	ObjectForSale objectPoliticCard;
	ObjectForSale objectPermitTile;
	
	@Before
	public void setUp(){
		objectAssistants = new ObjectForSale(10,2);
		objectPoliticCard = new ObjectForSale(new CardPolitic(ColorCouncillor.BLACK, false), 10);
		objectPermitTile = new ObjectForSale(new TileBusinessPermit(TypeRegion.HILLSIDE), 10);
	}
	
	public void testGetObject(){
		
		assertEquals(10, (int)objectAssistants.getAssistants());
		assertEquals(new CardPolitic(ColorCouncillor.BLACK, false), objectPoliticCard.getCardPolitic());
		assertEquals(new TileBusinessPermit(TypeRegion.HILLSIDE), objectPermitTile.getTileBusinessPermit());
	}
	
	public void testGetPrice(){
		
		assertEquals(2,(int) objectAssistants.getPrice());
		assertEquals(10,(int) objectPoliticCard.getPrice());
		assertEquals(10,(int) objectPermitTile.getPrice());
	}
	
	public void testIs(){
		
		assertTrue(objectAssistants.isAssistants());
		assertFalse(objectAssistants.isCardPolitc());
		assertFalse(objectAssistants.isTileBusinessPermit());
		
		
		assertFalse(objectPoliticCard.isAssistants());
		assertTrue(objectPoliticCard.isCardPolitc());
		assertFalse(objectPoliticCard.isTileBusinessPermit());
		
		assertFalse(objectPermitTile.isAssistants());
		assertFalse(objectPermitTile.isCardPolitc());
		assertTrue(objectPermitTile.isTileBusinessPermit());
	}
	
	public void testEquals(){
		
		assertTrue(objectAssistants.equals(new ObjectForSale(10, 1)));
		assertFalse(objectAssistants.equals(new ObjectForSale(4, 1)));
		
		assertTrue(objectPoliticCard.equals(new ObjectForSale(new CardPolitic(ColorCouncillor.BLACK, false),1)));
		assertFalse(objectPoliticCard.equals(new ObjectForSale(new CardPolitic(ColorCouncillor.PURPLE, false),1)));
		
		assertTrue(objectPermitTile.equals(new ObjectForSale(new TileBusinessPermit(TypeRegion.HILLSIDE),2)));
		assertFalse(objectPermitTile.equals(new ObjectForSale(new TileBusinessPermit(TypeRegion.SEASIDE),2)));
		
		assertFalse(objectAssistants.equals(objectPermitTile));
		assertFalse(objectAssistants.equals(objectPoliticCard));
		assertFalse(objectPoliticCard.equals(objectPermitTile));
	}
	
}
