package it.polimi.ingsw.ps30;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;

import bonusandcards.CardPolitic;
import junit.framework.TestCase;
import model.ColorCouncillor;
import model.Model;
import model.Region;
import model.TypeRegion;

public class TestRegion extends TestCase {

	private Region region;

	@Before
	public void setUp() throws Exception {

		Model model;

		HashMap<String, Color> infoPlayer;

		infoPlayer = new HashMap<>();

		infoPlayer.put("Federico", Color.red);
		infoPlayer.put("Riccardo", Color.black);

		model = new Model(infoPlayer, "Configuration/Configurazione 1/Configuration.xml");

		region = model.getBoard().getRegion(TypeRegion.SEASIDE);

	}

	public void testGetCity() {

		assertEquals("Arkon", region.getCity("Arkon").getName());
		assertEquals("Castrum", region.getCity("Castrum").getName());
		assertEquals("Burgen", region.getCity("Burgen").getName());
		assertEquals("Dorful", region.getCity("Dorful").getName());
		assertEquals("Esti", region.getCity("Esti").getName());
		assertEquals(null, region.getCity("aas"));

	}

	public void testCheckCardsMeetCouncil() {

		List<CardPolitic> cards;

		region.electCouncillor(ColorCouncillor.BLACK);
		region.electCouncillor(ColorCouncillor.BLACK);
		region.electCouncillor(ColorCouncillor.WHITE);
		region.electCouncillor(ColorCouncillor.PURPLE);

		cards = new ArrayList<>();

		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		assertTrue("1", region.checkCardsMeetCouncil(cards));
		cards.clear();

		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(ColorCouncillor.WHITE, false));
		assertTrue("2", region.checkCardsMeetCouncil(cards));
		cards.clear();

		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(ColorCouncillor.WHITE, false));
		cards.add(new CardPolitic(ColorCouncillor.PURPLE, false));
		assertTrue("3", region.checkCardsMeetCouncil(cards));
		cards.clear();

		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(null, true));
		cards.add(new CardPolitic(ColorCouncillor.WHITE, false));
		cards.add(new CardPolitic(ColorCouncillor.PURPLE, false));
		assertTrue("4", region.checkCardsMeetCouncil(cards));
		cards.clear();

		cards.add(new CardPolitic(null, true));
		assertTrue("5", region.checkCardsMeetCouncil(cards));
		cards.clear();

		cards.add(new CardPolitic(ColorCouncillor.FUCHSIA, false));
		cards.add(new CardPolitic(ColorCouncillor.BLACK, false));
		cards.add(new CardPolitic(ColorCouncillor.WHITE, false));
		cards.add(new CardPolitic(ColorCouncillor.PURPLE, false));
		assertFalse("6", region.checkCardsMeetCouncil(cards));
		cards.clear();

		cards.add(new CardPolitic(ColorCouncillor.FUCHSIA, false));
		cards.add(new CardPolitic(null, true));
		assertFalse("7", region.checkCardsMeetCouncil(cards));
		cards.clear();

		cards.add(new CardPolitic(ColorCouncillor.FUCHSIA, false));
		cards.add(new CardPolitic(null, true));
		cards.add(new CardPolitic(null, true));
		cards.add(new CardPolitic(null, true));
		assertFalse("8", region.checkCardsMeetCouncil(cards));
		cards.clear();

		cards.add(new CardPolitic(null, true));
		cards.add(new CardPolitic(null, true));
		cards.add(new CardPolitic(null, true));
		assertTrue("9", region.checkCardsMeetCouncil(cards));
		cards.clear();

		assertFalse("10", region.checkCardsMeetCouncil(cards));
		cards.clear();

	}

	public void testElectCouncillor() {

		for (int i = 0; i < region.getCouncillors().length; i++) {
			region.getCouncillors()[i] = null;
		}

		region.getCouncillors()[0] = ColorCouncillor.WHITE;
		region.getCouncillors()[1] = ColorCouncillor.WHITE;
		region.getCouncillors()[2] = ColorCouncillor.WHITE;
		region.getCouncillors()[3] = ColorCouncillor.WHITE;

		region.electCouncillor(ColorCouncillor.BLACK);
		assertEquals(region.getCouncillors()[0], ColorCouncillor.BLACK);
		assertEquals(region.getCouncillors()[1], ColorCouncillor.WHITE);
		assertEquals(region.getCouncillors()[2], ColorCouncillor.WHITE);
		assertEquals(region.getCouncillors()[3], ColorCouncillor.WHITE);

		region.electCouncillor(ColorCouncillor.BLUE);
		assertEquals(region.getCouncillors()[0], ColorCouncillor.BLUE);
		assertEquals(region.getCouncillors()[1], ColorCouncillor.BLACK);
		assertEquals(region.getCouncillors()[2], ColorCouncillor.WHITE);
		assertEquals(region.getCouncillors()[3], ColorCouncillor.WHITE);

		region.electCouncillor(ColorCouncillor.ORANGE);
		assertEquals(region.getCouncillors()[0], ColorCouncillor.ORANGE);
		assertEquals(region.getCouncillors()[1], ColorCouncillor.BLUE);
		assertEquals(region.getCouncillors()[2], ColorCouncillor.BLACK);
		assertEquals(region.getCouncillors()[3], ColorCouncillor.WHITE);

		region.electCouncillor(ColorCouncillor.FUCHSIA);
		assertEquals(region.getCouncillors()[0], ColorCouncillor.FUCHSIA);
		assertEquals(region.getCouncillors()[1], ColorCouncillor.ORANGE);
		assertEquals(region.getCouncillors()[2], ColorCouncillor.BLUE);
		assertEquals(region.getCouncillors()[3], ColorCouncillor.BLACK);

	}

	public void testAcquireTileBusinessPermit() {

		boolean correct;

		try {
			region.acquireTileBusinessPermit("");
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertFalse("1", correct);

		try {
			region.acquireTileBusinessPermit("A");
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertTrue("2", correct);

		try {
			region.acquireTileBusinessPermit("B");
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertTrue("3", correct);

		for (Integer i = 0; i < 8; i++) {
			try {
				region.acquireTileBusinessPermit("A");
				correct = true;
			} catch (Exception exception) {
				correct = false;
			}

			assertTrue(i.toString(), correct);
		}
		
		for (Integer i = 0; i < 5; i++) {
			try {
				region.acquireTileBusinessPermit("B");
				correct = true;
			} catch (Exception exception) {
				correct = false;
			}

			assertTrue(i.toString(), correct);
		}
		
		try {
			region.acquireTileBusinessPermit("A");
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertTrue(correct);
		
		try {
			assertEquals(null, region.acquireTileBusinessPermit("A"));
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertTrue(correct);
		
		try {
			assertEquals(null, region.acquireTileBusinessPermit("B"));
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertTrue(correct);

	}

	public void testChangeBusinessPermitTile(){
		
		boolean correct;
		
		for (int i=0; i<50;i++){
			region.changeBusinessPermitTile();
		}
		
		for (int i=0;i<15;i++){
			try {
				region.acquireTileBusinessPermit("A");
			} catch (Exception exception) {
			}
		}
		
		try {
			region.changeBusinessPermitTile();
			correct=true;
		} catch (Exception exception) {
			correct=false;
		}
		
		assertFalse(correct);
	}
	
}
