package it.polimi.ingsw.ps30;

import java.awt.Color;
import java.awt.Point;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import bonusandcards.CardPolitic;
import bonusandcards.Deck;
import junit.framework.TestCase;
import model.City;
import model.ColorCity;
import model.ColorCouncillor;
import model.Emporium;
import model.Player;

public class TestCity extends TestCase{
	
	Player player1;
	Player player2;
	City city;
	
	
	@Before
	public void setUp(){
		Deck<CardPolitic> deck = new Deck<>(true);
		
		for(int i=0; i<6; i++){
			deck.add(new CardPolitic(ColorCouncillor.BLACK, false));
		}
		player1 = new Player("Alessandro", Color.BLACK, deck, null, 10);
		player2 = new Player("Federico", Color.BLUE, deck, null, 10);
		city = new City("Milano", ColorCity.BRONZE, new Point());
		city.addNearbyCities(new City("Pavia", ColorCity.IRON, new Point()));
		city.addNearbyCities(new City("Torino", ColorCity.PURPLE, new Point()));
		city.addNearbyCities(new City("Brescia", ColorCity.SILVER, new Point()));
		
	}

	@Test
	public void testHasEmporium() {
		boolean correct;
		Emporium emporium = new Emporium(Color.BLACK);
		emporium.setOccupaid();
		
		city.getEmporiums().add(emporium);
		
		correct = city.hasEmporium(player1);
		assertTrue(correct);
		correct = city.hasEmporium(player2);
		assertFalse(correct);
	}
	
	@Test
	public void testBuildEmporium() {
		boolean correct;
		
		city.buildEmporium(player1);
		correct = city.hasEmporium(player1);
		assertTrue(correct);
	}
	
	@Test
	public void testCityVisit() {
		
		LinkedList<City> lista = new LinkedList<>();
		
		lista.add(city.getNearbyCity("Torino"));
		lista.add(city.getNearbyCity("Pavia"));
		
		city.buildEmporium(player1);
		city.getNearbyCity("Pavia").buildEmporium(player1);
		city.getNearbyCity("Torino").buildEmporium(player1);
		city.getListaVisita().clear();
		City.cityVisit(city, player1);
		assertEquals(lista, city.getListaVisita());
		
	
	}
	
	public void testGetConnectedCity() {
		
		LinkedList<City> lista = new LinkedList<>();
		
		lista.add(city.getNearbyCity("Torino"));
		lista.add(city.getNearbyCity("Pavia"));
		city.buildEmporium(player1);
		city.getNearbyCity("Pavia").buildEmporium(player1);
		city.getNearbyCity("Torino").buildEmporium(player1);
		assertEquals(lista, city.getConnectedCity(player1));
		
		
	}
	
	

}
