package it.polimi.ingsw.ps30;

import org.junit.Before;
import org.junit.Test;

import bonusandcards.CardPolitic;
import bonusandcards.Deck;
import bonusandcards.TileBusinessPermit;
import junit.framework.TestCase;
import model.*;

public class TestDeck extends TestCase{

	private Deck<CardPolitic> deckOfPoliticCards;
	private Deck<TileBusinessPermit> deckOfPermitTiles;
	private boolean correct;
	
	@Before
	public void setUp(){
		
		deckOfPoliticCards = new Deck<CardPolitic>(true);
		deckOfPermitTiles = new Deck<>(false);
		for(int i=0; i<500; i++){
			deckOfPoliticCards.add(new CardPolitic(ColorCouncillor.BLACK, false));
			deckOfPermitTiles.add(new TileBusinessPermit(TypeRegion.SEASIDE));
		}
	
	}
	
	
	@Test
	public void testDrawPoliticCards(){
		
		CardPolitic card = null;
		for(int i=0; i<501; i++){
			try {
				card = deckOfPoliticCards.draw();
				correct = true;
			} catch (Exception InfinityLoopException) {
				correct = false;
			}
			assertTrue(correct);
			deckOfPoliticCards.addRefuse(card);
		}
		
		
	}
	
	@Test
	public void testDrawPermitTile(){
		
		TileBusinessPermit permit = null;
		for(int i=0; i<500; i++){
			try {
				permit = deckOfPermitTiles.draw();
				correct = true;
			} catch (Exception InfinityLoopException) {
				correct = false;
			}
			assertTrue(correct);
			deckOfPermitTiles.addRefuse(permit);
		}
		try {
			permit = deckOfPermitTiles.draw();
			correct = true;
		} catch (Exception InfinityLoopException) {
			correct = false;
		}
		assertFalse(correct);
	}

}
