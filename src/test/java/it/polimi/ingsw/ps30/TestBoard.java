package it.polimi.ingsw.ps30;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;
import model.Board;

public class TestBoard extends TestCase {

	private Board board;

	@Before
	public void setUp() {
		try {
			board = new Board("Configuration/Configurazione 1/Configuration.xml");
		} catch (IOException exception) {
			TestCase.fail("Errore nell'apertura del file di configurazione: " + exception.getMessage());
			return;
		} catch (Exception exception) {
			fail("Errore nella creazione del model: " + exception.getMessage());
			return;
		}
	}

	@Test
	public void testElectCouncilor() {
		boolean correct;

		for(int i=0; i<board.getCouncillors().size(); i++){
		try {
			board.electCouncillor(board.getCouncillors().get(i), "SEASIDE");
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}
		assertTrue(correct);
		}
	}	

}
