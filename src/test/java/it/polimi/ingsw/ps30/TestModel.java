package it.polimi.ingsw.ps30;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;

import bonusandcards.CardPolitic;
import bonusandcards.TileBonus;
import bonusandcards.TileBonusColor;
import bonusandcards.TileBonusRegion;
import bonusandcards.TileBusinessPermit;
import junit.framework.TestCase;
import model.ColorCity;
import model.ColorCouncillor;
import model.Model;
import model.ObjectForSale;
import model.Player;
import model.TypeRegion;

public class TestModel extends TestCase {

	private Model model;

	public TestModel(String name) {
		super(name);
	}

	@Before
	protected void setUp() throws Exception {

		Map<String, Color> infoPlayer;

		infoPlayer = new HashMap<>();

		infoPlayer.put("Federico", new Color(0, 0, 0));
		infoPlayer.put("Alessandro", new Color(50, 0, 0));
		infoPlayer.put("Andrea", new Color(255, 255, 255));

		model = new Model(infoPlayer, "Configuration/Configurazione 1/Configuration.xml");
	}

	public void testGetPlayerByName() {
		assertEquals("Federico", model.getPlayerByName("Federico").getName());
		assertEquals("Alessandro", model.getPlayerByName("Alessandro").getName());
		assertEquals(null, model.getPlayerByName("Marco"));
	}

	public void testGetColorName() {

		/*
		 * correct=true; for (int r=0;r<=255;r++){ for (int g=0; g<=255; g++){
		 * for (int b=0; b<=255; b++){ try { Model.getColorName(new
		 * Color(r,g,b)); } catch (Exception exception) { correct=false; break;
		 * } } if (!correct){ break; } } if (!correct){ break; } }
		 * 
		 * assertTrue("1",correct);
		 */

		assertEquals("black", Model.getColorName(Color.BLACK));
		assertEquals("black", Model.getColorName(Color.black));
		assertEquals("blue", Model.getColorName(Color.BLUE));
		assertEquals("cyan", Model.getColorName(Color.CYAN));
		assertEquals("darkGray", Model.getColorName(Color.DARK_GRAY));
		assertEquals("gray", Model.getColorName(Color.GRAY));
		assertEquals("yellow", Model.getColorName(Color.YELLOW));
		assertEquals("red", Model.getColorName(Color.RED));
		assertEquals("unknown", Model.getColorName(new Color(50, 50, 50)));
	}

	public void testSetCurrentPlayer() {
		boolean correct;

		try {
			model.setCurrentPlayer(0);
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertTrue("1", correct);

		try {
			model.setCurrentPlayer(2);
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertTrue("1", correct);

		try {
			model.setCurrentPlayer(3);
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertFalse("1", correct);

		try {
			model.setCurrentPlayer(-1);
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertFalse("1", correct);

		try {
			model.setCurrentPlayer(model.getPlayerByName("Federico"));
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertTrue("1", correct);

		try {
			model.setCurrentPlayer(model.getPlayerByName("Alessandro"));
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertTrue("1", correct);

		try {
			model.setCurrentPlayer(model.getPlayerByName("Riccardo"));
			correct = true;
		} catch (Exception exception) {
			correct = false;
		}

		assertFalse("1", correct);
	}

	public void testAddObjectsForSale() {

		boolean correct;
		HashMap<CardPolitic, List<Integer>> cards;
		HashMap<TileBusinessPermit, List<Integer>> tiles;
		CardPolitic tempCard;
		TileBusinessPermit tempTile;

		cards = new HashMap<>();
		tiles = new HashMap<>();

		model.getCurrentPlayer().addAssistants(10);
		model.getCurrentPlayer().addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("SEASIDE")));
		model.getCurrentPlayer().addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("SEASIDE")));
		model.getCurrentPlayer().addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")));
		model.getCurrentPlayer().addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")));
		model.getCurrentPlayer().addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")));
		model.getCurrentPlayer().addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("MOUNTAIN")));

		model.getCurrentPlayer().getcardsPolitic().clear();
		model.getCurrentPlayer().addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("ORANGE"), false));
		model.getCurrentPlayer().addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false));
		model.getCurrentPlayer().addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false));
		model.getCurrentPlayer().addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false));
		model.getCurrentPlayer().addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("PURPLE"), false));
		model.getCurrentPlayer().addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("PURPLE"), false));
		model.getCurrentPlayer().addCardPolitic(new CardPolitic(null, true));

		tempCard = new CardPolitic(ColorCouncillor.parseIn("ORANGE"), false);
		cards.put(tempCard, new ArrayList<>());
		cards.get(tempCard).add(5);
		tempCard = new CardPolitic(ColorCouncillor.parseIn("WHITE"), false);
		cards.put(tempCard, new ArrayList<>());
		cards.put(tempCard, new ArrayList<>());
		cards.get(tempCard).add(5);
		cards.get(tempCard).add(6);

		tempCard = new CardPolitic(ColorCouncillor.parseIn("BLACK"), false);
		cards.put(tempCard, new ArrayList<>());
		cards.get(tempCard).add(5);
		tempCard = new CardPolitic(ColorCouncillor.parseIn("PURPLE"), false);
		cards.put(tempCard, new ArrayList<>());
		cards.get(tempCard).add(5);

		tempTile = new TileBusinessPermit(TypeRegion.parseIn("SEASIDE"));
		tiles.put(tempTile, new ArrayList<>());
		tiles.get(tempTile).add(2);
		tiles.get(tempTile).add(3);
		tempTile = new TileBusinessPermit(TypeRegion.parseIn("MOUNTAIN"));
		tiles.put(tempTile, new ArrayList<>());
		tiles.get(tempTile).add(2);
		tempTile = new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE"));
		tiles.put(tempTile, new ArrayList<>());
		tiles.get(tempTile).add(2);

		try {
			model.addObjectsForSale(5, 5, cards, tiles);
			correct = true;
		} catch (Exception e) {
			correct = false;
		}

		assertTrue("1", correct);
	}

	public void testBuyObjects() {
		boolean correct;
		HashMap<CardPolitic, List<Integer>> cards;
		HashMap<TileBusinessPermit, List<Integer>> tiles;
		HashMap<Player, List<ObjectForSale>> objects;
		CardPolitic tempCard;
		TileBusinessPermit tempTile;

		model.setCurrentPlayer(model.getPlayerByName("Andrea"));
		model.getCurrentPlayer().setPositionCoinsTrack(0);

		cards = new HashMap<>();
		tiles = new HashMap<>();
		objects = new HashMap<>();

		model.getCurrentPlayer().addAssistants(10);
		model.getCurrentPlayer().addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("SEASIDE")));
		model.getCurrentPlayer().addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("SEASIDE")));
		model.getCurrentPlayer().addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")));
		model.getCurrentPlayer().addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")));
		model.getCurrentPlayer().addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE")));
		model.getCurrentPlayer().addBusinessPermit(new TileBusinessPermit(TypeRegion.parseIn("MOUNTAIN")));

		model.getCurrentPlayer().getcardsPolitic().clear();
		model.getCurrentPlayer().addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("ORANGE"), false));
		model.getCurrentPlayer().addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false));
		model.getCurrentPlayer().addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false));
		model.getCurrentPlayer().addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("BLACK"), false));
		model.getCurrentPlayer().addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("PURPLE"), false));
		model.getCurrentPlayer().addCardPolitic(new CardPolitic(ColorCouncillor.parseIn("PURPLE"), false));
		model.getCurrentPlayer().addCardPolitic(new CardPolitic(null, true));

		tempCard = new CardPolitic(ColorCouncillor.parseIn("ORANGE"), false);
		cards.put(tempCard, new ArrayList<>());
		cards.get(tempCard).add(5);
		tempCard = new CardPolitic(ColorCouncillor.parseIn("WHITE"), false);
		cards.put(tempCard, new ArrayList<>());
		cards.put(tempCard, new ArrayList<>());
		cards.get(tempCard).add(5);
		cards.get(tempCard).add(6);

		tempCard = new CardPolitic(ColorCouncillor.parseIn("BLACK"), false);
		cards.put(tempCard, new ArrayList<>());
		cards.get(tempCard).add(5);
		tempCard = new CardPolitic(ColorCouncillor.parseIn("PURPLE"), false);
		cards.put(tempCard, new ArrayList<>());
		cards.get(tempCard).add(5);

		tempTile = new TileBusinessPermit(TypeRegion.parseIn("SEASIDE"));
		tiles.put(tempTile, new ArrayList<>());
		tiles.get(tempTile).add(2);
		tiles.get(tempTile).add(3);
		tempTile = new TileBusinessPermit(TypeRegion.parseIn("MOUNTAIN"));
		tiles.put(tempTile, new ArrayList<>());
		tiles.get(tempTile).add(2);
		tempTile = new TileBusinessPermit(TypeRegion.parseIn("HILLSIDE"));
		tiles.put(tempTile, new ArrayList<>());
		tiles.get(tempTile).add(2);

		model.addObjectsForSale(5, 5, cards, tiles);

		model.setCurrentPlayer(model.getPlayerByName("Federico"));
		model.getCurrentPlayer().setPositionCoinsTrack(0);
		model.getCurrentPlayer().addCoins(27);

		objects.put(model.getPlayerByName("Andrea"), new ArrayList<>());

		objects.get(model.getPlayerByName("Andrea")).add(new ObjectForSale(2, 5));

		objects.get(model.getPlayerByName("Andrea"))
				.add(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("ORANGE"), false), 5));
		objects.get(model.getPlayerByName("Andrea"))
				.add(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("WHITE"), false), 5));
		objects.get(model.getPlayerByName("Andrea"))
				.add(new ObjectForSale(new CardPolitic(ColorCouncillor.parseIn("PURPLE"), false), 5));

		objects.get(model.getPlayerByName("Andrea"))
				.add(new ObjectForSale(new TileBusinessPermit(TypeRegion.parseIn("SEASIDE")), 2));

		try {
			model.buyObjects(objects);
			correct = true;
		} catch (Exception e) {
			correct = false;
		}

		assertTrue(correct);
		assertEquals(0, model.getCurrentPlayer().getPositionCoinsTrack());
		assertEquals(27, model.getPlayerByName("Andrea").getPositionCoinsTrack());

	}

	public void testCheckAndTakeBonusBuild() {
		List<CardPolitic> cards;
		TileBonusRegion tileRegion;
		TileBonusColor tileColor;
		boolean correct;

		cards = new ArrayList<>();
		tileColor = null;
		tileRegion = null;
		cards.add(new CardPolitic(null, true));

		for (CardPolitic card : cards) {
			model.getCurrentPlayer().addCardPolitic(card);
		}
		model.getBoard().getCity("Arkon").buildEmporium(model.getCurrentPlayer());
		model.getBoard().getCity("Castrum").buildEmporium(model.getCurrentPlayer());
		model.getBoard().getCity("Burgen").buildEmporium(model.getCurrentPlayer());
		model.getBoard().getCity("Dorful").buildEmporium(model.getCurrentPlayer());

		model.getBoard().getCity("Indur").buildEmporium(model.getCurrentPlayer());
		model.getBoard().getCity("Naris").buildEmporium(model.getCurrentPlayer());
		model.getBoard().getCity("Arkon").buildEmporium(model.getCurrentPlayer());

		model.buildEmporiumKing(model.getBoard().getCity("Esti"), cards, 1);

		assertEquals("2",model.getCurrentPlayer().getTilesBonus().size(), 4);

		for (TileBonus tile : model.getCurrentPlayer().getTilesBonus()) {
			if (tile instanceof TileBonusRegion) {
				tileRegion = (TileBonusRegion) tile;
			} else if (tile instanceof TileBonusColor) {
				tileColor = (TileBonusColor) tile;
			}
		}
		
		if (tileColor!=null && tileRegion!=null){
			if (tileColor.getColor()==ColorCity.BRONZE && tileRegion.getRegion()==TypeRegion.SEASIDE){
				correct=true;
			}
			else{
				correct=false;
			}
		}
		else {
			correct=false;
		}
		
		assertTrue("1",correct);
	}

}
